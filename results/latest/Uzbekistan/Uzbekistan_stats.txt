0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1650.54    19.91
p_loo       19.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      264   89.2%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.049   0.167    0.343  ...   152.0     152.0      60.0   1.01
pu         0.773   0.047   0.701    0.853  ...   152.0     152.0      88.0   1.03
mu         0.119   0.029   0.080    0.171  ...     5.0       5.0      32.0   1.41
mus        0.199   0.049   0.119    0.286  ...    49.0      54.0      54.0   1.04
gamma      0.276   0.045   0.197    0.357  ...    85.0      72.0      61.0   0.99
Is_begin   8.870   5.846   0.229   20.061  ...    41.0      88.0      21.0   1.11
Ia_begin  14.331  11.250   0.088   34.820  ...    19.0      14.0      20.0   1.11
E_begin   14.671  14.904   0.024   52.614  ...    70.0      34.0      40.0   1.05

[8 rows x 11 columns]