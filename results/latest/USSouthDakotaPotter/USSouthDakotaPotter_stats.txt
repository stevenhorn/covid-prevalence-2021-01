0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -407.48    29.44
p_loo       26.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.054   0.176    0.345  ...    39.0      37.0      86.0   1.04
pu        0.740  0.023   0.708    0.789  ...    12.0      12.0      48.0   1.14
mu        0.123  0.030   0.070    0.182  ...    27.0      35.0      60.0   1.04
mus       0.167  0.036   0.112    0.236  ...    75.0      66.0      86.0   1.03
gamma     0.197  0.044   0.122    0.270  ...    22.0      20.0      60.0   1.08
Is_begin  0.256  0.366   0.000    0.975  ...    50.0      13.0      38.0   1.15
Ia_begin  0.444  0.538   0.000    1.432  ...    13.0       8.0      39.0   1.23
E_begin   0.203  0.282   0.000    0.723  ...    56.0      11.0      59.0   1.16

[8 rows x 11 columns]