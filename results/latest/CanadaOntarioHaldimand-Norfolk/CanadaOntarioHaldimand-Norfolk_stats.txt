0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -851.16    44.87
p_loo       41.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   92.6%
 (0.5, 0.7]   (ok)         17    5.5%
   (0.7, 1]   (bad)         5    1.6%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.259   0.066   0.150    0.343  ...    31.0      26.0      22.0   1.06
pu         0.869   0.032   0.800    0.900  ...    55.0      51.0      57.0   1.01
mu         0.196   0.026   0.154    0.246  ...    12.0       9.0      38.0   1.17
mus        0.290   0.046   0.216    0.389  ...    53.0      63.0      60.0   1.02
gamma      0.478   0.076   0.372    0.628  ...    90.0      90.0      80.0   1.01
Is_begin   5.153   4.046   0.550   12.219  ...    97.0      44.0      39.0   1.05
Ia_begin  22.259  16.156   0.356   50.629  ...    57.0      61.0      15.0   1.06
E_begin   13.388  11.080   0.291   37.328  ...    54.0      54.0      59.0   0.99

[8 rows x 11 columns]