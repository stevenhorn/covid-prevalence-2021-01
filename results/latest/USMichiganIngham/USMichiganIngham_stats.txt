0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1323.49    24.13
p_loo       22.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.060   0.153    0.335  ...   152.0     152.0      46.0   1.12
pu        0.786  0.056   0.701    0.872  ...    56.0      55.0      55.0   1.03
mu        0.129  0.020   0.095    0.173  ...    35.0      33.0      49.0   1.06
mus       0.186  0.033   0.136    0.263  ...    75.0      77.0      56.0   1.03
gamma     0.269  0.043   0.186    0.356  ...   152.0     152.0      41.0   1.00
Is_begin  3.124  2.111   0.262    6.815  ...    86.0     151.0      80.0   1.01
Ia_begin  5.484  3.805   0.032   12.487  ...    79.0      61.0      42.0   1.03
E_begin   4.646  3.987   0.225   12.696  ...   100.0     118.0      53.0   1.03

[8 rows x 11 columns]