0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1031.77    59.10
p_loo       50.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.215  0.050   0.150    0.306  ...    31.0      24.0      56.0   1.07
pu        0.727  0.022   0.700    0.769  ...    37.0      20.0      18.0   1.07
mu        0.173  0.022   0.135    0.213  ...    36.0      35.0      43.0   0.99
mus       0.286  0.035   0.223    0.344  ...    53.0      31.0      60.0   1.08
gamma     0.443  0.072   0.286    0.563  ...    76.0      75.0      60.0   1.01
Is_begin  0.793  0.675   0.012    1.880  ...    74.0      48.0      22.0   1.00
Ia_begin  1.616  1.459   0.031    4.805  ...    75.0      80.0      44.0   1.01
E_begin   0.702  0.771   0.025    2.572  ...    29.0      17.0      60.0   1.11

[8 rows x 11 columns]