0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -357.42    27.84
p_loo       24.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.052   0.170    0.331  ...   138.0     149.0      72.0   0.99
pu        0.805  0.051   0.703    0.866  ...     6.0       7.0      36.0   1.26
mu        0.124  0.021   0.084    0.164  ...    77.0      71.0      60.0   1.03
mus       0.175  0.037   0.102    0.235  ...   106.0     115.0      40.0   1.02
gamma     0.201  0.042   0.138    0.282  ...   121.0     152.0      38.0   1.10
Is_begin  0.343  0.391   0.003    1.022  ...    73.0      63.0      60.0   1.02
Ia_begin  0.521  0.647   0.004    1.881  ...    91.0      63.0      59.0   1.03
E_begin   0.289  0.384   0.000    1.163  ...    65.0      60.0      60.0   1.02

[8 rows x 11 columns]