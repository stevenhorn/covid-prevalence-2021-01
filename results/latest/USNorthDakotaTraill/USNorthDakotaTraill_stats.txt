0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -686.37    46.06
p_loo       33.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.157    0.339  ...   126.0      95.0      39.0   1.05
pu        0.778  0.024   0.740    0.825  ...    61.0      59.0      59.0   1.01
mu        0.124  0.025   0.087    0.173  ...    49.0      40.0      54.0   1.01
mus       0.184  0.030   0.135    0.242  ...    89.0      87.0      93.0   1.00
gamma     0.213  0.038   0.147    0.280  ...   100.0     106.0      86.0   1.00
Is_begin  0.415  0.488   0.002    1.542  ...    42.0      73.0      40.0   1.02
Ia_begin  0.656  0.827   0.007    1.875  ...   103.0     114.0      83.0   1.00
E_begin   0.353  0.398   0.005    1.066  ...    43.0      23.0      34.0   1.08

[8 rows x 11 columns]