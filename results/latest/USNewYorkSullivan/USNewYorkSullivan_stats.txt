0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -879.61    25.93
p_loo       21.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.246  0.058   0.150    0.337  ...   125.0     110.0      39.0   1.06
pu         0.756  0.035   0.701    0.803  ...   113.0     106.0      60.0   1.01
mu         0.131  0.018   0.103    0.168  ...    71.0      71.0      60.0   1.01
mus        0.180  0.033   0.114    0.228  ...   152.0     152.0      83.0   1.00
gamma      0.282  0.052   0.195    0.375  ...   113.0     152.0      93.0   1.00
Is_begin   3.417  2.466   0.049    8.263  ...   140.0     114.0      59.0   0.99
Ia_begin   9.180  3.859   2.439   15.569  ...    87.0      80.0      59.0   1.00
E_begin   12.344  8.526   0.407   29.532  ...    44.0      53.0      31.0   1.02

[8 rows x 11 columns]