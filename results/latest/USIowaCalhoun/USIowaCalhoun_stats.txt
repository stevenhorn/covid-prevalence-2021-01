0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -768.28    49.92
p_loo       29.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.054   0.170    0.349  ...    18.0      25.0      43.0   1.07
pu        0.760  0.023   0.716    0.798  ...    29.0      31.0      17.0   1.07
mu        0.097  0.012   0.079    0.119  ...    51.0      51.0      53.0   1.02
mus       0.175  0.031   0.132    0.238  ...    18.0      18.0      46.0   1.13
gamma     0.322  0.060   0.205    0.419  ...   117.0     133.0      86.0   1.03
Is_begin  0.411  0.442   0.005    1.447  ...    52.0      24.0      29.0   1.07
Ia_begin  0.522  0.833   0.003    1.998  ...    52.0      27.0      35.0   1.06
E_begin   0.238  0.272   0.003    0.641  ...    33.0      24.0      74.0   1.07

[8 rows x 11 columns]