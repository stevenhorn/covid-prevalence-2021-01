0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -993.87    49.17
p_loo       33.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.056   0.152    0.332  ...    62.0      60.0      57.0   1.03
pu        0.775  0.046   0.708    0.858  ...    45.0      57.0      14.0   1.06
mu        0.129  0.018   0.099    0.169  ...    28.0      31.0      40.0   1.05
mus       0.197  0.039   0.137    0.271  ...    64.0      63.0      43.0   0.99
gamma     0.266  0.053   0.182    0.354  ...   152.0     152.0      86.0   0.98
Is_begin  1.444  1.351   0.019    3.910  ...    52.0      30.0      60.0   1.13
Ia_begin  0.855  0.617   0.027    1.744  ...    47.0      32.0      42.0   1.14
E_begin   1.421  1.359   0.010    3.934  ...    27.0      18.0      59.0   1.09

[8 rows x 11 columns]