0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -794.61    41.68
p_loo       37.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.057   0.162    0.350  ...   107.0     132.0      40.0   1.00
pu        0.824  0.032   0.770    0.877  ...    68.0      69.0      60.0   1.00
mu        0.155  0.030   0.106    0.225  ...    33.0      33.0      59.0   1.05
mus       0.237  0.048   0.155    0.333  ...   101.0      99.0      59.0   1.01
gamma     0.312  0.061   0.212    0.456  ...    99.0      92.0      57.0   1.04
Is_begin  0.697  0.739   0.001    2.448  ...   137.0     152.0      40.0   0.98
Ia_begin  1.118  1.082   0.004    3.142  ...   123.0     122.0      59.0   0.99
E_begin   0.597  0.835   0.002    1.517  ...   102.0     140.0      83.0   1.01

[8 rows x 11 columns]