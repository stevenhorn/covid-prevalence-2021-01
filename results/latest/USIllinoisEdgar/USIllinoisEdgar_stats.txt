0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.69    69.64
p_loo       41.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.055   0.154    0.338  ...    72.0      72.0      93.0   0.98
pu        0.755  0.027   0.712    0.801  ...    10.0      10.0      40.0   1.18
mu        0.116  0.017   0.086    0.147  ...    58.0      58.0      60.0   1.04
mus       0.260  0.039   0.195    0.329  ...   152.0     152.0      58.0   1.00
gamma     0.430  0.086   0.296    0.589  ...    83.0      92.0      99.0   0.99
Is_begin  0.538  0.843   0.000    2.634  ...   119.0      47.0      55.0   1.00
Ia_begin  0.665  0.823   0.000    1.946  ...    88.0      52.0      37.0   1.01
E_begin   0.270  0.408   0.004    1.006  ...    88.0      14.0      31.0   1.12

[8 rows x 11 columns]