0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo   334.66    62.15
p_loo       66.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    5    1.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.047   0.185    0.343  ...   152.0     152.0      79.0   1.03
pu        0.873  0.027   0.823    0.898  ...    90.0      99.0      37.0   1.05
mu        0.168  0.037   0.105    0.223  ...    34.0      31.0      57.0   1.06
mus       0.275  0.028   0.237    0.331  ...   110.0     115.0      32.0   0.99
gamma     0.308  0.044   0.232    0.380  ...    46.0     119.0      16.0   1.02
Is_begin  0.486  0.530   0.006    1.492  ...    93.0      99.0      91.0   1.03
Ia_begin  0.735  0.885   0.015    2.075  ...    87.0      97.0      93.0   1.01
E_begin   0.250  0.331   0.002    0.673  ...    91.0      69.0      77.0   0.98

[8 rows x 11 columns]