1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1345.46    30.29
p_loo       27.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    2    0.7%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.235   0.052   0.161    0.337  ...   152.0     152.0      93.0   0.99
pu         0.768   0.047   0.701    0.850  ...    91.0     143.0      61.0   1.05
mu         0.108   0.024   0.069    0.146  ...    19.0      29.0      43.0   1.07
mus        0.164   0.028   0.110    0.205  ...    83.0      83.0      56.0   1.02
gamma      0.181   0.036   0.130    0.249  ...   127.0     136.0      93.0   1.00
Is_begin  13.402   8.641   0.282   26.956  ...    37.0      31.0      40.0   1.06
Ia_begin  32.372  14.873   4.086   59.779  ...    47.0      44.0      40.0   1.03
E_begin   31.506  20.031   2.091   67.095  ...    61.0      50.0      18.0   1.00

[8 rows x 11 columns]