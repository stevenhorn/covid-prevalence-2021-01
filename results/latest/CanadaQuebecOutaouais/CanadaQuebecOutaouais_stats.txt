0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1085.40    31.60
p_loo       24.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      296   95.8%
 (0.5, 0.7]   (ok)         10    3.2%
   (0.7, 1]   (bad)         2    0.6%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.257   0.052   0.167    0.340  ...   152.0     152.0      59.0   1.01
pu         0.828   0.053   0.725    0.900  ...    21.0      26.0      59.0   1.07
mu         0.132   0.024   0.092    0.178  ...    18.0      21.0      49.0   1.09
mus        0.172   0.024   0.132    0.207  ...    81.0      84.0      60.0   0.99
gamma      0.223   0.045   0.154    0.304  ...    97.0     106.0      37.0   1.10
Is_begin   5.304   4.558   0.030   14.942  ...   109.0      74.0      40.0   1.04
Ia_begin  29.077  21.388   2.167   76.043  ...   122.0      62.0      19.0   1.03
E_begin   13.371  12.643   0.673   45.529  ...    82.0      72.0     100.0   1.02

[8 rows x 11 columns]