0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -821.19    28.97
p_loo       18.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   96.9%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.196  0.034   0.159    0.270  ...     4.0       5.0      22.0   1.46
pu        0.809  0.010   0.792    0.826  ...     7.0       8.0      37.0   1.26
mu        0.224  0.044   0.158    0.281  ...     3.0       3.0      22.0   2.24
mus       0.188  0.042   0.141    0.263  ...     3.0       3.0      22.0   2.04
gamma     0.119  0.010   0.103    0.137  ...     3.0       4.0      14.0   1.83
Is_begin  0.837  0.687   0.073    2.254  ...     4.0       5.0      14.0   1.47
Ia_begin  2.252  1.112   0.708    4.096  ...     5.0       4.0      14.0   1.61
E_begin   1.762  0.953   0.234    3.302  ...     4.0       4.0      30.0   1.60

[8 rows x 11 columns]