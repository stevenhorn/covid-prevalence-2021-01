2 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1217.09    28.35
p_loo       26.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.054   0.151    0.328  ...    54.0      50.0      54.0   1.06
pu        0.845  0.045   0.746    0.892  ...    10.0      10.0      37.0   1.16
mu        0.124  0.021   0.088    0.166  ...    12.0      12.0      56.0   1.14
mus       0.168  0.035   0.116    0.225  ...    60.0     101.0     100.0   1.03
gamma     0.206  0.042   0.136    0.289  ...    89.0     111.0      57.0   1.00
Is_begin  1.455  1.229   0.065    4.219  ...    78.0      31.0      17.0   1.05
Ia_begin  4.615  2.817   0.528   10.157  ...   102.0      93.0      48.0   0.99
E_begin   3.429  2.920   0.019    8.992  ...    74.0      73.0      43.0   1.02

[8 rows x 11 columns]