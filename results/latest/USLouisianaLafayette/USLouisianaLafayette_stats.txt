0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1510.43    38.99
p_loo       31.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.054   0.162    0.340  ...    28.0      68.0      19.0   1.20
pu        0.760  0.035   0.701    0.814  ...    28.0      29.0      41.0   1.06
mu        0.137  0.024   0.107    0.181  ...    23.0      27.0      31.0   1.06
mus       0.202  0.028   0.152    0.246  ...    79.0      81.0      57.0   1.00
gamma     0.294  0.047   0.231    0.396  ...   131.0     120.0      88.0   0.99
Is_begin  0.947  0.703   0.011    2.017  ...   152.0     145.0      35.0   1.00
Ia_begin  3.190  1.891   0.254    6.853  ...    97.0      90.0      35.0   0.99
E_begin   2.934  2.757   0.083    9.347  ...    82.0      93.0      57.0   1.00

[8 rows x 11 columns]