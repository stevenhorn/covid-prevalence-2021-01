2 Divergences 
Passed validation 
Computed from 80 by 166 log-likelihood matrix

         Estimate       SE
elpd_loo  -431.41    16.82
p_loo       13.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      154   92.8%
 (0.5, 0.7]   (ok)         11    6.6%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.051   0.178    0.341  ...    27.0      27.0      40.0   1.01
pu        0.815  0.060   0.715    0.891  ...     6.0       6.0      36.0   1.30
mu        0.096  0.021   0.069    0.144  ...     6.0       7.0      40.0   1.29
mus       0.136  0.031   0.079    0.182  ...    61.0      67.0      60.0   1.07
gamma     0.163  0.034   0.104    0.226  ...    78.0      73.0      58.0   1.01
Is_begin  2.104  1.982   0.043    6.763  ...    46.0      48.0      56.0   1.02
Ia_begin  1.869  1.379   0.095    4.354  ...    59.0      53.0      40.0   1.02
E_begin   1.267  1.177   0.028    2.946  ...    48.0      53.0      43.0   1.01

[8 rows x 11 columns]