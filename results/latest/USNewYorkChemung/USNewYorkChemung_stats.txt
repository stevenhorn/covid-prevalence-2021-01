0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -860.21    32.64
p_loo       30.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.057   0.171    0.349  ...    43.0      53.0      42.0   1.03
pu        0.823  0.036   0.763    0.876  ...    33.0      45.0      39.0   1.02
mu        0.139  0.019   0.107    0.176  ...    21.0      21.0      38.0   1.07
mus       0.187  0.025   0.139    0.232  ...   106.0     113.0      73.0   1.02
gamma     0.270  0.046   0.191    0.335  ...   144.0     148.0      88.0   0.99
Is_begin  1.835  1.002   0.414    3.758  ...    84.0      77.0      60.0   0.98
Ia_begin  5.128  2.867   0.800   10.048  ...    86.0      81.0      56.0   0.99
E_begin   3.961  3.481   0.119   11.377  ...    65.0      53.0      60.0   0.98

[8 rows x 11 columns]