0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -759.01    48.36
p_loo       36.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.057   0.168    0.347  ...    60.0      71.0      60.0   1.02
pu        0.872  0.023   0.839    0.896  ...    55.0      54.0      58.0   1.06
mu        0.136  0.022   0.096    0.168  ...    19.0      18.0      16.0   1.08
mus       0.178  0.037   0.127    0.247  ...    63.0      60.0      37.0   1.02
gamma     0.245  0.054   0.156    0.362  ...    63.0      62.0      40.0   1.02
Is_begin  0.769  0.942   0.040    2.484  ...    93.0      62.0      42.0   1.03
Ia_begin  1.517  1.395   0.044    3.838  ...    74.0      51.0      60.0   1.02
E_begin   0.743  0.586   0.020    1.844  ...    73.0      60.0      60.0   1.07

[8 rows x 11 columns]