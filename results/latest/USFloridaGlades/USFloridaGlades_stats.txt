0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -848.67    54.20
p_loo       66.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.056   0.168    0.341  ...   100.0     116.0      60.0   1.03
pu        0.829  0.032   0.765    0.886  ...    29.0      24.0      34.0   1.08
mu        0.140  0.034   0.087    0.199  ...    10.0       9.0      24.0   1.19
mus       0.210  0.037   0.152    0.291  ...    27.0      26.0      60.0   1.07
gamma     0.248  0.051   0.161    0.334  ...    39.0      67.0      60.0   1.08
Is_begin  0.650  0.527   0.024    1.713  ...    63.0      52.0      39.0   1.03
Ia_begin  1.652  1.943   0.017    6.223  ...    59.0      70.0      40.0   1.00
E_begin   0.766  0.881   0.002    2.290  ...    70.0      54.0      59.0   1.03

[8 rows x 11 columns]