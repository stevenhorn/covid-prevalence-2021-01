0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -938.94    30.57
p_loo       27.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   92.9%
 (0.5, 0.7]   (ok)         17    5.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.044   0.155    0.317  ...   152.0     152.0      45.0   1.04
pu        0.820  0.043   0.741    0.894  ...   130.0     136.0      93.0   1.00
mu        0.128  0.023   0.095    0.176  ...    19.0      20.0      27.0   1.17
mus       0.225  0.043   0.169    0.319  ...   135.0     131.0      64.0   1.03
gamma     0.379  0.055   0.283    0.474  ...    24.0      30.0      93.0   1.06
Is_begin  1.779  1.203   0.050    3.718  ...   120.0     100.0      59.0   1.00
Ia_begin  5.450  2.980   1.248   10.929  ...   152.0     152.0      93.0   0.99
E_begin   5.017  3.968   0.143   12.133  ...   130.0     119.0     100.0   0.99

[8 rows x 11 columns]