0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -871.49    43.52
p_loo       33.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.051   0.174    0.332  ...    87.0      92.0      84.0   1.02
pu        0.818  0.031   0.752    0.862  ...    21.0      21.0      77.0   1.07
mu        0.135  0.023   0.097    0.181  ...    31.0      34.0      58.0   1.01
mus       0.194  0.041   0.135    0.272  ...    89.0      81.0      59.0   1.05
gamma     0.256  0.051   0.168    0.361  ...   120.0     149.0      60.0   1.00
Is_begin  0.799  0.591   0.026    1.749  ...   109.0      80.0      60.0   1.00
Ia_begin  1.769  1.578   0.029    4.243  ...    93.0      99.0      60.0   1.00
E_begin   0.994  1.297   0.021    3.936  ...    93.0      72.0      91.0   0.99

[8 rows x 11 columns]