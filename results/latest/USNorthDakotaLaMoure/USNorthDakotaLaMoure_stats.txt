0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -587.75    52.97
p_loo       31.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.175    0.332  ...   152.0     152.0      80.0   1.02
pu        0.766  0.028   0.700    0.809  ...    97.0     100.0      33.0   1.06
mu        0.119  0.022   0.085    0.153  ...    81.0      78.0      86.0   1.02
mus       0.173  0.027   0.135    0.224  ...   152.0     152.0      99.0   0.99
gamma     0.204  0.038   0.148    0.280  ...   127.0     127.0      91.0   1.00
Is_begin  0.326  0.523   0.001    1.081  ...    71.0      70.0      49.0   1.02
Ia_begin  0.684  0.705   0.000    2.108  ...   128.0      98.0      60.0   1.02
E_begin   0.256  0.329   0.001    0.944  ...    76.0      57.0      53.0   1.02

[8 rows x 11 columns]