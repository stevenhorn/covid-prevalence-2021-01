24 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1167.62    27.11
p_loo       24.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      254   86.1%
 (0.5, 0.7]   (ok)         31   10.5%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.052   0.170    0.334  ...     5.0       6.0      47.0   1.70
pu        0.768  0.034   0.700    0.811  ...    17.0      22.0      42.0   1.16
mu        0.153  0.024   0.108    0.178  ...     5.0       7.0      54.0   1.26
mus       0.233  0.042   0.165    0.283  ...     5.0       5.0      32.0   1.42
gamma     0.296  0.043   0.225    0.366  ...    98.0      84.0      80.0   1.07
Is_begin  2.256  1.205   0.316    4.386  ...    25.0      35.0      28.0   1.61
Ia_begin  5.075  2.722   1.025   10.913  ...    12.0      17.0      93.0   1.16
E_begin   5.488  3.802   0.253   13.504  ...    55.0     104.0      81.0   1.23

[8 rows x 11 columns]