0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1191.12    39.97
p_loo       30.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      264   89.5%
 (0.5, 0.7]   (ok)         25    8.5%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.051   0.151    0.322  ...   139.0     152.0      86.0   1.04
pu        0.778  0.027   0.721    0.822  ...    20.0      25.0      14.0   1.03
mu        0.120  0.014   0.093    0.146  ...    16.0      16.0      40.0   1.12
mus       0.273  0.035   0.207    0.328  ...   152.0     152.0      95.0   1.00
gamma     0.511  0.067   0.377    0.619  ...   138.0     152.0      87.0   1.06
Is_begin  1.168  0.938   0.064    3.041  ...   107.0      84.0      88.0   1.04
Ia_begin  2.223  1.848   0.086    5.835  ...    30.0      19.0      59.0   1.10
E_begin   1.594  1.250   0.021    3.824  ...    20.0      18.0      35.0   1.11

[8 rows x 11 columns]