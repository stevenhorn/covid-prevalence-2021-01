0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -773.01    19.05
p_loo       18.98        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.055   0.178    0.348  ...    41.0      35.0      22.0   1.03
pu        0.805  0.032   0.737    0.847  ...    11.0      12.0      59.0   1.12
mu        0.130  0.024   0.090    0.178  ...    10.0      16.0      19.0   1.27
mus       0.163  0.031   0.104    0.221  ...   116.0     103.0      87.0   1.10
gamma     0.198  0.032   0.143    0.265  ...    35.0      31.0      34.0   1.05
Is_begin  0.812  0.822   0.034    2.316  ...    96.0      63.0      61.0   1.01
Ia_begin  1.692  1.592   0.058    5.635  ...    62.0      47.0      59.0   1.03
E_begin   0.986  1.007   0.002    3.226  ...    17.0      12.0      22.0   1.13

[8 rows x 11 columns]