0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -505.34    29.27
p_loo       32.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.050   0.186    0.344  ...   100.0     110.0      69.0   1.02
pu        0.845  0.042   0.755    0.899  ...    40.0      56.0      55.0   1.04
mu        0.143  0.026   0.099    0.180  ...    65.0      66.0      57.0   1.02
mus       0.196  0.032   0.151    0.256  ...   152.0     152.0      45.0   1.06
gamma     0.256  0.050   0.151    0.340  ...   149.0     152.0      61.0   1.01
Is_begin  0.518  0.602   0.005    1.386  ...   104.0      87.0      44.0   1.00
Ia_begin  1.333  1.491   0.003    4.022  ...    94.0     149.0      84.0   1.02
E_begin   0.567  0.718   0.001    1.927  ...    84.0      82.0      88.0   1.01

[8 rows x 11 columns]