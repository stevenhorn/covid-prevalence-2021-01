0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -429.46    45.16
p_loo       36.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.048   0.154    0.324  ...   109.0     104.0      39.0   1.01
pu        0.767  0.029   0.715    0.816  ...    17.0      18.0      59.0   1.09
mu        0.156  0.031   0.099    0.205  ...     7.0       8.0      58.0   1.25
mus       0.220  0.035   0.161    0.284  ...    56.0      73.0      49.0   1.01
gamma     0.276  0.051   0.204    0.378  ...   145.0     130.0      96.0   0.99
Is_begin  0.459  0.498   0.005    1.570  ...    89.0     101.0      77.0   0.99
Ia_begin  0.659  0.832   0.012    2.373  ...    35.0      64.0      34.0   1.00
E_begin   0.323  0.388   0.002    1.095  ...    95.0      79.0      96.0   0.99

[8 rows x 11 columns]