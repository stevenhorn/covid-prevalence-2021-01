0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -339.17    32.56
p_loo       32.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.053   0.156    0.341  ...   151.0     129.0      56.0   1.01
pu        0.756  0.049   0.701    0.872  ...    19.0      18.0      33.0   1.09
mu        0.170  0.034   0.113    0.231  ...    28.0      24.0      62.0   1.05
mus       0.246  0.046   0.187    0.349  ...    79.0      93.0      91.0   1.01
gamma     0.291  0.048   0.219    0.390  ...    81.0     103.0      45.0   1.01
Is_begin  1.493  1.286   0.007    4.161  ...    62.0      42.0      24.0   1.03
Ia_begin  3.455  3.168   0.008    9.140  ...    80.0      55.0      31.0   1.01
E_begin   2.089  2.718   0.008    5.668  ...    47.0      67.0      38.0   1.01

[8 rows x 11 columns]