0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1243.45    26.30
p_loo       19.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   96.3%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.175    0.342  ...   152.0     152.0      93.0   1.05
pu        0.840  0.039   0.765    0.893  ...    49.0      53.0      51.0   1.02
mu        0.124  0.019   0.090    0.162  ...    25.0      24.0      16.0   1.06
mus       0.166  0.033   0.117    0.235  ...   130.0     139.0      95.0   1.03
gamma     0.215  0.040   0.138    0.286  ...   106.0     103.0      60.0   1.00
Is_begin  2.278  1.295   0.178    4.611  ...   152.0     149.0      96.0   0.98
Ia_begin  4.250  2.855   0.036    9.826  ...    51.0      40.0      29.0   1.01
E_begin   3.691  2.848   0.479    8.820  ...    87.0      37.0      55.0   1.03

[8 rows x 11 columns]