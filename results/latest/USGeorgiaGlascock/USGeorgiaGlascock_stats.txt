0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -466.60    45.88
p_loo       33.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.059   0.155    0.339  ...    74.0      68.0      60.0   1.03
pu        0.814  0.021   0.781    0.852  ...    64.0      67.0      60.0   1.03
mu        0.125  0.025   0.083    0.170  ...    73.0      73.0      59.0   0.99
mus       0.191  0.035   0.139    0.266  ...    55.0      62.0      51.0   1.02
gamma     0.270  0.051   0.196    0.356  ...   110.0     106.0      97.0   1.00
Is_begin  0.448  0.493   0.003    1.565  ...    82.0      38.0      22.0   1.03
Ia_begin  0.572  0.726   0.009    2.018  ...    48.0      36.0      60.0   1.04
E_begin   0.301  0.336   0.001    1.004  ...    42.0      36.0      60.0   1.03

[8 rows x 11 columns]