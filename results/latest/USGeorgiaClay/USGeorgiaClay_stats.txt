0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -475.47    32.39
p_loo       31.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.048   0.151    0.322  ...    79.0      81.0      40.0   1.00
pu        0.784  0.048   0.703    0.858  ...     5.0       5.0      39.0   1.37
mu        0.148  0.034   0.077    0.210  ...    20.0      24.0      57.0   1.07
mus       0.196  0.036   0.112    0.245  ...    54.0      52.0      59.0   1.02
gamma     0.238  0.053   0.150    0.331  ...    50.0      58.0      43.0   1.04
Is_begin  1.127  0.791   0.081    2.462  ...    67.0      54.0      58.0   1.00
Ia_begin  2.322  1.992   0.017    5.905  ...    80.0      75.0      93.0   1.01
E_begin   1.277  1.323   0.032    3.571  ...   110.0      79.0      59.0   1.01

[8 rows x 11 columns]