0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -496.72    33.70
p_loo       28.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      258   87.5%
 (0.5, 0.7]   (ok)         32   10.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.049   0.152    0.321  ...    27.0      25.0      40.0   1.07
pu        0.797  0.028   0.741    0.843  ...     8.0       8.0      43.0   1.21
mu        0.139  0.029   0.086    0.193  ...    28.0      26.0      40.0   1.05
mus       0.177  0.029   0.129    0.230  ...    36.0      36.0      60.0   1.02
gamma     0.195  0.040   0.138    0.280  ...   152.0     152.0      69.0   1.11
Is_begin  0.541  0.563   0.003    1.761  ...    52.0      17.0      38.0   1.10
Ia_begin  0.886  1.184   0.015    3.038  ...    44.0      31.0      77.0   1.04
E_begin   0.425  0.532   0.001    1.497  ...    54.0      33.0      38.0   1.05

[8 rows x 11 columns]