0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -878.46    45.08
p_loo       29.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.6%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.055   0.169    0.349  ...    57.0      56.0      73.0   1.05
pu        0.863  0.025   0.817    0.898  ...    19.0      17.0      17.0   1.09
mu        0.144  0.025   0.102    0.192  ...     7.0       7.0      57.0   1.27
mus       0.203  0.032   0.142    0.251  ...    60.0      70.0      60.0   1.01
gamma     0.278  0.044   0.201    0.357  ...    45.0      47.0      88.0   1.07
Is_begin  0.983  1.067   0.012    2.944  ...    69.0     118.0      59.0   1.07
Ia_begin  1.739  1.431   0.007    4.121  ...    63.0      29.0      14.0   1.07
E_begin   1.029  1.015   0.008    3.447  ...    73.0      64.0      54.0   1.04

[8 rows x 11 columns]