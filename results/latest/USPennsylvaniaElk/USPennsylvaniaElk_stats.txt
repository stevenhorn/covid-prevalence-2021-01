0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -630.19    30.80
p_loo       24.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.048   0.181    0.343  ...    97.0      90.0      59.0   1.01
pu        0.878  0.014   0.853    0.898  ...    62.0      55.0      62.0   1.04
mu        0.131  0.028   0.093    0.176  ...    41.0      29.0      56.0   1.05
mus       0.160  0.025   0.121    0.215  ...    77.0      85.0      60.0   1.01
gamma     0.197  0.037   0.129    0.270  ...   124.0     152.0     100.0   1.01
Is_begin  0.818  0.798   0.007    2.379  ...    89.0     107.0      60.0   1.06
Ia_begin  1.456  1.374   0.060    4.692  ...   101.0      66.0      58.0   1.00
E_begin   0.763  0.728   0.004    1.894  ...    94.0      59.0      30.0   1.01

[8 rows x 11 columns]