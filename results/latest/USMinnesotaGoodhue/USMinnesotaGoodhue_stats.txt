0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -839.80    44.90
p_loo       24.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.057   0.151    0.328  ...    35.0      31.0      32.0   1.05
pu        0.855  0.031   0.788    0.892  ...    30.0      32.0      22.0   1.04
mu        0.123  0.023   0.085    0.164  ...    50.0      53.0      83.0   1.01
mus       0.170  0.032   0.117    0.227  ...   115.0     137.0      60.0   0.99
gamma     0.192  0.044   0.121    0.272  ...    75.0     103.0      44.0   1.04
Is_begin  0.826  0.653   0.017    2.040  ...   149.0     104.0      37.0   0.99
Ia_begin  1.678  1.410   0.002    4.210  ...    91.0      61.0      20.0   1.01
E_begin   0.696  0.754   0.000    2.245  ...   128.0      42.0      15.0   1.06

[8 rows x 11 columns]