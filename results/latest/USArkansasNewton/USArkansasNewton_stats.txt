0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -632.16    56.38
p_loo       39.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.051   0.171    0.343  ...    63.0      65.0      37.0   1.07
pu        0.812  0.017   0.783    0.841  ...    52.0      52.0      60.0   1.01
mu        0.173  0.026   0.129    0.220  ...     7.0       7.0      24.0   1.29
mus       0.261  0.041   0.192    0.336  ...   152.0     152.0     100.0   1.00
gamma     0.389  0.068   0.267    0.494  ...   143.0     152.0      91.0   0.99
Is_begin  0.872  0.999   0.020    2.973  ...    96.0      56.0      38.0   1.01
Ia_begin  1.365  1.466   0.026    4.373  ...    61.0      51.0      97.0   1.02
E_begin   0.550  0.732   0.000    1.874  ...    79.0      43.0      15.0   1.00

[8 rows x 11 columns]