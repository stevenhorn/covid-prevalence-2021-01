0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -525.78    44.95
p_loo       31.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.051   0.151    0.330  ...    81.0      80.0      60.0   0.98
pu        0.868  0.020   0.836    0.898  ...    66.0      64.0      59.0   0.99
mu        0.160  0.026   0.114    0.192  ...    41.0      42.0      59.0   1.00
mus       0.196  0.040   0.136    0.269  ...    91.0      81.0      35.0   1.03
gamma     0.256  0.050   0.171    0.350  ...    82.0      94.0      54.0   1.01
Is_begin  0.905  0.751   0.063    2.375  ...   105.0      90.0      91.0   1.01
Ia_begin  2.349  1.935   0.018    6.469  ...    95.0      91.0      91.0   0.98
E_begin   1.183  1.041   0.010    3.257  ...    66.0      55.0     100.0   1.02

[8 rows x 11 columns]