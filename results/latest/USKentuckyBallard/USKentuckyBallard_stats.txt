0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -414.61    24.36
p_loo       22.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.048   0.187    0.348  ...   147.0     152.0      60.0   1.01
pu        0.876  0.025   0.820    0.900  ...    65.0     105.0      43.0   1.01
mu        0.133  0.023   0.101    0.187  ...    87.0      87.0      95.0   1.05
mus       0.162  0.029   0.113    0.221  ...   100.0      98.0      77.0   0.99
gamma     0.182  0.028   0.125    0.225  ...    86.0      72.0      49.0   1.03
Is_begin  0.647  0.625   0.003    1.695  ...    68.0      28.0      96.0   1.07
Ia_begin  1.485  1.633   0.010    5.300  ...    87.0     120.0      40.0   1.00
E_begin   0.775  0.851   0.009    2.612  ...    74.0      57.0      96.0   1.01

[8 rows x 11 columns]