0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1158.04    34.37
p_loo       25.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.045   0.203    0.349  ...    99.0     140.0      85.0   1.08
pu        0.888  0.014   0.850    0.900  ...    29.0      31.0      42.0   1.05
mu        0.161  0.029   0.121    0.221  ...     4.0       5.0      24.0   1.44
mus       0.177  0.034   0.124    0.240  ...   152.0     152.0      79.0   1.03
gamma     0.225  0.042   0.140    0.298  ...    93.0      81.0      60.0   1.02
Is_begin  0.910  0.992   0.037    3.167  ...    90.0     109.0      60.0   1.01
Ia_begin  1.792  1.517   0.050    4.713  ...   121.0     119.0      93.0   0.99
E_begin   1.046  1.122   0.005    2.941  ...    92.0      82.0      57.0   1.02

[8 rows x 11 columns]