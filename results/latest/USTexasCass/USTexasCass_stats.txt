0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -945.40    62.43
p_loo       58.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.057   0.179    0.347  ...    51.0      48.0      39.0   1.05
pu        0.856  0.022   0.817    0.893  ...    16.0      20.0      40.0   1.11
mu        0.129  0.021   0.091    0.165  ...    15.0      13.0      42.0   1.11
mus       0.198  0.035   0.132    0.260  ...    82.0      77.0      57.0   1.02
gamma     0.251  0.049   0.149    0.333  ...     9.0       8.0      69.0   1.21
Is_begin  0.940  0.777   0.074    2.417  ...   129.0     116.0      76.0   1.02
Ia_begin  2.060  1.981   0.074    6.169  ...    62.0      40.0      57.0   1.05
E_begin   1.005  1.084   0.008    2.791  ...    73.0      49.0      88.0   1.02

[8 rows x 11 columns]