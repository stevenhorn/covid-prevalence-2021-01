2 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1387.00    37.60
p_loo       32.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.058   0.161    0.343  ...    23.0      22.0      35.0   1.06
pu        0.835  0.019   0.797    0.864  ...    24.0      26.0      18.0   1.06
mu        0.162  0.030   0.120    0.226  ...     8.0       9.0      33.0   1.21
mus       0.155  0.026   0.106    0.203  ...    50.0      52.0      59.0   1.00
gamma     0.185  0.035   0.126    0.239  ...    61.0      61.0      97.0   1.00
Is_begin  0.724  0.659   0.005    2.197  ...    31.0      11.0      25.0   1.16
Ia_begin  0.594  0.441   0.028    1.352  ...    36.0      24.0      33.0   1.05
E_begin   0.515  0.550   0.005    1.738  ...    16.0      13.0      43.0   1.12

[8 rows x 11 columns]