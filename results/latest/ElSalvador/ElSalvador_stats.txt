0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1738.40    26.63
p_loo       18.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.049   0.181    0.343  ...   100.0     104.0      43.0   1.08
pu        0.868  0.038   0.807    0.900  ...    32.0      50.0      22.0   1.07
mu        0.112  0.023   0.064    0.154  ...    14.0      14.0      24.0   1.23
mus       0.152  0.027   0.115    0.216  ...    59.0      45.0      58.0   1.04
gamma     0.178  0.034   0.114    0.230  ...   108.0      98.0      59.0   1.01
Is_begin  0.874  0.750   0.046    1.968  ...    48.0      82.0      88.0   1.09
Ia_begin  1.943  1.324   0.016    4.115  ...    69.0      65.0      60.0   0.99
E_begin   0.972  1.249   0.011    3.590  ...    54.0      35.0     102.0   1.03

[8 rows x 11 columns]