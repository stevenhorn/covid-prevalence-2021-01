0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -754.33    32.37
p_loo       35.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.3%
 (0.5, 0.7]   (ok)          9    3.0%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.282  0.055   0.184    0.348  ...   135.0     114.0      60.0   0.98
pu        0.882  0.020   0.848    0.900  ...   127.0     110.0      67.0   1.01
mu        0.154  0.020   0.124    0.194  ...    21.0      16.0      43.0   1.10
mus       0.172  0.036   0.097    0.215  ...    62.0      50.0      80.0   1.03
gamma     0.214  0.046   0.143    0.307  ...    77.0      79.0      46.0   1.03
Is_begin  1.001  0.935   0.019    3.167  ...   130.0     101.0      74.0   1.02
Ia_begin  1.688  1.797   0.003    5.164  ...    16.0      11.0      51.0   1.16
E_begin   0.680  0.787   0.002    1.677  ...    37.0      30.0      58.0   1.07

[8 rows x 11 columns]