0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -652.83    26.16
p_loo       22.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.055   0.183    0.347  ...    25.0      26.0      35.0   1.09
pu        0.805  0.025   0.758    0.843  ...    11.0      10.0      39.0   1.19
mu        0.132  0.029   0.082    0.184  ...    19.0      18.0      22.0   1.11
mus       0.160  0.029   0.120    0.225  ...    78.0      77.0      59.0   1.00
gamma     0.187  0.040   0.124    0.272  ...    51.0      51.0      49.0   1.03
Is_begin  0.728  0.811   0.006    2.367  ...    33.0      51.0      49.0   1.04
Ia_begin  1.199  1.413   0.017    3.862  ...    63.0      30.0      54.0   1.07
E_begin   0.567  0.645   0.012    1.913  ...    64.0      39.0      60.0   1.01

[8 rows x 11 columns]