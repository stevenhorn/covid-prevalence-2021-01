0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -700.09    25.78
p_loo       24.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.057   0.162    0.344  ...    43.0      42.0      40.0   1.04
pu        0.862  0.026   0.809    0.898  ...    45.0      40.0      39.0   1.06
mu        0.128  0.021   0.087    0.169  ...    26.0      28.0      33.0   1.07
mus       0.154  0.031   0.115    0.218  ...    71.0      65.0      22.0   1.02
gamma     0.177  0.044   0.104    0.248  ...   106.0      95.0     100.0   1.01
Is_begin  1.095  1.019   0.078    3.079  ...    87.0      68.0      81.0   1.01
Ia_begin  2.643  2.111   0.063    6.685  ...    88.0      67.0      40.0   1.03
E_begin   1.386  1.529   0.010    3.955  ...    86.0      64.0      93.0   1.01

[8 rows x 11 columns]