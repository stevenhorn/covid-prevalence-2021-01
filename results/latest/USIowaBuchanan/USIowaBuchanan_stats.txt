0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -644.19    22.02
p_loo       21.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.057   0.156    0.342  ...    57.0      53.0      40.0   1.03
pu        0.836  0.037   0.751    0.888  ...    21.0      24.0      40.0   1.09
mu        0.125  0.026   0.083    0.174  ...    42.0      41.0      59.0   1.00
mus       0.182  0.031   0.129    0.236  ...   152.0     152.0      60.0   1.00
gamma     0.218  0.040   0.149    0.288  ...   152.0     152.0      74.0   1.04
Is_begin  0.700  0.749   0.007    2.306  ...    78.0      38.0      58.0   1.02
Ia_begin  1.552  1.453   0.012    3.850  ...    99.0      90.0      96.0   0.99
E_begin   0.681  0.707   0.019    2.292  ...    83.0     100.0      56.0   0.98

[8 rows x 11 columns]