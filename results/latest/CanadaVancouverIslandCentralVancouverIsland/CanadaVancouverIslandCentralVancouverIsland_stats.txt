0 Divergences 
Passed validation 
Computed from 80 by 307 log-likelihood matrix

         Estimate       SE
elpd_loo  -478.75    26.78
p_loo       26.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      289   94.1%
 (0.5, 0.7]   (ok)         12    3.9%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.278   0.045   0.198    0.347  ...    81.0      83.0      51.0   1.17
pu         0.887   0.014   0.864    0.900  ...    64.0      70.0      57.0   0.99
mu         0.198   0.027   0.151    0.248  ...    17.0      15.0      72.0   1.10
mus        0.228   0.041   0.178    0.329  ...    38.0      56.0      32.0   1.01
gamma      0.335   0.054   0.249    0.428  ...   152.0     152.0      83.0   1.00
Is_begin   4.637   3.441   0.029    9.899  ...   152.0     129.0      80.0   1.01
Ia_begin  14.615  12.850   0.017   40.216  ...    71.0      35.0      40.0   1.01
E_begin    5.837   5.419   0.024   18.564  ...    80.0      88.0      95.0   1.02

[8 rows x 11 columns]