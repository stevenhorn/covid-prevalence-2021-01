0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -342.35    42.44
p_loo       32.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.278  0.053   0.182    0.350  ...   152.0     152.0      59.0   1.07
pu        0.880  0.025   0.812    0.900  ...    30.0      57.0      20.0   1.17
mu        0.156  0.026   0.126    0.218  ...   112.0     106.0      93.0   0.98
mus       0.172  0.038   0.118    0.246  ...   152.0     152.0      60.0   1.04
gamma     0.224  0.045   0.129    0.298  ...   135.0     141.0      19.0   1.05
Is_begin  0.660  0.635   0.044    2.211  ...    87.0      47.0      77.0   1.03
Ia_begin  1.338  1.243   0.008    3.337  ...   120.0     152.0      40.0   1.09
E_begin   0.642  0.769   0.001    1.427  ...   102.0      84.0      36.0   1.04

[8 rows x 11 columns]