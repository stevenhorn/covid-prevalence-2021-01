0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -627.51    24.60
p_loo       22.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.057   0.155    0.338  ...    74.0      74.0      42.0   1.01
pu        0.860  0.023   0.817    0.894  ...    10.0      10.0      42.0   1.17
mu        0.114  0.020   0.076    0.148  ...    21.0      19.0      40.0   1.11
mus       0.147  0.024   0.100    0.198  ...   111.0     135.0      79.0   1.01
gamma     0.166  0.036   0.111    0.229  ...    66.0     100.0      61.0   1.07
Is_begin  0.834  0.840   0.007    2.496  ...   135.0      87.0      87.0   1.00
Ia_begin  1.562  1.719   0.022    4.183  ...    95.0     141.0      96.0   1.01
E_begin   0.810  0.823   0.029    2.509  ...   134.0     106.0      96.0   0.99

[8 rows x 11 columns]