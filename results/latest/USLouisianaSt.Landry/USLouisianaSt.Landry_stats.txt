0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1278.76    30.58
p_loo       28.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.042   0.181    0.320  ...    26.0      26.0      54.0   1.06
pu        0.811  0.024   0.753    0.840  ...    13.0      46.0      57.0   1.74
mu        0.158  0.026   0.114    0.194  ...     3.0       4.0      22.0   1.79
mus       0.198  0.027   0.134    0.244  ...    18.0      18.0      17.0   1.24
gamma     0.195  0.055   0.123    0.290  ...     3.0       3.0      38.0   2.21
Is_begin  1.334  0.791   0.017    2.685  ...    12.0      15.0      23.0   1.25
Ia_begin  0.568  0.554   0.013    1.944  ...    27.0      27.0      54.0   1.24
E_begin   0.850  1.028   0.005    2.442  ...    23.0       9.0      24.0   1.21

[8 rows x 11 columns]