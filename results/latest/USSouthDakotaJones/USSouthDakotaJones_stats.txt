0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -205.12    28.45
p_loo       28.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.057   0.157    0.343  ...    43.0      53.0      24.0   1.04
pu        0.855  0.026   0.811    0.894  ...     7.0       7.0      76.0   1.24
mu        0.124  0.028   0.071    0.169  ...    95.0     152.0      49.0   1.01
mus       0.174  0.030   0.126    0.234  ...    93.0     104.0      67.0   1.00
gamma     0.200  0.042   0.127    0.277  ...   112.0     132.0      21.0   0.99
Is_begin  0.493  0.536   0.002    1.755  ...    85.0      63.0      38.0   0.99
Ia_begin  0.703  0.583   0.009    1.821  ...   109.0      91.0      80.0   1.01
E_begin   0.328  0.442   0.000    0.855  ...    95.0      29.0      59.0   1.07

[8 rows x 11 columns]