0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1268.88    29.18
p_loo       25.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.048   0.156    0.329  ...   152.0     152.0      35.0   1.00
pu        0.793  0.028   0.750    0.856  ...    94.0      91.0      66.0   1.03
mu        0.110  0.021   0.066    0.141  ...    11.0       9.0      21.0   1.19
mus       0.151  0.026   0.112    0.205  ...    65.0      62.0      62.0   1.02
gamma     0.177  0.034   0.094    0.230  ...   152.0     152.0      46.0   0.99
Is_begin  0.606  0.650   0.001    2.110  ...    99.0      90.0      60.0   1.01
Ia_begin  1.207  1.096   0.018    3.269  ...   112.0      95.0      58.0   1.02
E_begin   0.701  0.781   0.002    2.303  ...   133.0     101.0      59.0   1.00

[8 rows x 11 columns]