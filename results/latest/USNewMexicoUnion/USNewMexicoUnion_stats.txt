0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -447.82    37.96
p_loo       41.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.050   0.180    0.349  ...    92.0     104.0     100.0   1.02
pu        0.880  0.011   0.862    0.897  ...    10.0      10.0      40.0   1.15
mu        0.122  0.020   0.089    0.162  ...    55.0      57.0      58.0   1.00
mus       0.169  0.027   0.117    0.208  ...    99.0     152.0      32.0   1.14
gamma     0.232  0.040   0.157    0.287  ...    95.0     152.0      26.0   1.06
Is_begin  0.658  0.803   0.002    2.469  ...   112.0     111.0      67.0   0.99
Ia_begin  1.299  1.254   0.038    3.922  ...    72.0      62.0      60.0   1.05
E_begin   0.538  0.628   0.004    1.531  ...    97.0      78.0      84.0   1.00

[8 rows x 11 columns]