0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1035.14    42.94
p_loo       35.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.051   0.162    0.326  ...    59.0      62.0      60.0   1.05
pu        0.804  0.035   0.730    0.859  ...    28.0      33.0      21.0   1.07
mu        0.167  0.025   0.116    0.213  ...    24.0      23.0      22.0   1.07
mus       0.230  0.033   0.186    0.302  ...    57.0      56.0      59.0   1.06
gamma     0.293  0.053   0.213    0.387  ...   152.0     152.0      93.0   0.98
Is_begin  0.728  0.736   0.009    1.861  ...    74.0      67.0      36.0   0.98
Ia_begin  1.184  1.414   0.004    3.094  ...    67.0      82.0      17.0   1.00
E_begin   0.550  0.528   0.027    1.591  ...   111.0      95.0      39.0   1.02

[8 rows x 11 columns]