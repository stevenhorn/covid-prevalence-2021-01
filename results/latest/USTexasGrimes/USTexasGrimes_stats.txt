0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1058.57    46.78
p_loo       36.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.048   0.175    0.348  ...    45.0      46.0      60.0   1.03
pu        0.840  0.051   0.701    0.891  ...     7.0       8.0      16.0   1.21
mu        0.117  0.020   0.084    0.154  ...    34.0      34.0      38.0   1.03
mus       0.163  0.028   0.116    0.213  ...   101.0     104.0      59.0   0.99
gamma     0.181  0.028   0.135    0.229  ...    52.0      51.0      57.0   1.05
Is_begin  0.634  0.539   0.002    1.754  ...    70.0      60.0      57.0   1.03
Ia_begin  0.659  0.527   0.007    1.570  ...   108.0     124.0      58.0   1.01
E_begin   0.414  0.461   0.012    1.196  ...    97.0      43.0      56.0   1.02

[8 rows x 11 columns]