0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1069.14    34.06
p_loo       27.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.063   0.151    0.340  ...    91.0      51.0      54.0   1.03
pu        0.791  0.034   0.727    0.844  ...    33.0      25.0      16.0   1.06
mu        0.104  0.023   0.069    0.155  ...    71.0      81.0      69.0   1.01
mus       0.136  0.031   0.081    0.193  ...   142.0     152.0     100.0   1.01
gamma     0.143  0.033   0.090    0.200  ...   115.0     152.0      65.0   1.08
Is_begin  0.738  0.720   0.005    2.034  ...   121.0     122.0      81.0   1.06
Ia_begin  1.392  1.482   0.008    4.600  ...    39.0      17.0      24.0   1.11
E_begin   0.679  0.882   0.001    1.945  ...    90.0      39.0      30.0   1.06

[8 rows x 11 columns]