0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1493.69    33.45
p_loo       30.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.228   0.050   0.153    0.301  ...    45.0      47.0      60.0   1.09
pu         0.751   0.036   0.703    0.821  ...    58.0      68.0      58.0   1.04
mu         0.125   0.019   0.097    0.162  ...    30.0      31.0      60.0   1.02
mus        0.194   0.029   0.137    0.240  ...    46.0      49.0      58.0   1.01
gamma      0.308   0.057   0.205    0.408  ...   105.0      99.0      39.0   0.99
Is_begin   5.108   4.038   0.005   13.704  ...    82.0      55.0      17.0   1.02
Ia_begin  17.702   7.580   7.199   33.444  ...   102.0     101.0      93.0   1.01
E_begin   18.604  11.840   2.169   44.475  ...    50.0      49.0      46.0   1.03

[8 rows x 11 columns]