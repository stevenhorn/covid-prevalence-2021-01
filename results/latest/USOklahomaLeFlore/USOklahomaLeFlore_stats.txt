0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -891.28    27.28
p_loo       20.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.057   0.155    0.336  ...   152.0     107.0      60.0   1.01
pu        0.799  0.024   0.752    0.841  ...   152.0     152.0      87.0   1.02
mu        0.113  0.024   0.079    0.168  ...    27.0      22.0      32.0   1.07
mus       0.154  0.026   0.116    0.220  ...    88.0     147.0      54.0   1.00
gamma     0.174  0.038   0.123    0.268  ...    83.0     117.0      20.0   1.03
Is_begin  0.595  0.620   0.010    1.883  ...   109.0     105.0      88.0   1.06
Ia_begin  1.245  1.276   0.014    4.001  ...   108.0     123.0      60.0   0.98
E_begin   0.541  0.616   0.013    2.004  ...   125.0      90.0      60.0   0.99

[8 rows x 11 columns]