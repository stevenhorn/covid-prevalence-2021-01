0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1028.80    36.37
p_loo       22.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.053   0.178    0.348  ...    49.0      41.0      40.0   1.03
pu        0.846  0.016   0.820    0.870  ...    66.0      65.0     100.0   1.03
mu        0.102  0.025   0.064    0.148  ...     5.0       5.0      40.0   1.42
mus       0.184  0.034   0.135    0.260  ...   113.0     144.0      53.0   1.01
gamma     0.209  0.040   0.141    0.274  ...   152.0     152.0      77.0   1.01
Is_begin  0.738  0.672   0.040    2.097  ...    97.0     102.0      76.0   0.99
Ia_begin  0.537  0.472   0.030    1.583  ...   133.0      86.0      20.0   1.04
E_begin   0.459  0.538   0.006    1.299  ...   107.0      79.0      93.0   0.98

[8 rows x 11 columns]