0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -905.28    46.09
p_loo       39.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.053   0.162    0.340  ...   143.0     152.0      73.0   1.01
pu        0.877  0.015   0.849    0.898  ...    39.0      29.0      55.0   1.08
mu        0.116  0.017   0.089    0.147  ...    20.0      22.0      60.0   1.09
mus       0.169  0.040   0.116    0.256  ...   133.0     148.0     100.0   1.00
gamma     0.242  0.056   0.141    0.329  ...    51.0      71.0      59.0   1.04
Is_begin  0.980  0.768   0.072    2.455  ...    30.0      17.0      21.0   1.08
Ia_begin  1.971  2.095   0.000    7.065  ...    87.0      28.0      17.0   1.06
E_begin   1.078  1.148   0.003    3.429  ...   110.0      20.0      56.0   1.07

[8 rows x 11 columns]