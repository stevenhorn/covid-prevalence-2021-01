0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -660.09    37.44
p_loo       26.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      255   86.4%
 (0.5, 0.7]   (ok)         35   11.9%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.054   0.153    0.339  ...    74.0      74.0      54.0   1.02
pu        0.795  0.025   0.737    0.831  ...    66.0      82.0      56.0   1.12
mu        0.140  0.029   0.098    0.187  ...    36.0      31.0      59.0   1.06
mus       0.174  0.036   0.108    0.228  ...    85.0      91.0      59.0   0.99
gamma     0.204  0.044   0.132    0.276  ...   121.0     123.0      22.0   1.00
Is_begin  0.555  0.429   0.021    1.276  ...   125.0     117.0      96.0   1.04
Ia_begin  1.169  1.278   0.022    3.438  ...   112.0     141.0      60.0   1.00
E_begin   0.477  0.500   0.001    1.480  ...   108.0     130.0      80.0   1.00

[8 rows x 11 columns]