0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -331.13    24.62
p_loo       24.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.047   0.182    0.342  ...    48.0      50.0      58.0   1.01
pu        0.863  0.018   0.832    0.897  ...    22.0      22.0      40.0   1.07
mu        0.120  0.023   0.074    0.155  ...    35.0      33.0      60.0   1.06
mus       0.197  0.039   0.135    0.267  ...    64.0      73.0     100.0   1.01
gamma     0.238  0.057   0.153    0.346  ...    70.0      99.0      59.0   1.07
Is_begin  0.422  0.705   0.001    0.978  ...    51.0      27.0      40.0   1.07
Ia_begin  0.720  0.693   0.004    1.997  ...    41.0      30.0      58.0   1.06
E_begin   0.301  0.317   0.002    1.014  ...    48.0      35.0      43.0   1.06

[8 rows x 11 columns]