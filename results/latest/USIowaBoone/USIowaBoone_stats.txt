0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -754.82    20.90
p_loo       21.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.053   0.158    0.328  ...   152.0     152.0      18.0   1.00
pu        0.827  0.032   0.768    0.875  ...    55.0      72.0      93.0   1.04
mu        0.112  0.024   0.072    0.153  ...    41.0      41.0      60.0   0.99
mus       0.158  0.029   0.110    0.211  ...   152.0     152.0      88.0   0.99
gamma     0.168  0.032   0.115    0.227  ...   152.0     133.0      93.0   1.04
Is_begin  0.707  0.748   0.003    2.506  ...    81.0     152.0      86.0   1.01
Ia_begin  1.316  1.279   0.023    4.159  ...   130.0     121.0      40.0   1.00
E_begin   0.687  0.832   0.007    2.259  ...   138.0     101.0      87.0   1.00

[8 rows x 11 columns]