0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1033.04    26.48
p_loo       25.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.051   0.174    0.339  ...    49.0      47.0      40.0   1.01
pu        0.842  0.016   0.820    0.871  ...    33.0      37.0      72.0   1.05
mu        0.123  0.017   0.094    0.151  ...    11.0      12.0      59.0   1.15
mus       0.171  0.028   0.132    0.224  ...    76.0      92.0      54.0   1.04
gamma     0.232  0.037   0.184    0.318  ...   152.0     152.0     100.0   1.00
Is_begin  0.744  0.831   0.005    2.349  ...    92.0      33.0      36.0   1.05
Ia_begin  1.324  1.342   0.001    3.964  ...    39.0      30.0      58.0   1.05
E_begin   0.705  0.756   0.003    2.403  ...    83.0      30.0      60.0   1.05

[8 rows x 11 columns]