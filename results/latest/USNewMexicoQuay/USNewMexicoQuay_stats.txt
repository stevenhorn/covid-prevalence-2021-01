0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -475.31    28.05
p_loo       22.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.282  0.043   0.204    0.347  ...    67.0      68.0      58.0   1.01
pu        0.888  0.014   0.860    0.900  ...    37.0      25.0      59.0   1.07
mu        0.138  0.023   0.096    0.176  ...    15.0      16.0      91.0   1.09
mus       0.176  0.032   0.121    0.227  ...    51.0      53.0      54.0   1.06
gamma     0.220  0.039   0.154    0.294  ...    65.0      72.0      46.0   1.02
Is_begin  0.569  0.591   0.015    1.584  ...   104.0     103.0      59.0   0.99
Ia_begin  1.176  1.395   0.080    3.781  ...   107.0      84.0      91.0   1.02
E_begin   0.580  0.752   0.013    1.762  ...   107.0     123.0      88.0   1.02

[8 rows x 11 columns]