0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -999.57    25.53
p_loo       24.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.059   0.151    0.331  ...    77.0      61.0      57.0   1.02
pu        0.783  0.049   0.703    0.852  ...    68.0      76.0     100.0   1.03
mu        0.113  0.023   0.071    0.149  ...    22.0      27.0      40.0   1.06
mus       0.163  0.038   0.097    0.229  ...    71.0     144.0      40.0   1.01
gamma     0.204  0.035   0.135    0.258  ...   123.0     148.0      91.0   0.99
Is_begin  1.595  1.168   0.002    3.876  ...    56.0      47.0      42.0   1.02
Ia_begin  5.154  2.986   0.504   10.517  ...    60.0      64.0      39.0   1.06
E_begin   4.365  3.592   0.077   11.961  ...    71.0      63.0      60.0   1.07

[8 rows x 11 columns]