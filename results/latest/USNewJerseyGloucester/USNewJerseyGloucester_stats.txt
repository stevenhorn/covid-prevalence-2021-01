0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1232.42    27.53
p_loo       26.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.053   0.162    0.343  ...    70.0     117.0      49.0   1.07
pu        0.746  0.035   0.700    0.807  ...   152.0     152.0      66.0   1.05
mu        0.109  0.022   0.072    0.147  ...    63.0      54.0      60.0   1.02
mus       0.174  0.035   0.110    0.241  ...   140.0     152.0      45.0   1.00
gamma     0.278  0.047   0.190    0.351  ...   152.0     110.0      60.0   1.01
Is_begin  1.018  0.769   0.011    2.531  ...   125.0     106.0      60.0   1.01
Ia_begin  3.814  2.162   0.338    7.118  ...   152.0     152.0      83.0   1.04
E_begin   4.323  4.075   0.021   12.432  ...    58.0      57.0      86.0   1.03

[8 rows x 11 columns]