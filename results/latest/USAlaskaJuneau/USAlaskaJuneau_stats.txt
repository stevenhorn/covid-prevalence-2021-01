0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -736.92    34.27
p_loo       30.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.053   0.179    0.343  ...    47.0      42.0      42.0   1.03
pu        0.881  0.016   0.848    0.900  ...    19.0      26.0      51.0   1.05
mu        0.133  0.035   0.075    0.193  ...     4.0       4.0      24.0   1.58
mus       0.171  0.037   0.116    0.250  ...    70.0      79.0      60.0   1.07
gamma     0.202  0.034   0.147    0.264  ...   102.0      91.0      56.0   0.98
Is_begin  1.062  0.870   0.058    2.694  ...    83.0      63.0      38.0   1.05
Ia_begin  2.293  1.819   0.087    6.152  ...    36.0      23.0      19.0   1.12
E_begin   1.201  1.163   0.070    3.947  ...    29.0      54.0      59.0   1.03

[8 rows x 11 columns]