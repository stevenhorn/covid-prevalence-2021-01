0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1256.34    59.92
p_loo       33.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.055   0.152    0.335  ...   111.0     103.0      35.0   1.01
pu        0.799  0.036   0.718    0.855  ...    67.0      76.0      59.0   1.11
mu        0.144  0.025   0.093    0.180  ...    11.0      11.0      24.0   1.16
mus       0.216  0.033   0.157    0.277  ...    99.0     127.0      54.0   1.02
gamma     0.296  0.056   0.198    0.380  ...    97.0      99.0      54.0   0.98
Is_begin  0.999  0.962   0.034    2.640  ...    84.0      65.0      36.0   1.06
Ia_begin  0.850  0.615   0.089    2.055  ...   152.0     152.0      72.0   1.06
E_begin   0.832  0.781   0.005    2.573  ...   124.0     106.0      57.0   1.01

[8 rows x 11 columns]