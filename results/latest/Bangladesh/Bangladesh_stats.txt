0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1907.70    31.94
p_loo       26.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.5%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.053   0.169    0.346  ...   152.0     152.0      96.0   1.01
pu        0.764  0.051   0.702    0.868  ...    89.0      82.0      54.0   1.00
mu        0.083  0.020   0.050    0.121  ...     4.0       4.0      15.0   1.48
mus       0.142  0.030   0.092    0.200  ...    53.0      56.0      96.0   1.01
gamma     0.271  0.048   0.196    0.352  ...   152.0     146.0      86.0   1.02
Is_begin  2.385  1.929   0.078    5.637  ...     6.0       6.0      38.0   1.37
Ia_begin  5.896  3.660   0.538   12.798  ...    46.0      40.0      43.0   1.04
E_begin   5.025  4.099   0.038   14.204  ...    67.0      63.0      56.0   0.99

[8 rows x 11 columns]