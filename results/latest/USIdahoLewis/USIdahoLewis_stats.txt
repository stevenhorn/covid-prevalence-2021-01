0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -485.90    34.50
p_loo       28.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.052   0.163    0.326  ...    85.0      90.0      35.0   0.99
pu        0.736  0.020   0.701    0.765  ...    66.0      60.0      58.0   1.02
mu        0.129  0.021   0.091    0.167  ...    35.0      35.0      58.0   1.04
mus       0.200  0.037   0.133    0.276  ...   152.0     152.0      53.0   1.00
gamma     0.229  0.042   0.158    0.302  ...   152.0     152.0      91.0   1.05
Is_begin  0.608  0.570   0.010    1.736  ...    41.0      31.0      43.0   1.05
Ia_begin  1.070  1.117   0.031    3.499  ...    40.0      36.0      96.0   1.02
E_begin   0.415  0.458   0.001    1.191  ...    45.0      31.0      17.0   1.05

[8 rows x 11 columns]