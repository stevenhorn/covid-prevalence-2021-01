0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -778.38    40.60
p_loo       21.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.055   0.179    0.348  ...   101.0     106.0      37.0   1.01
pu        0.872  0.012   0.854    0.894  ...    65.0      63.0      59.0   1.03
mu        0.133  0.026   0.090    0.184  ...    31.0      30.0      85.0   1.03
mus       0.159  0.030   0.117    0.220  ...   103.0      99.0      51.0   1.00
gamma     0.185  0.041   0.131    0.254  ...   136.0      83.0      59.0   0.99
Is_begin  0.740  0.760   0.012    2.016  ...   129.0     109.0      79.0   1.00
Ia_begin  1.406  1.157   0.037    3.980  ...    80.0      71.0      60.0   1.01
E_begin   0.599  0.826   0.005    2.196  ...    89.0      39.0      60.0   1.05

[8 rows x 11 columns]