0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -628.44    26.82
p_loo       27.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.055   0.170    0.347  ...    68.0      63.0      54.0   1.12
pu        0.829  0.028   0.787    0.875  ...    34.0      40.0      40.0   1.04
mu        0.169  0.031   0.127    0.234  ...    12.0      15.0      58.0   1.15
mus       0.197  0.039   0.124    0.270  ...    59.0      56.0      33.0   1.05
gamma     0.224  0.040   0.158    0.299  ...   152.0     152.0      64.0   1.11
Is_begin  1.106  0.759   0.016    2.448  ...   152.0     145.0      96.0   1.00
Ia_begin  2.460  2.706   0.031    9.687  ...   121.0      48.0      43.0   1.02
E_begin   1.209  1.356   0.002    4.339  ...    50.0      26.0      44.0   1.04

[8 rows x 11 columns]