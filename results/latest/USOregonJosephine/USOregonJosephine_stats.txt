0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -733.76    27.87
p_loo       23.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.279  0.056   0.170    0.349  ...   115.0     123.0      60.0   1.00
pu        0.886  0.015   0.852    0.900  ...   152.0     152.0      62.0   1.02
mu        0.145  0.026   0.093    0.191  ...    25.0      23.0      40.0   1.05
mus       0.161  0.035   0.106    0.231  ...    64.0      74.0      59.0   1.05
gamma     0.180  0.041   0.120    0.245  ...   152.0     152.0      95.0   1.05
Is_begin  1.123  0.895   0.098    3.052  ...   123.0      84.0      37.0   1.04
Ia_begin  2.709  2.035   0.043    6.734  ...    71.0     101.0      83.0   1.03
E_begin   1.237  1.085   0.010    3.197  ...    93.0      51.0      61.0   1.03

[8 rows x 11 columns]