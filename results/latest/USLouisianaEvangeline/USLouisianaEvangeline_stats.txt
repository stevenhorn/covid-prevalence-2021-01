0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1110.62    40.44
p_loo       32.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.056   0.166    0.344  ...   110.0     108.0      60.0   1.03
pu        0.829  0.028   0.772    0.870  ...    43.0      45.0      60.0   1.03
mu        0.119  0.017   0.094    0.161  ...    45.0      45.0      59.0   0.99
mus       0.158  0.028   0.115    0.226  ...    47.0      43.0      59.0   1.05
gamma     0.197  0.033   0.142    0.264  ...   113.0     140.0     100.0   1.01
Is_begin  0.777  0.632   0.017    2.173  ...   104.0     108.0      60.0   0.98
Ia_begin  0.586  0.437   0.003    1.332  ...    69.0      61.0      60.0   1.03
E_begin   0.481  0.390   0.045    1.332  ...    78.0      84.0      60.0   1.01

[8 rows x 11 columns]