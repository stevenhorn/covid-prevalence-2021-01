0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1359.52    32.13
p_loo       24.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   92.6%
 (0.5, 0.7]   (ok)         17    5.5%
   (0.7, 1]   (bad)         5    1.6%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.248   0.054   0.152    0.322  ...   152.0     125.0      60.0   1.00
pu         0.769   0.042   0.708    0.853  ...   102.0     100.0      59.0   1.00
mu         0.151   0.023   0.112    0.195  ...    59.0      57.0      59.0   1.00
mus        0.192   0.026   0.145    0.244  ...   152.0     152.0      74.0   1.03
gamma      0.272   0.045   0.197    0.350  ...    83.0      88.0     100.0   1.01
Is_begin   5.202   3.315   0.093   11.294  ...    71.0      56.0      42.0   1.05
Ia_begin  13.362   6.382   2.467   23.418  ...   142.0     133.0      72.0   1.00
E_begin   12.474  10.081   0.150   32.238  ...    95.0      77.0      57.0   0.98

[8 rows x 11 columns]