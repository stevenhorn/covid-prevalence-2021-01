0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -883.88    27.37
p_loo       25.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.242   0.056   0.163    0.350  ...    37.0      53.0      15.0   1.06
pu         0.739   0.031   0.701    0.796  ...   152.0     152.0      96.0   1.02
mu         0.155   0.028   0.112    0.221  ...    20.0      24.0      80.0   1.06
mus        0.235   0.037   0.174    0.303  ...   131.0     142.0      91.0   0.98
gamma      0.344   0.042   0.285    0.442  ...   152.0     152.0     100.0   0.99
Is_begin   4.744   2.845   0.604    9.970  ...   151.0      95.0      43.0   1.02
Ia_begin  12.048   5.601   2.181   20.895  ...    94.0      62.0      40.0   1.08
E_begin   25.396  14.997   5.578   57.719  ...   101.0     116.0      93.0   1.01

[8 rows x 11 columns]