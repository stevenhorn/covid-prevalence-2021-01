0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1623.55    23.67
p_loo       19.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.5%
 (0.5, 0.7]   (ok)         22    7.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.053   0.162    0.333  ...    88.0      75.0      58.0   1.02
pu        0.797  0.045   0.713    0.863  ...    64.0      70.0      57.0   1.03
mu        0.143  0.021   0.114    0.183  ...     6.0       7.0      58.0   1.27
mus       0.163  0.028   0.114    0.215  ...    39.0      38.0      36.0   1.02
gamma     0.227  0.037   0.163    0.295  ...    63.0      65.0      59.0   1.00
Is_begin  0.869  0.658   0.059    2.144  ...   113.0      93.0      38.0   1.06
Ia_begin  2.151  1.881   0.022    5.124  ...    99.0      92.0      72.0   1.01
E_begin   1.744  1.779   0.012    5.431  ...    85.0      84.0      87.0   1.01

[8 rows x 11 columns]