1 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -748.07    21.24
p_loo       17.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.052   0.171    0.338  ...    29.0      31.0      22.0   1.11
pu        0.876  0.029   0.811    0.899  ...    14.0      21.0      51.0   1.08
mu        0.127  0.033   0.077    0.177  ...     3.0       4.0      55.0   1.75
mus       0.158  0.029   0.106    0.208  ...    52.0      59.0      40.0   1.01
gamma     0.169  0.032   0.114    0.232  ...    36.0      28.0      53.0   1.08
Is_begin  0.990  0.750   0.033    2.560  ...    16.0      14.0      15.0   1.11
Ia_begin  2.303  1.810   0.089    5.599  ...    47.0      33.0      18.0   1.04
E_begin   1.390  1.277   0.047    3.896  ...    57.0      18.0      87.0   1.12

[8 rows x 11 columns]