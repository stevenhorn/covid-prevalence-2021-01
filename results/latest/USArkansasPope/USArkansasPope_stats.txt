0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1033.93    22.35
p_loo       21.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.049   0.164    0.328  ...    66.0      67.0      59.0   1.01
pu        0.776  0.025   0.724    0.819  ...    21.0      31.0      40.0   1.06
mu        0.105  0.024   0.074    0.161  ...    25.0      26.0      22.0   1.10
mus       0.144  0.028   0.088    0.195  ...    93.0      95.0      44.0   1.00
gamma     0.151  0.036   0.100    0.214  ...   123.0     152.0      35.0   1.00
Is_begin  0.673  0.626   0.007    1.851  ...    88.0      65.0      60.0   1.02
Ia_begin  1.258  1.399   0.015    3.308  ...    89.0      77.0      58.0   1.01
E_begin   0.670  0.664   0.004    1.855  ...   138.0     116.0      87.0   1.05

[8 rows x 11 columns]