0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -876.34    36.72
p_loo       22.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   96.3%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.024   0.150    0.222  ...    22.0       9.0      32.0   1.17
pu        0.710  0.010   0.700    0.729  ...    53.0      59.0      75.0   1.04
mu        0.116  0.022   0.077    0.160  ...    71.0      67.0      59.0   1.04
mus       0.170  0.025   0.122    0.211  ...    54.0      49.0      53.0   1.04
gamma     0.194  0.037   0.118    0.249  ...    80.0      88.0      77.0   1.03
Is_begin  0.260  0.364   0.003    0.617  ...    98.0      65.0      43.0   1.11
Ia_begin  0.362  0.324   0.026    0.795  ...    43.0      37.0      59.0   1.05
E_begin   0.152  0.183   0.002    0.448  ...    71.0      45.0      38.0   1.03

[8 rows x 11 columns]