0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1041.79    33.40
p_loo       29.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.053   0.153    0.328  ...    96.0      91.0      93.0   0.99
pu        0.827  0.019   0.788    0.855  ...    59.0      58.0      59.0   1.01
mu        0.114  0.018   0.080    0.142  ...    36.0      33.0      31.0   1.01
mus       0.173  0.031   0.119    0.226  ...    97.0     105.0     100.0   1.00
gamma     0.214  0.038   0.152    0.291  ...   120.0     152.0      60.0   1.03
Is_begin  0.646  0.584   0.008    1.755  ...   152.0     143.0     100.0   1.01
Ia_begin  1.075  0.969   0.003    3.229  ...    98.0      85.0      97.0   1.01
E_begin   0.617  0.591   0.007    1.668  ...   103.0      68.0      24.0   1.02

[8 rows x 11 columns]