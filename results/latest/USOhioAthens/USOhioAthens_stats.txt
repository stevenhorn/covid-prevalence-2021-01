0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -814.66    28.53
p_loo       26.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.044   0.183    0.333  ...   152.0     152.0      93.0   1.02
pu        0.877  0.015   0.850    0.898  ...    48.0      33.0      59.0   1.06
mu        0.186  0.028   0.136    0.231  ...    10.0      11.0      77.0   1.18
mus       0.227  0.035   0.154    0.279  ...   116.0     101.0      60.0   1.03
gamma     0.353  0.072   0.242    0.452  ...   152.0     122.0      49.0   1.06
Is_begin  0.782  0.661   0.031    1.907  ...    84.0     108.0      83.0   0.99
Ia_begin  1.755  1.829   0.034    5.261  ...    96.0      88.0      49.0   1.02
E_begin   1.034  1.213   0.005    2.836  ...   108.0      77.0      42.0   1.00

[8 rows x 11 columns]