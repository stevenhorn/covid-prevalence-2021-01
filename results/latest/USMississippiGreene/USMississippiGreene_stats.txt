0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -782.85    42.33
p_loo       36.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.057   0.165    0.345  ...    54.0      46.0      24.0   0.99
pu        0.831  0.025   0.779    0.869  ...    71.0      82.0      60.0   1.00
mu        0.113  0.018   0.082    0.146  ...    51.0      50.0      91.0   1.04
mus       0.192  0.031   0.130    0.252  ...    87.0     106.0      59.0   1.02
gamma     0.258  0.046   0.178    0.331  ...   131.0     142.0     100.0   0.98
Is_begin  0.452  0.508   0.010    1.583  ...    77.0      30.0      97.0   1.05
Ia_begin  0.735  0.805   0.013    2.005  ...    84.0      71.0      81.0   1.00
E_begin   0.368  0.403   0.008    1.136  ...    77.0      39.0      41.0   1.06

[8 rows x 11 columns]