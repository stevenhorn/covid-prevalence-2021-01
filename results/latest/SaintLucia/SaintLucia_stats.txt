0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -541.85    33.14
p_loo       30.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.3%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.295  0.040   0.209    0.345  ...    55.0      63.0      59.0   1.05
pu        0.886  0.017   0.848    0.900  ...    87.0      94.0      40.0   1.05
mu        0.170  0.028   0.120    0.223  ...     7.0       8.0      24.0   1.22
mus       0.195  0.035   0.137    0.256  ...   139.0     123.0      60.0   1.01
gamma     0.275  0.047   0.191    0.356  ...    27.0      30.0      19.0   1.06
Is_begin  1.436  1.116   0.068    3.601  ...   114.0      81.0      59.0   1.04
Ia_begin  0.738  0.618   0.011    1.763  ...    65.0      67.0      58.0   1.01
E_begin   0.798  0.788   0.002    2.507  ...    77.0      41.0      60.0   1.03

[8 rows x 11 columns]