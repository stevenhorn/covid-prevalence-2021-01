0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -673.08    25.41
p_loo       30.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   90.3%
 (0.5, 0.7]   (ok)         25    8.1%
   (0.7, 1]   (bad)         5    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.053   0.165    0.348  ...    51.0      54.0      30.0   1.07
pu        0.788  0.047   0.703    0.876  ...    15.0      15.0      31.0   1.09
mu        0.223  0.031   0.169    0.277  ...    10.0      15.0      14.0   1.24
mus       0.296  0.044   0.195    0.362  ...    12.0      13.0      19.0   1.11
gamma     0.502  0.073   0.389    0.655  ...   152.0     152.0      62.0   1.04
Is_begin  1.895  1.471   0.018    4.952  ...    43.0      29.0      45.0   1.07
Ia_begin  6.060  3.915   0.584   13.084  ...   100.0      87.0      58.0   1.01
E_begin   5.166  4.952   0.186   14.030  ...    20.0      18.0      60.0   1.08

[8 rows x 11 columns]