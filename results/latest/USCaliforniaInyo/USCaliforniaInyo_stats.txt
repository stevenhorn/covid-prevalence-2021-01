0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -686.38    28.71
p_loo       31.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.295  0.049   0.186    0.350  ...    42.0      39.0      49.0   1.00
pu        0.891  0.009   0.869    0.899  ...    32.0      24.0      91.0   1.07
mu        0.171  0.030   0.122    0.223  ...    18.0      19.0      24.0   1.06
mus       0.198  0.027   0.155    0.250  ...    60.0      61.0      91.0   1.00
gamma     0.300  0.067   0.179    0.412  ...    91.0     118.0      58.0   1.01
Is_begin  0.966  0.639   0.034    2.143  ...   100.0      82.0      58.0   0.99
Ia_begin  2.312  2.216   0.073    6.751  ...    36.0     143.0      31.0   1.01
E_begin   1.226  1.275   0.045    3.450  ...    60.0      76.0      52.0   1.06

[8 rows x 11 columns]