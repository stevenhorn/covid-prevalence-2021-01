0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -878.78    26.14
p_loo       22.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.277  0.051   0.179    0.349  ...   101.0      87.0      84.0   1.00
pu        0.881  0.018   0.844    0.899  ...    77.0     105.0      49.0   1.00
mu        0.150  0.030   0.095    0.202  ...    25.0      26.0      47.0   1.06
mus       0.164  0.029   0.114    0.218  ...   117.0      95.0      91.0   1.01
gamma     0.215  0.043   0.146    0.289  ...   111.0      92.0      69.0   1.02
Is_begin  1.205  1.056   0.007    3.015  ...   135.0      94.0      40.0   1.00
Ia_begin  2.957  2.596   0.025    7.577  ...   112.0     110.0      59.0   1.06
E_begin   1.276  1.136   0.014    3.842  ...    53.0      51.0      60.0   1.00

[8 rows x 11 columns]