0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -967.44    25.15
p_loo       17.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.056   0.151    0.341  ...    78.0      70.0      38.0   1.09
pu        0.842  0.014   0.819    0.864  ...   116.0     106.0      91.0   1.03
mu        0.125  0.025   0.077    0.162  ...    27.0      29.0      40.0   1.05
mus       0.158  0.032   0.101    0.206  ...    92.0      87.0      93.0   0.98
gamma     0.169  0.031   0.116    0.221  ...    87.0      82.0      72.0   1.02
Is_begin  0.863  0.688   0.017    2.057  ...    94.0      96.0      60.0   0.98
Ia_begin  0.481  0.533   0.002    1.441  ...    98.0      17.0      33.0   1.09
E_begin   0.570  0.534   0.005    1.444  ...   107.0      83.0      67.0   1.01

[8 rows x 11 columns]