0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -616.98    22.20
p_loo       18.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.050   0.180    0.334  ...   152.0     152.0      48.0   1.00
pu        0.862  0.019   0.829    0.894  ...    61.0      61.0      48.0   1.00
mu        0.115  0.019   0.080    0.149  ...    63.0      61.0     100.0   1.01
mus       0.167  0.029   0.115    0.216  ...    90.0      90.0      97.0   1.00
gamma     0.196  0.043   0.135    0.287  ...    73.0      90.0      57.0   1.04
Is_begin  0.804  0.722   0.010    2.309  ...   148.0      93.0      23.0   1.04
Ia_begin  1.605  1.530   0.043    4.635  ...   120.0     152.0      96.0   1.01
E_begin   0.914  0.789   0.004    2.739  ...    79.0      49.0      22.0   1.04

[8 rows x 11 columns]