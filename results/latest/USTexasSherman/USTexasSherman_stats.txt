0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -358.65    30.15
p_loo       25.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.051   0.171    0.348  ...    77.0      81.0      60.0   1.02
pu        0.805  0.046   0.720    0.885  ...    10.0      11.0      55.0   1.14
mu        0.127  0.029   0.089    0.188  ...    83.0      87.0      28.0   1.10
mus       0.163  0.023   0.128    0.217  ...    76.0      92.0      59.0   0.98
gamma     0.205  0.050   0.130    0.285  ...    62.0      77.0      60.0   1.00
Is_begin  0.715  0.811   0.007    2.201  ...    61.0      55.0      36.0   1.02
Ia_begin  1.849  1.642   0.022    4.806  ...    37.0      28.0      30.0   0.99
E_begin   0.894  1.007   0.045    2.855  ...    52.0      37.0      30.0   0.98

[8 rows x 11 columns]