0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -648.53    35.57
p_loo       29.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.056   0.157    0.349  ...    79.0      71.0      41.0   1.05
pu        0.799  0.025   0.744    0.835  ...    77.0      73.0      83.0   1.02
mu        0.127  0.024   0.085    0.171  ...    87.0      94.0      83.0   1.00
mus       0.163  0.035   0.105    0.218  ...    98.0     112.0      59.0   1.07
gamma     0.173  0.031   0.113    0.220  ...    37.0      34.0      66.0   1.04
Is_begin  0.222  0.321   0.001    0.818  ...    55.0      41.0     100.0   1.01
Ia_begin  0.479  0.739   0.001    2.106  ...    47.0      88.0      46.0   1.00
E_begin   0.230  0.543   0.001    0.749  ...    80.0      70.0      60.0   1.00

[8 rows x 11 columns]