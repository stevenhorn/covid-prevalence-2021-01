0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1732.08    35.48
p_loo       27.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      258   87.2%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)        12    4.1%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.254   0.054   0.160    0.333  ...   152.0     152.0      47.0   1.06
pu         0.811   0.056   0.717    0.899  ...    79.0      85.0      60.0   1.07
mu         0.155   0.021   0.118    0.190  ...    35.0      34.0      57.0   1.03
mus        0.194   0.029   0.146    0.250  ...   152.0     152.0      21.0   1.03
gamma      0.315   0.044   0.241    0.397  ...   129.0     138.0      64.0   1.01
Is_begin  15.243  11.555   0.132   34.942  ...   152.0      80.0      53.0   1.00
Ia_begin  38.583  19.117   4.960   71.992  ...    57.0      52.0      59.0   1.04
E_begin   36.829  29.419   1.561   90.103  ...   152.0     125.0      86.0   0.99

[8 rows x 11 columns]