0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1444.69    26.68
p_loo       85.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    2    0.7%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.318   0.036   0.246    0.348  ...     4.0       6.0      20.0   1.70
pu          0.848   0.046   0.800    0.899  ...     3.0       3.0      57.0   1.91
mu          0.268   0.037   0.210    0.304  ...     3.0       4.0      40.0   1.83
mus         0.208   0.020   0.162    0.233  ...    14.0      11.0      42.0   1.54
gamma       0.218   0.120   0.103    0.382  ...     3.0       3.0      14.0   1.89
Is_begin   15.616   4.539   6.388   23.159  ...    24.0      16.0      73.0   1.50
Ia_begin   27.111  21.064   6.862   61.203  ...     3.0       3.0      57.0   2.05
E_begin   102.608  76.066  35.981  243.847  ...     3.0       3.0      20.0   2.41

[8 rows x 11 columns]