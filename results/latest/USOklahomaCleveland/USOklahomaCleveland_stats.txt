0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1381.94    27.87
p_loo       25.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.054   0.175    0.350  ...    61.0      52.0      17.0   1.02
pu        0.745  0.034   0.700    0.811  ...    61.0      55.0      42.0   1.02
mu        0.134  0.023   0.097    0.174  ...     7.0       7.0      20.0   1.29
mus       0.188  0.033   0.136    0.257  ...    81.0     104.0      95.0   1.01
gamma     0.261  0.041   0.170    0.332  ...    83.0      87.0      60.0   1.00
Is_begin  2.653  2.025   0.101    6.392  ...    74.0      63.0      60.0   1.07
Ia_begin  5.992  3.636   0.852   12.579  ...    63.0      58.0      59.0   1.00
E_begin   5.235  5.300   0.138   15.712  ...    94.0      62.0      43.0   1.00

[8 rows x 11 columns]