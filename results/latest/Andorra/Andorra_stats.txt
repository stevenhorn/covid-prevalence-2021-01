0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1291.68    40.89
p_loo       27.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.2%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.207   0.051   0.151    0.302  ...   103.0      87.0      59.0   0.98
pu         0.725   0.021   0.701    0.766  ...    76.0      62.0      60.0   1.04
mu         0.145   0.030   0.096    0.194  ...     6.0       6.0      80.0   1.34
mus        0.215   0.037   0.150    0.278  ...   114.0     105.0     100.0   1.02
gamma      0.293   0.050   0.224    0.396  ...   110.0     128.0      73.0   1.02
Is_begin  22.705  13.211   0.538   47.344  ...    66.0      60.0      60.0   1.03
Ia_begin  57.561  34.285   2.013  110.065  ...    84.0      71.0      59.0   0.98
E_begin   45.296  36.832   0.176  116.756  ...    21.0      18.0      40.0   1.09

[8 rows x 11 columns]