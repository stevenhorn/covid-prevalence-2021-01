0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -921.86    52.62
p_loo       37.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.055   0.157    0.327  ...    75.0      72.0      57.0   1.05
pu        0.749  0.027   0.702    0.794  ...    96.0      92.0      54.0   0.99
mu        0.136  0.022   0.096    0.177  ...    27.0      27.0      56.0   1.06
mus       0.201  0.036   0.143    0.279  ...    57.0      60.0      60.0   1.01
gamma     0.261  0.049   0.179    0.352  ...    29.0      49.0      65.0   1.03
Is_begin  0.975  0.806   0.019    2.447  ...    59.0     152.0      24.0   1.18
Ia_begin  1.677  1.261   0.099    3.848  ...    85.0      90.0      59.0   0.99
E_begin   0.958  1.066   0.082    3.046  ...    33.0      55.0      93.0   1.05

[8 rows x 11 columns]