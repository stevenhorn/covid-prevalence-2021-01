0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.41    57.34
p_loo       47.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.048   0.155    0.316  ...    37.0      36.0      31.0   1.03
pu        0.725  0.019   0.701    0.758  ...    19.0      27.0      97.0   1.08
mu        0.141  0.028   0.098    0.184  ...    27.0      27.0      40.0   1.08
mus       0.206  0.042   0.136    0.289  ...    53.0      54.0      56.0   1.05
gamma     0.240  0.045   0.161    0.319  ...    66.0      69.0      59.0   1.03
Is_begin  0.393  0.402   0.003    1.072  ...    88.0      80.0      48.0   1.04
Ia_begin  0.664  1.075   0.002    2.494  ...   104.0      54.0      43.0   1.03
E_begin   0.315  0.428   0.004    1.331  ...   108.0      74.0     100.0   1.03

[8 rows x 11 columns]