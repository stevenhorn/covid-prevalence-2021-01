1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1384.54    34.50
p_loo       26.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.050   0.175    0.344  ...   152.0     152.0      95.0   0.99
pu        0.803  0.033   0.736    0.858  ...    43.0      54.0      33.0   1.01
mu        0.129  0.022   0.089    0.162  ...    31.0      31.0      40.0   1.03
mus       0.179  0.045   0.122    0.243  ...    50.0      69.0      59.0   1.00
gamma     0.241  0.054   0.150    0.354  ...    93.0     117.0      93.0   0.99
Is_begin  1.245  1.150   0.000    3.479  ...    80.0      61.0      60.0   0.99
Ia_begin  2.092  2.195   0.030    7.609  ...   104.0      83.0      96.0   0.99
E_begin   1.048  1.107   0.038    3.260  ...    99.0      84.0      84.0   1.02

[8 rows x 11 columns]