1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -535.41    26.17
p_loo       23.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.048   0.183    0.340  ...    23.0      24.0      54.0   1.05
pu        0.854  0.045   0.740    0.896  ...     8.0      13.0      17.0   1.25
mu        0.136  0.018   0.104    0.171  ...     8.0       8.0      21.0   1.23
mus       0.156  0.036   0.106    0.211  ...    13.0       8.0      34.0   1.24
gamma     0.204  0.033   0.136    0.260  ...   143.0     132.0      60.0   1.08
Is_begin  0.451  0.451   0.007    1.096  ...    58.0      36.0      83.0   1.00
Ia_begin  0.848  1.226   0.038    3.898  ...    18.0      19.0      51.0   1.08
E_begin   0.354  0.386   0.002    0.820  ...    34.0      37.0      38.0   1.02

[8 rows x 11 columns]