0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1077.93    26.46
p_loo       25.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.046   0.175    0.329  ...   152.0     152.0      59.0   1.00
pu        0.826  0.056   0.716    0.898  ...   111.0     110.0      60.0   1.01
mu        0.107  0.021   0.076    0.139  ...    25.0      28.0      60.0   1.00
mus       0.171  0.035   0.103    0.228  ...   145.0     123.0      43.0   1.06
gamma     0.211  0.037   0.145    0.265  ...    54.0      63.0      59.0   1.03
Is_begin  1.275  1.000   0.126    3.196  ...    70.0      48.0      97.0   1.05
Ia_begin  0.850  0.703   0.018    2.107  ...   108.0      88.0      96.0   1.00
E_begin   0.994  0.978   0.004    3.114  ...    61.0      24.0      14.0   1.07

[8 rows x 11 columns]