0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1028.51    41.85
p_loo       23.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.057   0.156    0.343  ...   101.0     101.0      87.0   1.04
pu        0.837  0.017   0.803    0.869  ...    65.0      67.0      59.0   1.02
mu        0.120  0.023   0.080    0.159  ...    47.0      44.0      33.0   1.07
mus       0.153  0.035   0.104    0.223  ...   152.0     152.0      53.0   1.00
gamma     0.154  0.042   0.075    0.222  ...    85.0     116.0      83.0   1.05
Is_begin  0.741  0.868   0.006    2.201  ...   106.0     152.0      88.0   1.00
Ia_begin  1.453  1.486   0.023    4.874  ...   117.0     150.0      60.0   1.02
E_begin   0.532  0.620   0.002    1.730  ...   100.0     108.0      40.0   0.99

[8 rows x 11 columns]