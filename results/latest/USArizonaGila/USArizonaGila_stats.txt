0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1040.80    28.03
p_loo       21.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.053   0.165    0.338  ...    76.0      70.0      60.0   1.00
pu        0.792  0.025   0.749    0.829  ...    51.0      56.0      35.0   1.01
mu        0.118  0.026   0.076    0.159  ...    16.0      18.0      60.0   1.07
mus       0.158  0.031   0.118    0.213  ...    85.0      86.0      59.0   1.01
gamma     0.180  0.031   0.118    0.233  ...    54.0      49.0      96.0   0.98
Is_begin  0.549  0.622   0.001    1.879  ...    80.0     102.0      59.0   0.99
Ia_begin  1.171  1.200   0.009    3.853  ...    37.0      36.0      40.0   1.08
E_begin   0.487  0.521   0.005    1.563  ...    26.0      54.0      40.0   1.09

[8 rows x 11 columns]