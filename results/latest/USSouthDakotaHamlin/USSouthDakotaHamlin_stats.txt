0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -492.49    27.59
p_loo       21.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.055   0.156    0.341  ...   152.0     152.0      88.0   1.01
pu        0.803  0.020   0.770    0.844  ...   124.0     124.0      57.0   0.99
mu        0.126  0.025   0.089    0.173  ...    27.0      24.0      59.0   1.07
mus       0.169  0.031   0.113    0.228  ...   152.0     152.0      93.0   1.01
gamma     0.186  0.038   0.128    0.248  ...   152.0     119.0      69.0   1.00
Is_begin  0.537  0.567   0.011    1.820  ...   110.0     105.0      56.0   1.01
Ia_begin  1.206  1.468   0.000    3.520  ...   111.0     103.0      54.0   0.99
E_begin   0.517  0.588   0.017    1.681  ...    90.0      78.0      59.0   0.99

[8 rows x 11 columns]