0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -636.22    29.14
p_loo       30.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.054   0.171    0.342  ...   152.0     152.0      67.0   1.02
pu        0.850  0.019   0.812    0.879  ...    72.0      63.0      48.0   0.99
mu        0.167  0.028   0.127    0.233  ...    17.0      18.0      56.0   1.08
mus       0.225  0.038   0.159    0.292  ...   152.0     152.0      62.0   1.04
gamma     0.312  0.057   0.217    0.426  ...    93.0      92.0      83.0   1.02
Is_begin  0.852  0.670   0.070    1.958  ...   128.0     152.0     100.0   1.01
Ia_begin  1.501  1.496   0.032    4.257  ...    69.0      49.0      78.0   1.06
E_begin   0.757  0.826   0.002    2.155  ...   108.0     120.0      91.0   1.01

[8 rows x 11 columns]