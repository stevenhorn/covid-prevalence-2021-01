0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -379.09    33.67
p_loo       26.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.297  0.040   0.221    0.349  ...   152.0     152.0      60.0   1.00
pu        0.891  0.012   0.868    0.900  ...   152.0     152.0      69.0   0.99
mu        0.155  0.036   0.104    0.226  ...    81.0      79.0      56.0   0.99
mus       0.169  0.027   0.129    0.221  ...   152.0     152.0      97.0   1.00
gamma     0.210  0.038   0.145    0.275  ...   105.0     113.0      93.0   0.99
Is_begin  1.081  0.791   0.002    2.550  ...   128.0      70.0      35.0   1.00
Ia_begin  2.520  2.030   0.030    6.743  ...    74.0      69.0      60.0   1.00
E_begin   1.431  1.461   0.016    4.538  ...    95.0      78.0      75.0   0.99

[8 rows x 11 columns]