0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -923.56    26.75
p_loo       22.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.057   0.151    0.336  ...    67.0      61.0      64.0   1.03
pu        0.856  0.035   0.786    0.898  ...    13.0      14.0      36.0   1.12
mu        0.113  0.023   0.076    0.154  ...    14.0      12.0      54.0   1.16
mus       0.156  0.026   0.118    0.208  ...    35.0      38.0      53.0   1.05
gamma     0.183  0.033   0.121    0.237  ...   152.0     152.0      87.0   1.09
Is_begin  0.834  0.717   0.020    2.187  ...   115.0      69.0      60.0   1.02
Ia_begin  2.117  2.107   0.010    5.275  ...    59.0      60.0      59.0   1.00
E_begin   1.305  1.482   0.009    4.130  ...    62.0      48.0      57.0   0.99

[8 rows x 11 columns]