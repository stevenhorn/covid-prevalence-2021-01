0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -626.57    30.48
p_loo       24.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   89.3%
 (0.5, 0.7]   (ok)         24    7.8%
   (0.7, 1]   (bad)         6    1.9%
   (1, Inf)   (very bad)    3    1.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.233   0.049   0.156    0.315  ...    40.0      36.0      40.0   1.12
pu         0.745   0.037   0.702    0.816  ...     8.0      13.0      18.0   1.12
mu         0.171   0.038   0.106    0.235  ...     7.0       8.0      56.0   1.23
mus        0.264   0.036   0.198    0.328  ...    79.0      77.0      59.0   1.06
gamma      0.334   0.058   0.242    0.468  ...   152.0     152.0      59.0   1.07
Is_begin   4.473   3.749   0.034   11.047  ...   123.0      94.0      60.0   0.99
Ia_begin  11.191  10.530   0.384   31.424  ...    50.0      45.0      53.0   0.99
E_begin    5.152   4.761   0.678   15.213  ...    64.0      54.0      56.0   1.06

[8 rows x 11 columns]