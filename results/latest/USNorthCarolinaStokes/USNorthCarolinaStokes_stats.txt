0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -806.84    30.94
p_loo       25.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.060   0.151    0.341  ...   118.0      63.0      36.0   1.01
pu        0.833  0.034   0.771    0.891  ...    64.0      65.0      59.0   1.03
mu        0.115  0.026   0.074    0.167  ...    15.0      30.0      46.0   1.08
mus       0.152  0.037   0.101    0.226  ...   116.0     152.0      76.0   1.13
gamma     0.159  0.031   0.112    0.225  ...   152.0     152.0      93.0   1.02
Is_begin  0.696  0.684   0.001    2.073  ...   133.0     152.0      25.0   1.06
Ia_begin  1.469  1.580   0.004    4.700  ...    84.0     102.0      96.0   1.00
E_begin   0.556  0.630   0.008    1.415  ...    99.0      79.0      59.0   0.99

[8 rows x 11 columns]