0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -473.11    35.90
p_loo       31.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.054   0.173    0.338  ...    48.0      45.0      56.0   1.02
pu        0.758  0.025   0.716    0.810  ...    84.0      87.0      59.0   1.01
mu        0.132  0.026   0.093    0.184  ...    20.0      20.0      77.0   1.10
mus       0.175  0.037   0.102    0.241  ...    49.0      52.0      39.0   1.04
gamma     0.221  0.037   0.142    0.276  ...    59.0      56.0      34.0   1.01
Is_begin  0.354  0.355   0.002    1.318  ...    34.0      40.0      33.0   1.08
Ia_begin  0.750  0.975   0.000    3.097  ...    85.0      43.0      20.0   1.04
E_begin   0.399  0.534   0.005    1.359  ...    51.0      70.0      47.0   1.01

[8 rows x 11 columns]