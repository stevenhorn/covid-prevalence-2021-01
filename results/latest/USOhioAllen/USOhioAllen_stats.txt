0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1083.20    30.79
p_loo       24.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.058   0.164    0.341  ...    96.0      98.0      25.0   1.09
pu        0.799  0.034   0.738    0.859  ...    70.0      67.0      38.0   1.02
mu        0.124  0.021   0.084    0.159  ...    14.0      15.0      48.0   1.11
mus       0.159  0.037   0.101    0.228  ...    70.0      90.0      57.0   1.01
gamma     0.195  0.035   0.141    0.261  ...    54.0      59.0      84.0   1.01
Is_begin  1.044  1.004   0.039    3.222  ...    47.0      46.0      45.0   1.04
Ia_begin  2.734  2.571   0.042    6.965  ...   124.0      93.0      29.0   0.99
E_begin   1.715  1.869   0.010    5.928  ...    99.0      66.0      88.0   1.02

[8 rows x 11 columns]