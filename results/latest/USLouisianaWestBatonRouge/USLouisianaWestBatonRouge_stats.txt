0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -962.31    28.89
p_loo       26.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.054   0.170    0.333  ...    78.0      76.0      56.0   1.01
pu        0.817  0.041   0.741    0.888  ...    11.0      10.0      58.0   1.22
mu        0.140  0.024   0.099    0.192  ...    37.0      35.0      42.0   1.03
mus       0.162  0.029   0.108    0.212  ...   107.0     135.0      59.0   1.13
gamma     0.217  0.042   0.142    0.292  ...    43.0      40.0      60.0   1.02
Is_begin  1.206  1.115   0.004    3.394  ...    22.0      17.0      49.0   1.11
Ia_begin  0.836  0.570   0.064    1.744  ...   118.0     125.0      96.0   1.00
E_begin   1.046  1.025   0.046    3.263  ...    64.0      47.0      81.0   1.01

[8 rows x 11 columns]