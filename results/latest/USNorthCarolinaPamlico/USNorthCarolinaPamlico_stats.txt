0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -610.40    35.02
p_loo       32.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.055   0.174    0.344  ...   131.0      96.0      19.0   1.04
pu        0.864  0.021   0.832    0.899  ...    23.0      17.0      54.0   1.09
mu        0.166  0.029   0.107    0.210  ...    28.0      29.0      60.0   1.07
mus       0.201  0.038   0.142    0.287  ...   152.0     152.0      60.0   1.07
gamma     0.255  0.048   0.175    0.328  ...   117.0     152.0      88.0   1.00
Is_begin  1.062  1.070   0.008    3.217  ...   109.0      32.0      59.0   1.05
Ia_begin  2.451  2.374   0.017    6.898  ...    59.0     111.0      45.0   1.04
E_begin   1.144  1.318   0.013    3.808  ...   101.0     131.0      53.0   1.00

[8 rows x 11 columns]