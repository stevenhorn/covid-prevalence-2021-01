0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1229.25    37.65
p_loo       28.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.050   0.156    0.330  ...   115.0     118.0      31.0   0.99
pu        0.771  0.027   0.722    0.813  ...    83.0      90.0      60.0   1.07
mu        0.138  0.023   0.088    0.179  ...     9.0      10.0      30.0   1.18
mus       0.187  0.032   0.118    0.230  ...   103.0     101.0      66.0   1.02
gamma     0.222  0.047   0.137    0.307  ...   152.0     152.0      84.0   1.02
Is_begin  0.797  0.672   0.021    2.195  ...   140.0     115.0      91.0   1.01
Ia_begin  1.540  1.263   0.025    3.841  ...    74.0      67.0      40.0   1.01
E_begin   0.809  0.764   0.009    2.263  ...    94.0      88.0      67.0   1.00

[8 rows x 11 columns]