0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -482.82    44.41
p_loo       28.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.047   0.169    0.323  ...   123.0     127.0      60.0   0.98
pu        0.786  0.019   0.752    0.816  ...   110.0     105.0      95.0   1.02
mu        0.127  0.024   0.085    0.172  ...    72.0      67.0      58.0   0.99
mus       0.183  0.025   0.142    0.237  ...   119.0     123.0      59.0   0.99
gamma     0.215  0.038   0.136    0.266  ...   129.0     116.0      40.0   1.01
Is_begin  0.564  0.589   0.018    1.431  ...   106.0      97.0      57.0   1.00
Ia_begin  0.996  0.951   0.023    2.900  ...   115.0      93.0      60.0   1.00
E_begin   0.456  0.528   0.008    1.467  ...    78.0      55.0      24.0   1.03

[8 rows x 11 columns]