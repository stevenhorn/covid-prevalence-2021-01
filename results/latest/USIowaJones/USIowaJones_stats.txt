0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -907.74    64.98
p_loo       49.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.213  0.044   0.151    0.304  ...    10.0      10.0      55.0   1.19
pu        0.720  0.014   0.700    0.745  ...    11.0      11.0      95.0   1.16
mu        0.097  0.017   0.067    0.130  ...    89.0      78.0      93.0   1.04
mus       0.181  0.046   0.096    0.245  ...     8.0       9.0      47.0   1.18
gamma     0.362  0.048   0.283    0.465  ...    57.0      61.0      69.0   1.02
Is_begin  0.652  0.882   0.002    1.951  ...    89.0      63.0      81.0   1.00
Ia_begin  1.075  1.142   0.006    3.699  ...    59.0      58.0      21.0   1.00
E_begin   0.449  0.602   0.000    1.685  ...    73.0      43.0      41.0   1.03

[8 rows x 11 columns]