0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -831.61    53.82
p_loo       39.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.161    0.332  ...    69.0      69.0      74.0   0.99
pu        0.766  0.026   0.717    0.810  ...     9.0       9.0      19.0   1.19
mu        0.122  0.018   0.091    0.151  ...     8.0       8.0      84.0   1.25
mus       0.280  0.042   0.196    0.337  ...   152.0     152.0      91.0   1.02
gamma     0.398  0.074   0.253    0.510  ...    94.0     106.0      40.0   1.03
Is_begin  0.944  0.760   0.027    2.670  ...    61.0      59.0      91.0   1.00
Ia_begin  1.456  1.260   0.043    3.374  ...   120.0     112.0      59.0   1.01
E_begin   0.799  0.800   0.010    2.461  ...    41.0      38.0      60.0   1.01

[8 rows x 11 columns]