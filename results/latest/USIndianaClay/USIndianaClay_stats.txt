0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -714.95    24.31
p_loo       21.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.050   0.172    0.349  ...    56.0      61.0      49.0   1.01
pu        0.844  0.020   0.811    0.887  ...    50.0      50.0      59.0   1.01
mu        0.130  0.023   0.088    0.168  ...    59.0      57.0      59.0   1.07
mus       0.186  0.027   0.144    0.231  ...    71.0      74.0     100.0   1.05
gamma     0.234  0.040   0.169    0.314  ...   111.0     110.0      48.0   0.99
Is_begin  0.864  0.799   0.004    2.542  ...    91.0      57.0      15.0   1.03
Ia_begin  1.576  1.401   0.014    4.246  ...    85.0      68.0      48.0   1.01
E_begin   0.786  0.761   0.003    2.322  ...   100.0      78.0      67.0   0.99

[8 rows x 11 columns]