0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1005.21    39.42
p_loo       27.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.057   0.166    0.340  ...   146.0     129.0      56.0   1.01
pu        0.807  0.022   0.768    0.855  ...    73.0      79.0      93.0   1.00
mu        0.135  0.018   0.108    0.172  ...    25.0      24.0      28.0   1.07
mus       0.208  0.030   0.150    0.259  ...    55.0      55.0      87.0   1.06
gamma     0.257  0.053   0.167    0.345  ...    85.0      85.0     100.0   1.00
Is_begin  1.162  0.971   0.015    3.049  ...    87.0      56.0      40.0   0.99
Ia_begin  2.112  2.035   0.032    6.651  ...    79.0      63.0      38.0   1.00
E_begin   1.149  1.043   0.032    2.780  ...   104.0      70.0      60.0   1.01

[8 rows x 11 columns]