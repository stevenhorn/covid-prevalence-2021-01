0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -835.40    30.30
p_loo       24.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.059   0.160    0.349  ...    47.0      44.0      55.0   1.02
pu        0.823  0.018   0.794    0.855  ...    66.0      63.0      84.0   1.00
mu        0.132  0.025   0.094    0.178  ...    46.0      47.0      41.0   1.03
mus       0.164  0.034   0.104    0.229  ...    81.0     100.0      57.0   1.03
gamma     0.182  0.038   0.111    0.236  ...   132.0     152.0      87.0   1.01
Is_begin  0.945  0.670   0.116    2.268  ...   141.0     135.0      69.0   1.01
Ia_begin  1.828  1.669   0.070    4.951  ...   104.0      94.0      96.0   1.02
E_begin   0.818  0.642   0.076    2.237  ...   149.0     134.0      56.0   1.04

[8 rows x 11 columns]