1 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -754.06    41.24
p_loo       25.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.052   0.159    0.322  ...    22.0      36.0      57.0   1.07
pu        0.868  0.014   0.838    0.887  ...    47.0      45.0      59.0   1.01
mu        0.125  0.022   0.085    0.162  ...    15.0      17.0      64.0   1.11
mus       0.153  0.024   0.107    0.182  ...    45.0      47.0      43.0   1.03
gamma     0.151  0.034   0.099    0.219  ...    56.0      53.0      53.0   1.06
Is_begin  0.587  0.802   0.002    2.180  ...    53.0      37.0      66.0   1.07
Ia_begin  1.849  1.729   0.008    5.046  ...    31.0      31.0      31.0   1.04
E_begin   0.739  0.909   0.003    2.051  ...    45.0      33.0      43.0   1.07

[8 rows x 11 columns]