0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -295.67    38.38
p_loo       31.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.047   0.157    0.328  ...    69.0      77.0      40.0   1.03
pu        0.856  0.030   0.792    0.898  ...    14.0      11.0      18.0   1.13
mu        0.141  0.028   0.100    0.193  ...    38.0      36.0      38.0   1.03
mus       0.186  0.033   0.142    0.269  ...   132.0     114.0      46.0   1.00
gamma     0.215  0.040   0.147    0.294  ...   113.0      99.0      39.0   1.08
Is_begin  0.614  0.642   0.010    1.638  ...    40.0      50.0      40.0   1.01
Ia_begin  1.151  1.448   0.003    3.431  ...    69.0      54.0      40.0   1.05
E_begin   0.497  0.727   0.001    1.356  ...    58.0      45.0      54.0   1.02

[8 rows x 11 columns]