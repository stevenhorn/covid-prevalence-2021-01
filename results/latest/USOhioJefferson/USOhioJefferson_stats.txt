0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -897.55    49.49
p_loo       28.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.058   0.168    0.350  ...    20.0      27.0      17.0   1.08
pu        0.868  0.019   0.836    0.896  ...    63.0      58.0      54.0   1.04
mu        0.132  0.021   0.102    0.171  ...    28.0      23.0      75.0   1.06
mus       0.162  0.038   0.100    0.233  ...    77.0      81.0      56.0   1.07
gamma     0.195  0.033   0.145    0.256  ...    75.0      84.0      59.0   1.01
Is_begin  1.020  1.026   0.012    3.224  ...   142.0     152.0      60.0   0.99
Ia_begin  2.149  2.123   0.012    7.057  ...    97.0      47.0      60.0   1.00
E_begin   1.115  1.386   0.003    3.775  ...    99.0      75.0      32.0   1.02

[8 rows x 11 columns]