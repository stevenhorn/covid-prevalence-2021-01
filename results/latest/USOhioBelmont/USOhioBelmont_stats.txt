0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -912.25    28.32
p_loo       24.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.051   0.166    0.344  ...   114.0     135.0      48.0   1.02
pu        0.829  0.046   0.747    0.894  ...    38.0      39.0      60.0   1.03
mu        0.134  0.025   0.092    0.178  ...    43.0      36.0      42.0   1.05
mus       0.171  0.023   0.128    0.207  ...   120.0     113.0     100.0   1.00
gamma     0.230  0.039   0.171    0.297  ...   152.0     152.0      86.0   1.04
Is_begin  0.964  0.931   0.007    2.822  ...   128.0     132.0      43.0   1.00
Ia_begin  2.272  1.763   0.093    5.847  ...   105.0     106.0      60.0   1.01
E_begin   1.332  1.268   0.005    4.325  ...   106.0      83.0      33.0   1.02

[8 rows x 11 columns]