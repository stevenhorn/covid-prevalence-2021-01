0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -584.87    52.22
p_loo       46.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.055   0.179    0.349  ...    82.0     106.0     100.0   1.00
pu        0.757  0.032   0.701    0.812  ...    55.0      44.0      31.0   1.04
mu        0.129  0.026   0.089    0.179  ...    59.0      57.0      40.0   1.00
mus       0.175  0.039   0.125    0.270  ...   138.0     152.0      60.0   1.00
gamma     0.205  0.040   0.154    0.293  ...    95.0     110.0      77.0   1.03
Is_begin  0.508  0.535   0.005    1.418  ...    70.0      60.0      40.0   1.03
Ia_begin  1.391  1.658   0.019    4.561  ...    93.0      96.0      49.0   0.99
E_begin   0.465  0.582   0.002    1.504  ...    80.0      50.0      22.0   1.02

[8 rows x 11 columns]