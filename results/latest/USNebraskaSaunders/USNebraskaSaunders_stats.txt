0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -746.98    29.57
p_loo       21.12        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.161    0.339  ...    44.0      41.0      38.0   0.99
pu        0.805  0.035   0.745    0.862  ...    14.0      16.0      22.0   1.08
mu        0.126  0.025   0.084    0.169  ...    20.0      20.0      21.0   1.13
mus       0.158  0.030   0.116    0.232  ...   124.0     122.0      87.0   1.11
gamma     0.172  0.030   0.126    0.229  ...    65.0      64.0      59.0   1.05
Is_begin  0.660  0.518   0.030    1.621  ...    87.0      77.0      91.0   1.01
Ia_begin  1.310  1.189   0.023    3.905  ...    80.0      69.0      58.0   1.03
E_begin   0.469  0.507   0.010    1.646  ...    83.0      29.0      48.0   1.07

[8 rows x 11 columns]