0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -727.88    36.36
p_loo       27.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.049   0.170    0.348  ...   152.0     152.0      59.0   1.05
pu        0.804  0.023   0.759    0.837  ...   106.0     111.0      36.0   1.02
mu        0.122  0.019   0.089    0.157  ...    32.0      29.0      35.0   1.07
mus       0.228  0.040   0.165    0.299  ...   129.0     116.0      95.0   0.99
gamma     0.344  0.063   0.253    0.469  ...   152.0     152.0      83.0   1.01
Is_begin  1.000  0.891   0.050    2.940  ...    90.0     115.0      59.0   1.03
Ia_begin  2.011  1.575   0.067    5.132  ...    64.0      56.0      91.0   1.01
E_begin   1.350  1.615   0.011    3.498  ...    60.0      81.0      43.0   1.07

[8 rows x 11 columns]