0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -766.76    26.53
p_loo       21.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.042   0.182    0.335  ...   130.0     131.0      65.0   1.05
pu        0.839  0.023   0.794    0.871  ...    78.0      73.0      59.0   1.02
mu        0.128  0.021   0.091    0.166  ...    68.0      64.0      53.0   1.03
mus       0.176  0.029   0.129    0.233  ...    60.0      64.0      91.0   1.03
gamma     0.188  0.040   0.128    0.280  ...   152.0     152.0      77.0   1.01
Is_begin  0.856  0.894   0.015    2.170  ...    94.0      78.0      69.0   1.00
Ia_begin  1.798  1.950   0.017    5.328  ...    67.0      54.0      57.0   1.00
E_begin   0.883  0.828   0.001    2.367  ...    50.0      65.0      56.0   1.06

[8 rows x 11 columns]