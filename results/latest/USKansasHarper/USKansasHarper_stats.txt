0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -677.15    47.40
p_loo       32.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.221  0.042   0.157    0.289  ...   119.0     112.0      60.0   1.00
pu        0.734  0.017   0.709    0.762  ...    69.0      65.0      40.0   1.00
mu        0.189  0.030   0.125    0.235  ...    24.0      17.0      57.0   1.13
mus       0.259  0.041   0.184    0.332  ...    48.0      45.0      20.0   1.02
gamma     0.349  0.058   0.266    0.453  ...    15.0      16.0      59.0   1.12
Is_begin  0.395  0.384   0.002    1.246  ...    76.0      52.0      58.0   1.05
Ia_begin  0.737  0.808   0.002    2.511  ...   102.0      81.0      38.0   1.01
E_begin   0.359  0.507   0.002    1.524  ...   101.0      69.0      72.0   1.02

[8 rows x 11 columns]