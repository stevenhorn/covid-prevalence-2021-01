0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -492.96    35.74
p_loo       28.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.052   0.159    0.330  ...   124.0     120.0      96.0   1.02
pu        0.829  0.018   0.798    0.859  ...    82.0      75.0      59.0   1.05
mu        0.122  0.020   0.093    0.164  ...    60.0      57.0      93.0   1.00
mus       0.174  0.032   0.128    0.231  ...   152.0     152.0      91.0   0.99
gamma     0.251  0.044   0.185    0.329  ...    91.0      81.0      59.0   1.01
Is_begin  0.288  0.370   0.000    1.121  ...    34.0      19.0      53.0   1.09
Ia_begin  0.566  0.746   0.001    2.190  ...    73.0      77.0      40.0   1.01
E_begin   0.250  0.358   0.000    0.815  ...    78.0      75.0      46.0   1.00

[8 rows x 11 columns]