0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1027.59    29.86
p_loo       24.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.048   0.180    0.342  ...   141.0     142.0      80.0   1.01
pu        0.804  0.038   0.721    0.863  ...     9.0      18.0      38.0   1.12
mu        0.124  0.030   0.078    0.177  ...    25.0      31.0      53.0   1.07
mus       0.167  0.037   0.112    0.241  ...   134.0     152.0      91.0   1.04
gamma     0.191  0.036   0.140    0.266  ...   103.0     115.0      86.0   1.00
Is_begin  0.877  0.802   0.058    2.292  ...   117.0      90.0      81.0   1.00
Ia_begin  0.639  0.527   0.010    1.596  ...   103.0      96.0      93.0   1.00
E_begin   0.544  0.568   0.012    1.553  ...    66.0      42.0      88.0   1.01

[8 rows x 11 columns]