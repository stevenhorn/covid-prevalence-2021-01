0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -670.83    30.37
p_loo       24.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.049   0.180    0.348  ...    92.0      89.0      59.0   1.08
pu        0.828  0.021   0.791    0.867  ...    53.0      53.0      58.0   1.02
mu        0.132  0.024   0.095    0.176  ...    42.0      39.0      16.0   1.09
mus       0.160  0.038   0.105    0.226  ...   152.0     152.0      37.0   1.06
gamma     0.173  0.037   0.118    0.233  ...   152.0     152.0      93.0   1.05
Is_begin  0.592  0.672   0.007    1.878  ...   117.0      79.0      88.0   1.01
Ia_begin  0.952  0.866   0.022    2.745  ...    59.0      41.0      60.0   1.01
E_begin   0.452  0.528   0.004    1.679  ...    86.0      67.0      58.0   0.98

[8 rows x 11 columns]