0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1289.91    26.91
p_loo       23.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.227  0.047   0.150    0.313  ...    33.0      27.0      43.0   1.07
pu        0.731  0.023   0.700    0.777  ...    65.0      56.0      58.0   1.02
mu        0.139  0.020   0.106    0.172  ...     9.0       9.0      59.0   1.18
mus       0.167  0.024   0.118    0.205  ...   137.0     133.0      60.0   1.03
gamma     0.229  0.036   0.161    0.308  ...   110.0     113.0     102.0   0.99
Is_begin  0.550  0.549   0.042    1.781  ...    68.0      37.0      19.0   1.03
Ia_begin  1.517  1.472   0.010    4.427  ...    52.0      45.0      59.0   1.03
E_begin   0.719  0.913   0.004    2.151  ...    82.0      47.0      43.0   1.04

[8 rows x 11 columns]