0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -697.11    27.74
p_loo       25.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.056   0.154    0.332  ...    54.0      61.0      42.0   1.03
pu        0.855  0.027   0.816    0.897  ...    12.0      14.0      24.0   1.11
mu        0.146  0.025   0.098    0.193  ...     7.0       7.0      58.0   1.24
mus       0.164  0.033   0.111    0.227  ...   121.0     148.0      99.0   0.98
gamma     0.192  0.036   0.123    0.249  ...   152.0     152.0      97.0   1.03
Is_begin  0.798  0.821   0.042    2.705  ...   104.0      80.0      95.0   1.00
Ia_begin  1.933  2.067   0.025    4.810  ...   111.0      84.0      60.0   0.99
E_begin   0.834  0.835   0.021    2.361  ...    81.0      61.0      93.0   1.00

[8 rows x 11 columns]