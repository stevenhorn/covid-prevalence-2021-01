0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -341.80    57.16
p_loo       39.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.045   0.163    0.320  ...    19.0      19.0      40.0   1.09
pu        0.749  0.035   0.701    0.814  ...    11.0      11.0      59.0   1.15
mu        0.151  0.032   0.100    0.205  ...    16.0      16.0      45.0   1.11
mus       0.240  0.037   0.185    0.310  ...    24.0      43.0      80.0   1.06
gamma     0.251  0.041   0.180    0.325  ...    49.0      44.0      93.0   1.03
Is_begin  0.739  0.582   0.050    1.964  ...    63.0      49.0      91.0   1.02
Ia_begin  1.405  1.423   0.013    4.700  ...    70.0      45.0      33.0   1.04
E_begin   0.635  0.629   0.007    1.905  ...    96.0      69.0      47.0   1.11

[8 rows x 11 columns]