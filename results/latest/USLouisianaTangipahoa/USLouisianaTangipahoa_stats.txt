0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1378.30    40.97
p_loo       38.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.048   0.172    0.335  ...   132.0     135.0      81.0   0.98
pu        0.766  0.044   0.702    0.836  ...    46.0      49.0      75.0   1.05
mu        0.134  0.020   0.100    0.165  ...    19.0      19.0      53.0   1.08
mus       0.202  0.040   0.139    0.271  ...    58.0      60.0      57.0   1.00
gamma     0.305  0.047   0.233    0.411  ...    87.0      84.0      96.0   1.01
Is_begin  1.904  1.542   0.018    4.520  ...    75.0      29.0      24.0   1.09
Ia_begin  0.862  0.650   0.044    2.068  ...    83.0      59.0      91.0   1.01
E_begin   1.881  2.167   0.020    7.634  ...    74.0      58.0      60.0   1.06

[8 rows x 11 columns]