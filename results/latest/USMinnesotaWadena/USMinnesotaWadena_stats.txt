0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -577.69    25.82
p_loo       20.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.056   0.174    0.347  ...    57.0      70.0      59.0   1.13
pu        0.863  0.013   0.846    0.886  ...    64.0      69.0      97.0   1.17
mu        0.139  0.027   0.091    0.178  ...    44.0      43.0      43.0   0.99
mus       0.157  0.039   0.100    0.230  ...   100.0     152.0      79.0   1.01
gamma     0.150  0.029   0.105    0.210  ...    62.0      63.0      88.0   1.00
Is_begin  0.749  0.625   0.046    2.036  ...    98.0      69.0      79.0   1.00
Ia_begin  1.712  1.835   0.019    5.040  ...   103.0     152.0      88.0   0.98
E_begin   0.745  0.858   0.001    2.518  ...    60.0      28.0      14.0   1.06

[8 rows x 11 columns]