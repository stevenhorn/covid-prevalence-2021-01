0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1207.70    24.26
p_loo       22.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.053   0.181    0.349  ...    57.0      60.0      43.0   1.08
pu        0.759  0.029   0.708    0.806  ...    24.0      19.0      43.0   1.08
mu        0.120  0.033   0.071    0.189  ...    11.0      13.0      15.0   1.09
mus       0.172  0.034   0.120    0.230  ...    16.0      14.0      47.0   1.12
gamma     0.233  0.046   0.151    0.317  ...   104.0      99.0      72.0   1.01
Is_begin  1.186  0.925   0.007    2.716  ...    31.0      15.0      35.0   1.09
Ia_begin  2.753  2.190   0.038    6.825  ...    63.0      39.0      48.0   1.04
E_begin   1.829  2.310   0.002    7.974  ...    51.0      25.0      60.0   1.07

[8 rows x 11 columns]