0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -642.40    43.59
p_loo       35.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.6%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.191  0.030   0.154    0.246  ...   152.0     125.0      49.0   0.99
pu        0.711  0.009   0.700    0.727  ...   152.0     102.0      60.0   0.98
mu        0.110  0.020   0.076    0.147  ...    16.0      17.0      59.0   1.10
mus       0.169  0.037   0.110    0.250  ...    68.0      58.0      88.0   1.03
gamma     0.189  0.036   0.134    0.261  ...   152.0     152.0      95.0   1.00
Is_begin  0.215  0.202   0.005    0.622  ...    58.0      36.0      60.0   1.05
Ia_begin  0.405  0.548   0.003    1.472  ...    82.0      58.0      43.0   1.00
E_begin   0.189  0.306   0.001    0.794  ...    96.0      82.0      60.0   0.98

[8 rows x 11 columns]