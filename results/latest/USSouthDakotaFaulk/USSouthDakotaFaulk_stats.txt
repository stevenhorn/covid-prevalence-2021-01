0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -506.97    42.34
p_loo       31.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.046   0.161    0.310  ...    84.0      79.0      69.0   1.01
pu        0.736  0.023   0.702    0.772  ...    69.0      60.0      60.0   1.01
mu        0.137  0.027   0.093    0.199  ...    14.0      13.0      57.0   1.10
mus       0.201  0.034   0.152    0.263  ...   108.0     104.0      60.0   1.01
gamma     0.238  0.041   0.170    0.317  ...    46.0      57.0      34.0   1.05
Is_begin  0.453  0.537   0.012    1.740  ...    66.0      50.0      96.0   1.05
Ia_begin  0.799  0.862   0.002    2.455  ...    70.0      57.0      58.0   1.00
E_begin   0.349  0.359   0.006    1.097  ...    73.0      70.0      55.0   0.98

[8 rows x 11 columns]