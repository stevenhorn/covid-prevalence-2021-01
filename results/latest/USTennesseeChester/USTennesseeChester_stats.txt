0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -776.82    34.20
p_loo       31.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.052   0.165    0.332  ...    75.0      74.0      59.0   1.04
pu        0.785  0.030   0.726    0.826  ...    16.0      15.0      22.0   1.13
mu        0.131  0.030   0.085    0.178  ...     4.0       4.0      24.0   1.62
mus       0.179  0.031   0.114    0.232  ...    53.0      59.0      60.0   1.01
gamma     0.216  0.038   0.150    0.287  ...   152.0     152.0      81.0   1.00
Is_begin  0.631  0.653   0.014    2.043  ...    74.0      70.0      69.0   1.00
Ia_begin  1.207  1.247   0.026    3.372  ...   116.0      63.0      38.0   1.00
E_begin   0.592  0.501   0.004    1.613  ...   110.0     101.0      93.0   0.99

[8 rows x 11 columns]