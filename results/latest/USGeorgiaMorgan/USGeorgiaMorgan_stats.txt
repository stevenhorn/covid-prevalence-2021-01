0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -840.54    64.65
p_loo       37.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.054   0.163    0.339  ...    75.0      85.0      59.0   1.00
pu        0.771  0.029   0.715    0.811  ...    44.0      50.0      59.0   1.03
mu        0.160  0.022   0.121    0.196  ...    16.0      15.0      14.0   1.10
mus       0.261  0.033   0.206    0.317  ...    47.0      50.0      91.0   1.03
gamma     0.421  0.066   0.303    0.532  ...   117.0     112.0      58.0   1.00
Is_begin  1.024  0.763   0.028    2.371  ...    37.0      23.0      81.0   1.10
Ia_begin  1.895  1.709   0.008    5.928  ...    61.0      66.0      60.0   0.99
E_begin   0.982  1.041   0.022    2.804  ...    58.0      49.0      60.0   1.08

[8 rows x 11 columns]