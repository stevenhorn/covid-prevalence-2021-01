0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1118.71    38.09
p_loo       32.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.056   0.152    0.331  ...    62.0      64.0      14.0   1.02
pu        0.733  0.034   0.700    0.792  ...    60.0      39.0      76.0   1.05
mu        0.164  0.023   0.123    0.206  ...    52.0      52.0      56.0   1.05
mus       0.230  0.035   0.175    0.278  ...   152.0     152.0      88.0   1.00
gamma     0.409  0.049   0.323    0.487  ...    84.0      86.0      56.0   1.07
Is_begin  0.861  0.717   0.015    2.098  ...   107.0      84.0      64.0   1.08
Ia_begin  1.911  1.467   0.027    4.794  ...    15.0       8.0      49.0   1.23
E_begin   1.775  2.366   0.002    5.753  ...    25.0       8.0      38.0   1.24

[8 rows x 11 columns]