4 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1329.72    32.03
p_loo       26.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.236   0.055   0.150    0.326  ...    79.0      79.0      20.0   1.03
pu         0.757   0.038   0.702    0.828  ...    46.0      52.0      58.0   1.01
mu         0.123   0.028   0.082    0.182  ...    26.0      30.0      22.0   1.04
mus        0.192   0.035   0.132    0.253  ...    83.0      85.0      58.0   0.99
gamma      0.275   0.054   0.195    0.378  ...    56.0      64.0      43.0   1.04
Is_begin  10.107   7.809   0.139   24.882  ...    95.0      77.0      88.0   0.99
Ia_begin  30.062  12.117   8.018   48.054  ...    59.0      64.0      81.0   1.03
E_begin   40.532  26.890   5.949   99.860  ...    75.0      67.0      78.0   1.01

[8 rows x 11 columns]