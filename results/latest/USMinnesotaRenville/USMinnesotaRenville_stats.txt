0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -615.08    26.32
p_loo       20.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.052   0.179    0.341  ...   152.0     152.0      60.0   0.99
pu        0.831  0.014   0.807    0.854  ...   152.0     152.0      60.0   1.00
mu        0.122  0.022   0.092    0.164  ...    32.0      35.0      35.0   1.22
mus       0.156  0.027   0.115    0.210  ...    79.0      84.0      60.0   1.01
gamma     0.180  0.031   0.139    0.249  ...    43.0      39.0      22.0   1.05
Is_begin  0.613  0.504   0.002    1.670  ...    96.0      70.0      45.0   0.99
Ia_begin  1.374  1.128   0.055    3.227  ...    93.0      84.0      60.0   1.00
E_begin   0.613  0.564   0.000    1.554  ...    72.0      57.0      59.0   1.01

[8 rows x 11 columns]