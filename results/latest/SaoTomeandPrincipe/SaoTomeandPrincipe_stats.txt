0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -952.89    55.67
p_loo       46.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.3%
 (0.5, 0.7]   (ok)          2    0.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    5    1.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.056   0.153    0.339  ...    69.0      68.0     100.0   0.99
pu        0.887  0.017   0.855    0.900  ...    19.0       5.0      17.0   1.42
mu        0.148  0.027   0.097    0.187  ...     5.0       5.0      16.0   1.42
mus       0.217  0.032   0.161    0.266  ...    88.0      90.0      96.0   1.00
gamma     0.314  0.058   0.210    0.415  ...    55.0      54.0      77.0   1.04
Is_begin  0.765  0.897   0.002    2.833  ...   111.0      71.0      60.0   0.99
Ia_begin  1.550  1.973   0.052    5.174  ...    56.0      63.0      47.0   1.01
E_begin   0.889  1.039   0.007    2.644  ...    75.0      64.0      59.0   1.01

[8 rows x 11 columns]