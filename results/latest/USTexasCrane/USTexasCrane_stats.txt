0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -831.99    64.27
p_loo       44.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.035   0.151    0.262  ...   118.0     107.0      48.0   1.09
pu        0.719  0.014   0.701    0.749  ...   152.0     152.0      61.0   1.00
mu        0.136  0.025   0.093    0.179  ...   152.0     138.0      72.0   0.98
mus       0.171  0.033   0.125    0.236  ...   118.0     151.0      39.0   1.03
gamma     0.196  0.035   0.141    0.261  ...    20.0      15.0      60.0   1.09
Is_begin  0.596  0.655   0.004    2.178  ...   124.0      76.0      93.0   0.99
Ia_begin  1.577  1.609   0.030    4.972  ...    45.0     106.0      96.0   1.03
E_begin   0.647  0.711   0.000    1.901  ...    96.0      51.0      22.0   1.05

[8 rows x 11 columns]