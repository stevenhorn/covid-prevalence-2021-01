0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1061.18    34.00
p_loo       21.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.046   0.183    0.336  ...   133.0     122.0      91.0   1.02
pu        0.860  0.022   0.820    0.894  ...   110.0     107.0      84.0   1.01
mu        0.114  0.022   0.082    0.159  ...    30.0      30.0      65.0   1.08
mus       0.146  0.025   0.102    0.190  ...    98.0      98.0      59.0   1.04
gamma     0.161  0.031   0.117    0.219  ...    26.0      39.0      49.0   1.04
Is_begin  0.783  0.619   0.002    1.846  ...   145.0     138.0      96.0   0.99
Ia_begin  1.768  1.303   0.019    4.414  ...    59.0      63.0      22.0   1.04
E_begin   0.989  0.873   0.004    2.468  ...   100.0      79.0      81.0   1.01

[8 rows x 11 columns]