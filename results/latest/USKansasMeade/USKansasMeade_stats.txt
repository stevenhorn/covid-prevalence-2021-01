0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -641.79    28.95
p_loo       25.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.052   0.174    0.347  ...    20.0      23.0      55.0   1.07
pu        0.740  0.027   0.701    0.785  ...    34.0      28.0      38.0   1.16
mu        0.140  0.027   0.093    0.183  ...     7.0       7.0      18.0   1.27
mus       0.185  0.043   0.112    0.266  ...    39.0      39.0      40.0   1.04
gamma     0.204  0.043   0.142    0.286  ...    36.0      41.0      96.0   1.04
Is_begin  0.509  0.479   0.003    1.472  ...    48.0      41.0      40.0   0.99
Ia_begin  0.989  1.285   0.015    2.916  ...    37.0      26.0      59.0   1.07
E_begin   0.404  0.501   0.007    1.417  ...    37.0      30.0      91.0   1.04

[8 rows x 11 columns]