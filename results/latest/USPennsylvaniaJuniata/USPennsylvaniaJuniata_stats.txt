0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -694.23    30.63
p_loo       33.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.055   0.164    0.330  ...    47.0      41.0      60.0   1.02
pu        0.861  0.020   0.825    0.896  ...    35.0      33.0      36.0   1.10
mu        0.138  0.024   0.095    0.176  ...    11.0      12.0      44.0   1.14
mus       0.206  0.037   0.141    0.276  ...    79.0      81.0      74.0   1.09
gamma     0.276  0.049   0.183    0.369  ...    46.0      60.0      87.0   1.04
Is_begin  1.474  1.202   0.046    3.938  ...    79.0      60.0      59.0   1.03
Ia_begin  4.408  3.162   0.336   11.539  ...    59.0      43.0      93.0   1.05
E_begin   2.744  2.362   0.020    7.690  ...    32.0      20.0      56.0   1.07

[8 rows x 11 columns]