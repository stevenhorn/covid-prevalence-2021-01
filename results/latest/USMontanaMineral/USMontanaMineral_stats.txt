0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -346.71    31.12
p_loo       27.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.046   0.182    0.339  ...    81.0      84.0      79.0   1.03
pu        0.848  0.039   0.762    0.894  ...    13.0      15.0      60.0   1.09
mu        0.123  0.025   0.083    0.173  ...    75.0      76.0      56.0   1.03
mus       0.159  0.034   0.112    0.235  ...   129.0     135.0      68.0   0.99
gamma     0.192  0.045   0.117    0.285  ...   139.0     127.0      87.0   0.99
Is_begin  0.370  0.388   0.013    1.175  ...    69.0      60.0      58.0   0.99
Ia_begin  0.766  0.850   0.006    2.364  ...    53.0      50.0      59.0   1.00
E_begin   0.334  0.416   0.003    1.127  ...    63.0      61.0      24.0   1.10

[8 rows x 11 columns]