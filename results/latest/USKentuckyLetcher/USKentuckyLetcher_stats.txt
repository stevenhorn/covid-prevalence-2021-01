0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -685.60    32.69
p_loo       23.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.056   0.160    0.333  ...    66.0      60.0      57.0   1.00
pu        0.842  0.024   0.800    0.886  ...    55.0      54.0      59.0   0.99
mu        0.119  0.025   0.074    0.159  ...    26.0      27.0      31.0   1.03
mus       0.155  0.028   0.109    0.200  ...   102.0     101.0      93.0   1.01
gamma     0.189  0.037   0.134    0.281  ...   107.0     152.0      76.0   1.03
Is_begin  0.443  0.421   0.001    1.303  ...    36.0      12.0      32.0   1.14
Ia_begin  1.123  1.161   0.015    3.018  ...    91.0      62.0      20.0   0.99
E_begin   0.484  0.616   0.029    1.838  ...    86.0      48.0      55.0   1.02

[8 rows x 11 columns]