0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -522.99    25.71
p_loo       23.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.057   0.165    0.345  ...    43.0      29.0      59.0   1.06
pu        0.821  0.022   0.776    0.863  ...    30.0      41.0      38.0   1.06
mu        0.118  0.018   0.085    0.146  ...    87.0      86.0      88.0   1.01
mus       0.147  0.027   0.104    0.201  ...   152.0     152.0      48.0   1.01
gamma     0.170  0.029   0.108    0.211  ...    59.0      55.0      59.0   1.05
Is_begin  0.441  0.446   0.046    1.373  ...    84.0      37.0      52.0   1.02
Ia_begin  1.039  1.078   0.025    2.700  ...    91.0      79.0      60.0   1.00
E_begin   0.389  0.447   0.003    1.229  ...    71.0      53.0      35.0   1.00

[8 rows x 11 columns]