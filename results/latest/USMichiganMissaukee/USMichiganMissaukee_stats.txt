0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -594.98    29.86
p_loo       28.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.056   0.159    0.348  ...   152.0     152.0      49.0   1.04
pu        0.873  0.019   0.837    0.899  ...   115.0     152.0      88.0   0.99
mu        0.125  0.020   0.099    0.170  ...    39.0      38.0      41.0   1.06
mus       0.167  0.028   0.115    0.214  ...   152.0     152.0      88.0   1.01
gamma     0.229  0.043   0.160    0.313  ...   152.0     152.0      88.0   0.99
Is_begin  0.972  0.988   0.009    2.885  ...   120.0     152.0      91.0   1.05
Ia_begin  1.686  1.812   0.007    5.026  ...    75.0      46.0      43.0   1.01
E_begin   0.862  0.965   0.003    2.583  ...    96.0      79.0      60.0   1.01

[8 rows x 11 columns]