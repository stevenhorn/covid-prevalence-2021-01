2 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1004.04    31.78
p_loo       22.01        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.277  0.051   0.184    0.345  ...    58.0      57.0      49.0   1.03
pu        0.884  0.013   0.857    0.900  ...    78.0      71.0      91.0   1.04
mu        0.148  0.020   0.115    0.181  ...     7.0       7.0      38.0   1.27
mus       0.173  0.031   0.111    0.216  ...    48.0      48.0      40.0   1.09
gamma     0.223  0.041   0.137    0.291  ...   142.0     151.0      97.0   1.03
Is_begin  0.919  0.840   0.023    2.581  ...    57.0      50.0      93.0   0.99
Ia_begin  0.623  0.516   0.010    1.525  ...    22.0      16.0      59.0   1.10
E_begin   0.647  0.579   0.020    1.754  ...    51.0      40.0      81.0   1.03

[8 rows x 11 columns]