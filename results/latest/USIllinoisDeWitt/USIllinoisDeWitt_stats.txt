0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -682.25    52.41
p_loo       35.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.052   0.158    0.319  ...    45.0      51.0     100.0   1.00
pu        0.843  0.024   0.799    0.880  ...    40.0      42.0      59.0   1.04
mu        0.132  0.017   0.098    0.155  ...    83.0      90.0      95.0   1.04
mus       0.186  0.029   0.127    0.233  ...    45.0      48.0      43.0   1.03
gamma     0.200  0.055   0.115    0.284  ...   152.0     152.0      51.0   1.05
Is_begin  0.806  1.013   0.001    2.588  ...    96.0      55.0      77.0   1.06
Ia_begin  1.565  1.927   0.004    5.340  ...    76.0      54.0      22.0   1.00
E_begin   0.513  0.567   0.005    1.778  ...    99.0      49.0      80.0   1.05

[8 rows x 11 columns]