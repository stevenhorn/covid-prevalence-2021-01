0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -528.57    47.75
p_loo       40.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.057   0.154    0.336  ...    83.0      80.0      40.0   1.01
pu        0.819  0.049   0.737    0.896  ...    43.0      74.0      55.0   1.10
mu        0.169  0.021   0.137    0.209  ...    76.0      88.0      59.0   1.03
mus       0.250  0.049   0.176    0.341  ...   106.0     105.0      59.0   1.04
gamma     0.346  0.055   0.264    0.455  ...   110.0      99.0      58.0   1.05
Is_begin  1.540  0.981   0.087    3.371  ...    99.0      81.0      96.0   1.03
Ia_begin  3.037  2.454   0.034    8.050  ...    71.0      47.0      59.0   1.01
E_begin   1.957  1.828   0.002    6.668  ...    58.0      45.0      22.0   1.03

[8 rows x 11 columns]