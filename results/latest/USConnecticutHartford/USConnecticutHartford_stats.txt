0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1791.45    27.42
p_loo       25.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.242   0.051   0.168    0.336  ...    98.0      95.0      60.0   0.99
pu         0.767   0.049   0.700    0.850  ...    50.0      50.0      40.0   1.01
mu         0.110   0.018   0.075    0.139  ...    10.0      10.0      56.0   1.17
mus        0.161   0.029   0.111    0.216  ...    98.0     101.0      60.0   1.10
gamma      0.257   0.040   0.191    0.335  ...    84.0      85.0      59.0   1.01
Is_begin   8.666   4.488   1.620   15.464  ...    72.0      75.0      95.0   1.10
Ia_begin  18.605  10.885   1.034   38.590  ...    73.0      66.0      40.0   1.01
E_begin   17.946  13.621   0.149   39.355  ...    29.0      11.0      22.0   1.18

[8 rows x 11 columns]