0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1525.36    26.72
p_loo       28.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.240   0.052   0.166    0.335  ...    63.0      72.0      74.0   1.03
pu         0.754   0.039   0.707    0.835  ...    75.0      94.0      60.0   1.02
mu         0.107   0.020   0.073    0.146  ...    11.0      11.0      34.0   1.32
mus        0.170   0.028   0.125    0.218  ...    70.0      74.0      91.0   1.01
gamma      0.231   0.036   0.154    0.278  ...    54.0      47.0      45.0   1.03
Is_begin  17.765  11.161   0.671   36.489  ...    80.0      66.0      60.0   1.05
Ia_begin  49.299  20.329  22.767   92.174  ...    77.0      92.0      59.0   1.02
E_begin   58.043  36.213  13.288  134.097  ...   105.0     100.0      34.0   1.02

[8 rows x 11 columns]