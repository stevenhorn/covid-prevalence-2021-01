0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -659.14    37.19
p_loo       26.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   96.9%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.047   0.158    0.320  ...    13.0      11.0      38.0   1.23
pu        0.741  0.026   0.704    0.791  ...    40.0      37.0      53.0   1.20
mu        0.146  0.023   0.103    0.187  ...    10.0      11.0      32.0   1.15
mus       0.221  0.036   0.159    0.290  ...    54.0      53.0      58.0   1.00
gamma     0.291  0.047   0.222    0.383  ...    44.0      47.0      24.0   1.05
Is_begin  0.798  0.772   0.000    2.187  ...    22.0      18.0      24.0   1.08
Ia_begin  1.823  2.035   0.010    5.094  ...    42.0      24.0      24.0   1.08
E_begin   0.684  0.668   0.025    1.781  ...    14.0      11.0      54.0   1.14

[8 rows x 11 columns]