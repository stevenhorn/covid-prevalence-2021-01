0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1018.97    48.96
p_loo       35.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.056   0.170    0.350  ...    74.0      63.0      43.0   1.01
pu        0.846  0.026   0.802    0.887  ...    22.0      16.0      23.0   1.12
mu        0.140  0.021   0.097    0.170  ...    15.0      11.0      48.0   1.18
mus       0.192  0.033   0.124    0.235  ...    62.0      61.0      59.0   1.03
gamma     0.257  0.048   0.174    0.346  ...    67.0      61.0      60.0   1.04
Is_begin  1.042  0.905   0.019    2.988  ...    73.0      43.0      15.0   1.05
Ia_begin  2.237  1.991   0.022    5.972  ...    73.0      56.0      24.0   1.00
E_begin   1.068  1.099   0.019    3.295  ...    60.0      39.0      45.0   0.99

[8 rows x 11 columns]