0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -850.87    23.76
p_loo       20.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.054   0.173    0.347  ...    42.0      41.0      88.0   1.02
pu        0.799  0.031   0.734    0.854  ...    35.0      36.0      58.0   1.04
mu        0.112  0.020   0.082    0.146  ...    13.0      14.0      43.0   1.11
mus       0.149  0.030   0.089    0.194  ...   122.0     145.0      88.0   1.01
gamma     0.161  0.035   0.101    0.221  ...    87.0      81.0      67.0   1.00
Is_begin  0.944  0.916   0.009    2.895  ...    98.0      52.0      45.0   1.00
Ia_begin  1.549  1.908   0.082    6.098  ...    94.0      76.0      79.0   1.00
E_begin   0.874  1.069   0.012    2.957  ...    97.0     108.0      50.0   0.99

[8 rows x 11 columns]