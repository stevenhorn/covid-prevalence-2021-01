0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -625.49    28.26
p_loo       24.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.046   0.177    0.331  ...    88.0      93.0      60.0   1.01
pu        0.867  0.025   0.810    0.896  ...    47.0      42.0      60.0   1.02
mu        0.124  0.024   0.084    0.172  ...    36.0      30.0      59.0   1.04
mus       0.163  0.027   0.116    0.208  ...   152.0     152.0      99.0   1.01
gamma     0.183  0.039   0.113    0.255  ...   124.0     130.0      86.0   1.04
Is_begin  0.402  0.631   0.000    1.218  ...    78.0      59.0      33.0   0.99
Ia_begin  0.671  0.956   0.001    2.550  ...    56.0      19.0      38.0   1.07
E_begin   0.319  0.525   0.000    0.962  ...    94.0      34.0      15.0   1.02

[8 rows x 11 columns]