0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -896.11    28.63
p_loo       26.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.048   0.173    0.343  ...    65.0      71.0      60.0   1.01
pu        0.743  0.029   0.700    0.791  ...    13.0      15.0      54.0   1.11
mu        0.111  0.025   0.067    0.155  ...     6.0       6.0      45.0   1.33
mus       0.152  0.029   0.104    0.203  ...    20.0      19.0      38.0   1.08
gamma     0.170  0.036   0.104    0.241  ...    56.0      57.0      57.0   1.00
Is_begin  0.302  0.339   0.005    0.820  ...    91.0      66.0      55.0   0.99
Ia_begin  0.560  0.693   0.010    1.779  ...   112.0      36.0      31.0   1.06
E_begin   0.252  0.243   0.002    0.733  ...   112.0      90.0      97.0   1.05

[8 rows x 11 columns]