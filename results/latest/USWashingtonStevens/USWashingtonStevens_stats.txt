0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -657.50    26.42
p_loo       23.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.291  0.051   0.197    0.350  ...   127.0     112.0      95.0   1.07
pu        0.889  0.012   0.875    0.900  ...    89.0     152.0      79.0   0.98
mu        0.171  0.031   0.125    0.240  ...    18.0      20.0      43.0   1.20
mus       0.175  0.027   0.114    0.220  ...   152.0     152.0      58.0   1.00
gamma     0.209  0.037   0.137    0.276  ...    93.0     110.0      59.0   1.03
Is_begin  1.064  0.988   0.016    3.012  ...   126.0     152.0      65.0   1.03
Ia_begin  2.543  2.493   0.130    7.676  ...   152.0     130.0      97.0   1.00
E_begin   1.327  1.600   0.043    4.231  ...    77.0      39.0      60.0   1.05

[8 rows x 11 columns]