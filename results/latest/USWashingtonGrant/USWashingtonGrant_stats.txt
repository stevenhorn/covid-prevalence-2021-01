0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1162.53    32.07
p_loo       26.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.047   0.171    0.335  ...    94.0      92.0      38.0   0.99
pu        0.870  0.019   0.839    0.893  ...    73.0      71.0      60.0   1.02
mu        0.157  0.025   0.115    0.202  ...    13.0      14.0      44.0   1.11
mus       0.172  0.027   0.109    0.213  ...    78.0      67.0      60.0   1.03
gamma     0.216  0.040   0.158    0.280  ...    76.0      74.0      88.0   0.99
Is_begin  1.394  1.256   0.003    3.842  ...    52.0      30.0      24.0   1.07
Ia_begin  4.310  2.788   0.256    8.285  ...    68.0     120.0      61.0   1.06
E_begin   2.435  2.315   0.089    6.765  ...   109.0      77.0      65.0   1.03

[8 rows x 11 columns]