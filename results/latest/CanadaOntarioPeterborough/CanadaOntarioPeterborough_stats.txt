0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -614.35    37.65
p_loo       35.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      293   94.8%
 (0.5, 0.7]   (ok)         12    3.9%
   (0.7, 1]   (bad)         2    0.6%
   (1, Inf)   (very bad)    2    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.264   0.059   0.159    0.348  ...    45.0      37.0      23.0   1.08
pu         0.878   0.023   0.828    0.899  ...    51.0      38.0      58.0   1.04
mu         0.154   0.026   0.115    0.216  ...    56.0      51.0      58.0   0.99
mus        0.196   0.029   0.152    0.265  ...   107.0     139.0      61.0   1.02
gamma      0.265   0.047   0.191    0.357  ...    64.0      78.0      25.0   1.07
Is_begin   6.389   5.547   0.224   18.060  ...    81.0     121.0      51.0   1.00
Ia_begin  37.801  22.024   5.609   74.266  ...    24.0      36.0      91.0   1.11
E_begin   16.771  13.586   1.142   43.425  ...    93.0     111.0      88.0   0.98

[8 rows x 11 columns]