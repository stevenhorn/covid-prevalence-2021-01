0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -873.48    23.81
p_loo       20.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.052   0.156    0.333  ...   129.0     129.0      67.0   1.00
pu        0.829  0.043   0.756    0.900  ...    50.0      44.0      40.0   1.08
mu        0.102  0.024   0.068    0.154  ...    40.0      37.0      40.0   1.09
mus       0.153  0.029   0.099    0.197  ...   152.0     143.0      59.0   1.00
gamma     0.171  0.037   0.110    0.226  ...   126.0     129.0      87.0   1.01
Is_begin  0.838  0.881   0.018    2.426  ...    87.0      67.0      40.0   1.00
Ia_begin  2.007  2.110   0.017    5.912  ...    68.0     116.0      54.0   1.00
E_begin   0.967  1.235   0.015    2.630  ...    79.0      55.0      60.0   0.99

[8 rows x 11 columns]