0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -696.88    27.52
p_loo       27.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.046   0.168    0.331  ...   152.0     152.0      76.0   1.01
pu        0.778  0.031   0.729    0.839  ...    39.0      38.0      25.0   1.03
mu        0.133  0.020   0.105    0.172  ...    14.0      16.0      40.0   1.10
mus       0.192  0.034   0.136    0.256  ...    15.0      15.0      76.0   1.13
gamma     0.256  0.039   0.188    0.318  ...   116.0     133.0      58.0   0.99
Is_begin  0.808  0.659   0.010    1.864  ...    68.0      65.0      60.0   1.07
Ia_begin  1.647  1.174   0.020    3.914  ...    86.0      73.0      57.0   1.00
E_begin   0.918  0.957   0.002    2.567  ...    88.0      56.0      68.0   0.99

[8 rows x 11 columns]