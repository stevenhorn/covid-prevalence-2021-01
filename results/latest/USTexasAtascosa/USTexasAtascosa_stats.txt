31 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1267.38    50.24
p_loo       38.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      255   86.4%
 (0.5, 0.7]   (ok)         25    8.5%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    8    2.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.040   0.153    0.257  ...     5.0       4.0      14.0   1.75
pu        0.746  0.020   0.717    0.788  ...    13.0      21.0      43.0   1.29
mu        0.159  0.024   0.129    0.198  ...     3.0       3.0      34.0   2.24
mus       0.190  0.032   0.126    0.231  ...    15.0       8.0      22.0   1.21
gamma     0.298  0.059   0.187    0.422  ...     7.0       7.0      40.0   1.25
Is_begin  0.754  0.436   0.106    1.671  ...     6.0       6.0      16.0   1.30
Ia_begin  0.958  1.078   0.047    1.994  ...    25.0      24.0      19.0   1.28
E_begin   0.708  0.700   0.101    1.876  ...     4.0       5.0      38.0   1.50

[8 rows x 11 columns]