0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -990.97    28.85
p_loo       32.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.056   0.156    0.335  ...    41.0      40.0      60.0   1.04
pu        0.761  0.034   0.700    0.810  ...    36.0      34.0      14.0   1.02
mu        0.110  0.016   0.086    0.137  ...     8.0       9.0      95.0   1.18
mus       0.150  0.027   0.112    0.205  ...    13.0      13.0      38.0   1.15
gamma     0.182  0.040   0.130    0.276  ...    99.0     152.0      61.0   1.04
Is_begin  0.698  0.792   0.003    2.078  ...    81.0      51.0      60.0   0.99
Ia_begin  1.827  1.805   0.101    4.709  ...    74.0      42.0      77.0   1.02
E_begin   0.945  0.979   0.032    2.748  ...    44.0      34.0      60.0   1.06

[8 rows x 11 columns]