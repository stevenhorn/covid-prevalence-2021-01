0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -138.27    22.38
p_loo       27.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.056   0.151    0.336  ...    54.0      48.0      34.0   0.98
pu        0.868  0.041   0.758    0.899  ...     4.0       4.0      22.0   1.58
mu        0.141  0.022   0.103    0.179  ...    99.0     112.0      79.0   1.00
mus       0.186  0.039   0.115    0.254  ...    12.0      13.0      59.0   1.13
gamma     0.212  0.047   0.143    0.292  ...    54.0      74.0      39.0   1.02
Is_begin  0.387  0.515   0.007    1.129  ...    55.0      87.0      52.0   1.05
Ia_begin  0.576  0.704   0.006    2.010  ...    47.0      55.0      43.0   1.06
E_begin   0.287  0.467   0.004    0.582  ...    58.0      24.0      42.0   1.06

[8 rows x 11 columns]