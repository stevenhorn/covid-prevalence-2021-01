0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -815.64    45.66
p_loo       34.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      291   94.2%
 (0.5, 0.7]   (ok)         15    4.9%
   (0.7, 1]   (bad)         2    0.6%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.268   0.056   0.164    0.344  ...   132.0     152.0      14.0   1.11
pu         0.871   0.033   0.794    0.900  ...   111.0     111.0      43.0   1.00
mu         0.141   0.024   0.101    0.183  ...    10.0      13.0      19.0   1.11
mus        0.192   0.032   0.136    0.245  ...    65.0      63.0      97.0   0.99
gamma      0.289   0.051   0.212    0.387  ...    95.0      93.0      60.0   1.00
Is_begin   6.748   4.709   0.438   13.427  ...   119.0     104.0      39.0   1.05
Ia_begin  47.305  20.582  13.624   83.480  ...    78.0      74.0      35.0   1.03
E_begin   33.775  24.572   3.696   84.512  ...    93.0     112.0      91.0   0.98

[8 rows x 11 columns]