0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -571.85    33.73
p_loo       28.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.045   0.157    0.318  ...    29.0      35.0     100.0   1.06
pu        0.817  0.033   0.741    0.860  ...    40.0      37.0      54.0   1.04
mu        0.127  0.023   0.092    0.171  ...    28.0      30.0      74.0   1.04
mus       0.160  0.024   0.124    0.206  ...    70.0      73.0      45.0   1.00
gamma     0.230  0.038   0.157    0.300  ...    66.0      62.0      58.0   1.04
Is_begin  0.561  0.512   0.057    1.800  ...    56.0      44.0      68.0   1.02
Ia_begin  0.923  1.165   0.008    2.621  ...    52.0      52.0      59.0   1.00
E_begin   0.431  0.599   0.007    1.337  ...    61.0      48.0      96.0   1.02

[8 rows x 11 columns]