0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1209.77    28.31
p_loo       20.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.276  0.042   0.203    0.346  ...    19.0      20.0      48.0   1.11
pu        0.818  0.017   0.780    0.840  ...     8.0       8.0      65.0   1.26
mu        0.125  0.024   0.084    0.167  ...     6.0       6.0      59.0   1.39
mus       0.166  0.037   0.102    0.241  ...    36.0      58.0      59.0   1.04
gamma     0.181  0.036   0.129    0.256  ...   123.0     128.0      99.0   1.01
Is_begin  0.777  0.791   0.015    2.513  ...    96.0      84.0      60.0   1.01
Ia_begin  0.625  0.588   0.004    1.755  ...   110.0      49.0      60.0   1.00
E_begin   0.504  0.572   0.016    1.497  ...    80.0      68.0      58.0   0.99

[8 rows x 11 columns]