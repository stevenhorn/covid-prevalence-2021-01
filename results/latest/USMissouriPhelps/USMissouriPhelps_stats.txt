0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -880.15    43.74
p_loo       25.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.050   0.182    0.338  ...    81.0      65.0      21.0   1.05
pu        0.836  0.027   0.787    0.878  ...    26.0      37.0      22.0   1.04
mu        0.114  0.023   0.074    0.152  ...    68.0      60.0      60.0   1.01
mus       0.167  0.032   0.122    0.231  ...   112.0     128.0      60.0   1.00
gamma     0.179  0.044   0.122    0.283  ...    71.0      56.0      26.0   1.05
Is_begin  0.735  0.783   0.011    2.197  ...   120.0     138.0     102.0   0.99
Ia_begin  1.295  1.472   0.002    4.419  ...    74.0      92.0      59.0   1.00
E_begin   0.549  0.560   0.003    1.752  ...   108.0      94.0      48.0   0.99

[8 rows x 11 columns]