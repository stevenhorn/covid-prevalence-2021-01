0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1805.91    23.92
p_loo       23.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.243   0.061   0.164    0.349  ...    85.0      90.0      40.0   0.99
pu         0.756   0.042   0.700    0.832  ...    55.0      57.0      53.0   1.06
mu         0.110   0.019   0.076    0.141  ...     9.0       9.0      54.0   1.21
mus        0.164   0.027   0.126    0.219  ...   114.0     106.0      87.0   1.00
gamma      0.224   0.044   0.152    0.289  ...   110.0      92.0      54.0   1.00
Is_begin  11.992   8.312   0.486   26.421  ...    48.0      35.0      40.0   1.00
Ia_begin  30.730  14.728   1.609   58.206  ...    77.0      73.0      38.0   1.04
E_begin   38.707  28.357   2.523   88.817  ...    79.0      68.0      60.0   1.01

[8 rows x 11 columns]