0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -648.12    21.85
p_loo       18.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.285  0.054   0.180    0.350  ...    31.0      25.0      24.0   1.09
pu        0.880  0.021   0.836    0.900  ...    48.0      31.0      42.0   1.03
mu        0.127  0.024   0.080    0.162  ...    10.0      10.0      16.0   1.16
mus       0.169  0.034   0.117    0.251  ...   125.0     135.0     100.0   1.00
gamma     0.196  0.037   0.135    0.253  ...   140.0     124.0      46.0   1.00
Is_begin  1.171  1.040   0.037    2.842  ...    89.0      68.0      96.0   1.01
Ia_begin  2.410  2.042   0.028    6.634  ...    56.0      42.0      51.0   1.03
E_begin   1.434  1.271   0.020    4.076  ...    97.0      84.0      69.0   1.00

[8 rows x 11 columns]