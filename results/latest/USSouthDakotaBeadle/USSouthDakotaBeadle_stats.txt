0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -813.91    20.86
p_loo       22.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.210  0.033   0.155    0.263  ...    53.0      53.0      60.0   1.07
pu        0.720  0.013   0.701    0.743  ...    73.0      60.0      39.0   1.02
mu        0.149  0.023   0.107    0.192  ...    10.0       9.0      17.0   1.17
mus       0.205  0.034   0.152    0.266  ...    89.0      50.0      66.0   1.06
gamma     0.278  0.045   0.185    0.342  ...    67.0      44.0      52.0   1.06
Is_begin  0.990  0.828   0.070    2.457  ...    90.0     152.0      79.0   1.04
Ia_begin  0.560  0.654   0.008    1.988  ...   106.0      68.0      16.0   1.02
E_begin   0.527  0.709   0.003    1.678  ...   103.0      84.0      42.0   1.03

[8 rows x 11 columns]