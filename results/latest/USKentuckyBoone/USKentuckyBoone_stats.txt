0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1135.24    28.59
p_loo       25.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.173    0.341  ...   152.0     152.0      59.0   1.08
pu        0.790  0.044   0.713    0.860  ...   152.0     152.0      81.0   1.05
mu        0.115  0.024   0.072    0.159  ...    30.0      33.0      35.0   1.06
mus       0.156  0.027   0.109    0.207  ...    17.0      21.0      87.0   1.08
gamma     0.194  0.033   0.140    0.256  ...   128.0     144.0      72.0   0.99
Is_begin  1.037  0.925   0.013    2.952  ...   152.0     100.0      95.0   1.00
Ia_begin  2.932  2.291   0.088    7.291  ...   110.0     100.0      72.0   1.00
E_begin   1.613  1.607   0.015    4.638  ...    93.0      78.0      84.0   1.00

[8 rows x 11 columns]