0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -492.47    25.13
p_loo       19.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.293  0.047   0.185    0.349  ...    91.0      88.0      76.0   1.01
pu        0.886  0.015   0.851    0.900  ...    84.0      88.0      61.0   1.00
mu        0.132  0.027   0.073    0.173  ...    18.0      17.0      38.0   1.10
mus       0.174  0.038   0.110    0.248  ...    88.0      96.0      91.0   1.17
gamma     0.197  0.035   0.129    0.250  ...    62.0      66.0      56.0   1.02
Is_begin  0.803  0.721   0.036    2.288  ...    75.0      76.0      59.0   1.06
Ia_begin  1.455  1.965   0.004    5.646  ...    78.0      46.0      39.0   0.99
E_begin   0.837  1.134   0.007    2.415  ...    96.0     101.0      51.0   1.05

[8 rows x 11 columns]