0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1053.12    46.33
p_loo       32.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   89.9%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    3    1.0%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.224    0.047   0.150    0.302  ...   148.0      86.0      38.0   1.01
pu          0.744    0.035   0.701    0.810  ...    66.0      63.0      65.0   1.00
mu          0.203    0.020   0.166    0.233  ...    20.0      18.0      39.0   1.09
mus         0.289    0.043   0.213    0.364  ...    93.0     113.0      54.0   1.02
gamma       0.512    0.075   0.418    0.712  ...    79.0      94.0      38.0   1.01
Is_begin   76.537   44.601  10.828  155.372  ...    39.0      69.0      24.0   1.01
Ia_begin  187.687  100.573  10.206  338.414  ...    54.0      55.0      21.0   1.16
E_begin   218.979  135.348  22.230  490.475  ...    57.0      55.0      69.0   1.03

[8 rows x 11 columns]