2 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -872.13    25.39
p_loo       25.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.047   0.187    0.347  ...    49.0      49.0      18.0   1.03
pu        0.777  0.026   0.731    0.824  ...    38.0      45.0      58.0   1.06
mu        0.131  0.026   0.091    0.184  ...    27.0      21.0      58.0   1.12
mus       0.164  0.037   0.111    0.249  ...    36.0      72.0      87.0   1.10
gamma     0.171  0.033   0.127    0.261  ...    23.0      40.0      30.0   1.01
Is_begin  0.817  0.548   0.129    1.878  ...    66.0      59.0      83.0   0.99
Ia_begin  1.554  1.453   0.011    4.678  ...    67.0      48.0      40.0   1.04
E_begin   0.874  0.687   0.008    2.070  ...    63.0      63.0      54.0   1.00

[8 rows x 11 columns]