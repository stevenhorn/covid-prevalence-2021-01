0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -946.67    34.15
p_loo       35.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      261   88.5%
 (0.5, 0.7]   (ok)         25    8.5%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.055   0.162    0.341  ...   152.0     150.0      75.0   1.05
pu        0.835  0.049   0.724    0.898  ...     7.0       8.0      56.0   1.27
mu        0.149  0.022   0.111    0.184  ...     7.0       7.0      35.0   1.29
mus       0.182  0.032   0.123    0.250  ...   152.0     152.0      33.0   1.08
gamma     0.245  0.034   0.191    0.316  ...    43.0      53.0      52.0   1.02
Is_begin  1.263  0.813   0.022    2.912  ...   120.0     102.0      59.0   1.00
Ia_begin  3.024  2.339   0.099    7.034  ...    96.0      83.0      92.0   1.14
E_begin   1.729  1.585   0.023    4.391  ...   108.0     101.0      93.0   0.99

[8 rows x 11 columns]