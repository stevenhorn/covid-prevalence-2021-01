0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1288.37    32.49
p_loo       23.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.239   0.043   0.172    0.324  ...    67.0      66.0      18.0   1.04
pu         0.765   0.045   0.704    0.852  ...    84.0      90.0      99.0   1.00
mu         0.124   0.023   0.088    0.167  ...     8.0       9.0      59.0   1.19
mus        0.177   0.030   0.130    0.232  ...   118.0     108.0      56.0   0.99
gamma      0.243   0.038   0.194    0.324  ...   133.0     152.0      59.0   1.01
Is_begin   8.857   5.101   0.036   16.038  ...    29.0      27.0      24.0   1.15
Ia_begin  23.924  10.926   5.935   41.202  ...   152.0     152.0      59.0   1.01
E_begin   23.539  15.368   0.209   50.490  ...    67.0      42.0      15.0   1.06

[8 rows x 11 columns]