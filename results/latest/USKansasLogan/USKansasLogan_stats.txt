0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -508.45    25.88
p_loo       18.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.050   0.162    0.324  ...    12.0      14.0      60.0   1.12
pu        0.744  0.025   0.701    0.779  ...    30.0      30.0      53.0   1.09
mu        0.140  0.028   0.094    0.191  ...    31.0      29.0      24.0   1.07
mus       0.173  0.039   0.107    0.248  ...    59.0      59.0      59.0   1.01
gamma     0.170  0.034   0.109    0.239  ...    20.0      20.0      21.0   1.08
Is_begin  0.180  0.231   0.002    0.626  ...    26.0      10.0      47.0   1.16
Ia_begin  0.381  0.457   0.010    1.311  ...    38.0      16.0      44.0   1.12
E_begin   0.165  0.247   0.001    0.441  ...    77.0      37.0      60.0   1.07

[8 rows x 11 columns]