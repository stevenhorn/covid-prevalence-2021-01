0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -703.95    38.74
p_loo       29.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.044   0.220    0.349  ...    48.0      47.0      60.0   1.03
pu        0.890  0.010   0.873    0.900  ...    35.0      29.0      42.0   1.05
mu        0.170  0.030   0.130    0.232  ...    15.0      14.0      31.0   1.13
mus       0.178  0.039   0.129    0.267  ...    22.0      18.0      35.0   1.09
gamma     0.217  0.038   0.154    0.289  ...   117.0     141.0      50.0   1.10
Is_begin  1.051  0.883   0.002    2.241  ...   115.0     141.0      58.0   0.99
Ia_begin  2.216  2.550   0.031    8.504  ...    89.0      89.0      58.0   0.99
E_begin   0.876  0.917   0.014    3.226  ...    86.0      74.0      39.0   1.03

[8 rows x 11 columns]