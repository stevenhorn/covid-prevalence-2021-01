0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -476.17    31.61
p_loo       32.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   96.9%
 (0.5, 0.7]   (ok)          4    1.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.042   0.211    0.350  ...    45.0      61.0      24.0   1.09
pu        0.889  0.010   0.870    0.900  ...    30.0      19.0      60.0   1.09
mu        0.130  0.026   0.088    0.182  ...    36.0      41.0      29.0   1.03
mus       0.158  0.029   0.113    0.223  ...   148.0     152.0      48.0   1.04
gamma     0.187  0.033   0.133    0.249  ...   122.0     125.0      88.0   1.02
Is_begin  0.661  0.682   0.003    2.118  ...    98.0      81.0     100.0   1.02
Ia_begin  1.276  1.175   0.031    3.188  ...    89.0     103.0      40.0   1.00
E_begin   0.655  0.646   0.008    1.743  ...    77.0      87.0      84.0   1.04

[8 rows x 11 columns]