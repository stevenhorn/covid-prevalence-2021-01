0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1702.64    34.85
p_loo       29.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.235   0.055   0.166    0.346  ...    85.0      85.0      59.0   1.00
pu         0.763   0.043   0.703    0.844  ...    43.0      50.0      40.0   1.04
mu         0.127   0.022   0.095    0.173  ...    11.0      11.0      24.0   1.16
mus        0.172   0.034   0.119    0.231  ...    65.0      67.0      48.0   1.02
gamma      0.240   0.044   0.176    0.333  ...   119.0     152.0      50.0   1.04
Is_begin  12.313   6.037   0.426   22.427  ...    83.0      80.0      57.0   1.01
Ia_begin  31.046  18.616   2.445   58.899  ...    11.0       9.0      36.0   1.17
E_begin   33.486  24.338   0.461   81.899  ...    61.0      55.0      96.0   1.02

[8 rows x 11 columns]