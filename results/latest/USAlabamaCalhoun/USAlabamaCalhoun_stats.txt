0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1193.30    49.11
p_loo       33.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.050   0.156    0.320  ...    95.0     101.0      60.0   1.00
pu        0.810  0.018   0.772    0.841  ...    30.0      31.0      58.0   1.05
mu        0.140  0.019   0.112    0.177  ...    23.0      25.0      33.0   1.09
mus       0.189  0.031   0.141    0.245  ...   122.0     128.0      91.0   0.99
gamma     0.242  0.036   0.188    0.300  ...   152.0     119.0      39.0   1.06
Is_begin  1.172  1.148   0.013    3.211  ...   152.0     128.0      59.0   1.05
Ia_begin  2.750  2.469   0.010    6.680  ...    82.0      47.0      14.0   1.01
E_begin   1.356  1.482   0.028    5.035  ...   124.0     106.0      97.0   1.02

[8 rows x 11 columns]