0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -507.75    39.79
p_loo       31.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.298  0.043   0.206    0.348  ...   135.0     152.0      55.0   0.99
pu        0.890  0.010   0.875    0.900  ...    75.0      78.0      43.0   1.03
mu        0.188  0.025   0.147    0.235  ...    71.0      68.0      57.0   1.00
mus       0.207  0.043   0.132    0.285  ...   152.0     152.0      40.0   1.01
gamma     0.275  0.044   0.211    0.361  ...   124.0     110.0      88.0   1.00
Is_begin  0.906  0.840   0.010    2.377  ...   128.0      54.0      69.0   1.02
Ia_begin  1.832  1.861   0.065    5.308  ...    47.0      26.0      46.0   1.05
E_begin   0.905  0.903   0.011    2.402  ...    38.0      17.0      31.0   1.10

[8 rows x 11 columns]