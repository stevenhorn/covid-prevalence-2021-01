0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -801.99    39.57
p_loo       40.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.057   0.172    0.350  ...    95.0     120.0      60.0   1.05
pu        0.788  0.033   0.728    0.853  ...    59.0      63.0      22.0   1.02
mu        0.188  0.026   0.144    0.234  ...    30.0      33.0      93.0   1.03
mus       0.244  0.032   0.190    0.295  ...   124.0     116.0      83.0   0.99
gamma     0.377  0.053   0.290    0.464  ...    79.0      78.0      62.0   1.00
Is_begin  1.220  1.007   0.005    3.409  ...    54.0      24.0      24.0   1.07
Ia_begin  0.758  0.544   0.017    1.784  ...    69.0      57.0      42.0   1.03
E_begin   0.904  0.896   0.005    2.836  ...    77.0      81.0      45.0   1.01

[8 rows x 11 columns]