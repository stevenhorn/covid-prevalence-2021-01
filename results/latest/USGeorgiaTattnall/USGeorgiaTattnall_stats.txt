0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -848.75    49.05
p_loo       32.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.054   0.168    0.340  ...    74.0      77.0      88.0   1.00
pu        0.828  0.029   0.765    0.868  ...    46.0      59.0      79.0   1.07
mu        0.120  0.026   0.079    0.178  ...    11.0       9.0      27.0   1.21
mus       0.167  0.029   0.120    0.220  ...    58.0      58.0      60.0   1.01
gamma     0.197  0.038   0.133    0.271  ...   142.0     151.0      54.0   1.03
Is_begin  0.788  0.730   0.006    2.230  ...   103.0      73.0      65.0   1.00
Ia_begin  1.218  1.084   0.044    3.427  ...    79.0      64.0      60.0   1.01
E_begin   0.681  0.659   0.005    1.850  ...    30.0     124.0      46.0   1.02

[8 rows x 11 columns]