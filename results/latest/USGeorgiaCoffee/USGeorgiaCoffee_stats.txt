0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1149.88    57.48
p_loo       45.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.049   0.159    0.318  ...   103.0     100.0      56.0   1.01
pu        0.747  0.031   0.703    0.799  ...    49.0      48.0      60.0   1.03
mu        0.131  0.023   0.091    0.170  ...    43.0      44.0      59.0   1.01
mus       0.208  0.037   0.145    0.270  ...    92.0     125.0      60.0   1.01
gamma     0.268  0.049   0.189    0.367  ...   116.0     112.0     100.0   1.01
Is_begin  1.112  0.890   0.003    2.482  ...   152.0     152.0      55.0   0.98
Ia_begin  2.159  2.025   0.020    5.970  ...    76.0      84.0      57.0   1.02
E_begin   1.155  1.223   0.008    3.702  ...   115.0      90.0      58.0   1.03

[8 rows x 11 columns]