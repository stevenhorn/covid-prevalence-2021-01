0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -900.55    36.13
p_loo       27.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.054   0.160    0.333  ...    13.0      13.0      24.0   1.14
pu        0.858  0.019   0.829    0.895  ...     8.0       8.0      58.0   1.23
mu        0.127  0.017   0.096    0.154  ...    21.0      21.0      42.0   1.07
mus       0.161  0.035   0.104    0.224  ...    42.0      41.0      59.0   1.03
gamma     0.172  0.028   0.128    0.218  ...    37.0      38.0     100.0   1.06
Is_begin  0.821  0.793   0.034    2.536  ...    65.0     104.0      59.0   1.02
Ia_begin  1.341  1.547   0.005    4.317  ...   108.0      66.0      96.0   1.02
E_begin   0.586  0.898   0.005    2.722  ...    53.0      62.0      59.0   1.04

[8 rows x 11 columns]