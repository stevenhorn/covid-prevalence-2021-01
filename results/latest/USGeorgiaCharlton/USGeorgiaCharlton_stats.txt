0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -839.62    60.85
p_loo       39.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.050   0.176    0.344  ...    86.0      84.0      58.0   1.02
pu        0.793  0.032   0.743    0.844  ...    17.0      14.0      15.0   1.11
mu        0.146  0.021   0.114    0.186  ...    46.0      52.0      40.0   1.01
mus       0.211  0.034   0.157    0.276  ...    69.0      66.0      49.0   1.01
gamma     0.315  0.052   0.242    0.441  ...    74.0      69.0      61.0   1.01
Is_begin  0.656  0.786   0.001    1.812  ...    92.0      72.0      56.0   1.02
Ia_begin  1.124  1.287   0.007    4.203  ...    46.0      41.0      40.0   1.07
E_begin   0.436  0.435   0.007    1.526  ...   111.0     102.0      88.0   1.00

[8 rows x 11 columns]