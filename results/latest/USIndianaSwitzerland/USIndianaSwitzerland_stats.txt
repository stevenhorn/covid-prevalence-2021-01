0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -513.26    27.66
p_loo       23.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.047   0.172    0.327  ...   127.0     127.0      40.0   1.01
pu        0.880  0.013   0.856    0.900  ...    83.0      70.0      43.0   1.03
mu        0.135  0.024   0.092    0.174  ...    62.0      66.0      93.0   1.02
mus       0.185  0.029   0.146    0.255  ...   124.0     152.0      59.0   1.01
gamma     0.231  0.039   0.151    0.296  ...   136.0     152.0      54.0   1.01
Is_begin  1.083  0.821   0.001    2.603  ...    52.0      38.0      43.0   1.05
Ia_begin  2.929  2.066   0.085    6.484  ...   113.0     129.0      58.0   0.99
E_begin   1.399  1.336   0.019    3.784  ...    77.0      83.0      75.0   1.00

[8 rows x 11 columns]