2 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -744.36    25.88
p_loo       21.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.042   0.187    0.327  ...    37.0      39.0      60.0   1.04
pu        0.810  0.025   0.746    0.848  ...    14.0      20.0      15.0   1.14
mu        0.106  0.020   0.075    0.135  ...    17.0      12.0      49.0   1.14
mus       0.154  0.034   0.097    0.207  ...    36.0      50.0      30.0   1.05
gamma     0.160  0.031   0.102    0.215  ...    33.0      31.0      70.0   1.05
Is_begin  0.578  0.584   0.011    1.686  ...   114.0      84.0      60.0   0.99
Ia_begin  1.338  1.237   0.000    4.562  ...    68.0      57.0      31.0   1.03
E_begin   0.591  0.556   0.030    1.787  ...    93.0      80.0      93.0   1.02

[8 rows x 11 columns]