0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -630.07    32.42
p_loo       25.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.060   0.152    0.339  ...    59.0      47.0      49.0   1.06
pu        0.848  0.015   0.823    0.873  ...   152.0     152.0      93.0   0.99
mu        0.130  0.018   0.101    0.161  ...    13.0      21.0      35.0   1.15
mus       0.163  0.038   0.112    0.228  ...   152.0     152.0      54.0   1.00
gamma     0.253  0.046   0.189    0.352  ...   152.0     152.0      63.0   1.00
Is_begin  0.690  0.592   0.021    1.663  ...   105.0      69.0      59.0   0.98
Ia_begin  1.768  1.436   0.081    4.592  ...   119.0     140.0      87.0   1.00
E_begin   0.841  0.951   0.005    2.362  ...   119.0      81.0      88.0   0.99

[8 rows x 11 columns]