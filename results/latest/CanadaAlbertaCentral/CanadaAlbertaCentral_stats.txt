0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1224.84    42.23
p_loo       34.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   88.7%
 (0.5, 0.7]   (ok)         26    8.4%
   (0.7, 1]   (bad)         8    2.6%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.281  0.051   0.184    0.350  ...   101.0      94.0      32.0   1.00
pu        0.884  0.016   0.841    0.900  ...   111.0     138.0      60.0   1.01
mu        0.182  0.027   0.138    0.236  ...     8.0       7.0      24.0   1.31
mus       0.196  0.036   0.149    0.265  ...   139.0     100.0      37.0   1.00
gamma     0.270  0.051   0.189    0.365  ...    63.0      62.0      55.0   1.00
Is_begin  2.108  1.922   0.007    5.481  ...    51.0      30.0      38.0   1.03
Ia_begin  3.009  2.220   0.017    7.725  ...    77.0      85.0      86.0   1.08
E_begin   1.650  1.610   0.041    4.946  ...    46.0      40.0      60.0   1.05

[8 rows x 11 columns]