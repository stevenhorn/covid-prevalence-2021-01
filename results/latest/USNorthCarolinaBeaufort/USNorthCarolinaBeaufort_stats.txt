0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -894.13    30.17
p_loo       25.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.045   0.160    0.334  ...   152.0     152.0      57.0   0.99
pu        0.781  0.029   0.716    0.825  ...   117.0     129.0      53.0   1.01
mu        0.116  0.024   0.076    0.166  ...    25.0      18.0      69.0   1.10
mus       0.167  0.034   0.094    0.225  ...   152.0     152.0      93.0   1.00
gamma     0.191  0.031   0.131    0.239  ...   114.0     115.0     102.0   0.99
Is_begin  0.680  0.713   0.001    2.084  ...   139.0      88.0      55.0   1.01
Ia_begin  1.341  1.335   0.014    4.151  ...    56.0      36.0      74.0   1.04
E_begin   0.610  0.664   0.003    1.852  ...    93.0      64.0      59.0   1.05

[8 rows x 11 columns]