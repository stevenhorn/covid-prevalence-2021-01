1 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1476.32    31.72
p_loo       22.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.059   0.171    0.343  ...    14.0      21.0      32.0   1.06
pu        0.797  0.028   0.746    0.847  ...    31.0      37.0      17.0   1.04
mu        0.141  0.019   0.106    0.176  ...    17.0      17.0      40.0   1.11
mus       0.180  0.030   0.128    0.233  ...    72.0      83.0      60.0   1.01
gamma     0.228  0.035   0.165    0.285  ...    73.0      76.0      60.0   0.98
Is_begin  0.710  0.713   0.013    2.276  ...   127.0     111.0      60.0   1.02
Ia_begin  1.349  1.291   0.024    3.988  ...    65.0      81.0      57.0   1.10
E_begin   0.511  0.710   0.010    1.705  ...    92.0      97.0      58.0   0.99

[8 rows x 11 columns]