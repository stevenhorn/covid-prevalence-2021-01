0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1503.92    63.87
p_loo       52.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    5    1.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.176  0.018   0.152    0.207  ...    82.0      88.0      56.0   1.02
pu        0.709  0.008   0.700    0.725  ...    97.0     125.0      87.0   1.03
mu        0.141  0.017   0.115    0.170  ...    31.0      34.0      17.0   1.00
mus       0.223  0.031   0.169    0.278  ...    51.0      49.0      34.0   1.05
gamma     0.391  0.063   0.261    0.489  ...    76.0      74.0      34.0   0.99
Is_begin  0.572  0.647   0.015    1.977  ...    42.0      22.0      59.0   1.04
Ia_begin  1.040  1.287   0.012    3.838  ...    47.0      39.0      25.0   1.01
E_begin   0.363  0.410   0.004    1.356  ...    43.0      35.0      58.0   1.00

[8 rows x 11 columns]