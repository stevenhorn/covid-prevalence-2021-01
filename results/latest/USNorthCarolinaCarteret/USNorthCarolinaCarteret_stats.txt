0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -901.09    31.93
p_loo       26.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.051   0.191    0.346  ...   114.0     114.0      91.0   0.99
pu        0.880  0.018   0.835    0.900  ...    48.0      34.0      65.0   1.08
mu        0.120  0.024   0.075    0.158  ...     8.0       9.0      19.0   1.23
mus       0.147  0.032   0.089    0.204  ...   152.0     152.0      93.0   1.00
gamma     0.173  0.032   0.120    0.238  ...   121.0     125.0      59.0   1.00
Is_begin  0.563  0.583   0.000    1.935  ...   140.0     130.0      40.0   1.01
Ia_begin  1.058  0.982   0.044    3.121  ...   137.0      70.0      61.0   1.04
E_begin   0.468  0.493   0.003    1.505  ...    40.0      42.0      42.0   1.06

[8 rows x 11 columns]