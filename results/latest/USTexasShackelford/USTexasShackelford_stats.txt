0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -551.70    54.14
p_loo       41.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.056   0.156    0.335  ...    64.0      57.0      59.0   1.02
pu        0.812  0.020   0.781    0.854  ...    24.0      22.0      59.0   1.07
mu        0.129  0.026   0.084    0.180  ...     8.0       8.0      80.0   1.23
mus       0.203  0.039   0.144    0.278  ...    62.0      76.0      54.0   1.02
gamma     0.247  0.044   0.168    0.306  ...    68.0     108.0      59.0   1.02
Is_begin  0.477  0.429   0.010    1.473  ...    51.0      52.0      39.0   1.04
Ia_begin  0.945  1.253   0.006    3.762  ...    61.0      32.0      60.0   1.03
E_begin   0.451  0.627   0.027    1.586  ...    74.0      54.0     102.0   1.00

[8 rows x 11 columns]