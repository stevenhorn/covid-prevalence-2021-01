0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -584.09    26.13
p_loo       21.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   96.3%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.052   0.173    0.349  ...    73.0      70.0      42.0   0.99
pu        0.845  0.017   0.822    0.875  ...    67.0      62.0      69.0   1.00
mu        0.133  0.026   0.096    0.189  ...    32.0      30.0      52.0   1.03
mus       0.170  0.034   0.115    0.226  ...   116.0      93.0      57.0   1.01
gamma     0.205  0.035   0.140    0.269  ...   148.0     152.0      65.0   1.04
Is_begin  0.566  0.730   0.013    1.987  ...   114.0      83.0      99.0   1.01
Ia_begin  1.122  1.039   0.023    3.141  ...    71.0      79.0     100.0   0.99
E_begin   0.625  0.729   0.023    2.275  ...   109.0     111.0     100.0   0.99

[8 rows x 11 columns]