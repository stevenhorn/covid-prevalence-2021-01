0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -416.89    45.60
p_loo       33.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.047   0.162    0.320  ...    70.0      67.0      39.0   1.00
pu        0.863  0.013   0.844    0.888  ...    71.0      72.0      83.0   0.99
mu        0.131  0.019   0.101    0.169  ...    61.0      58.0      70.0   0.99
mus       0.186  0.043   0.114    0.267  ...   134.0     152.0      32.0   1.02
gamma     0.221  0.040   0.160    0.303  ...   152.0     152.0      72.0   1.01
Is_begin  0.842  0.759   0.015    2.303  ...   152.0     118.0      74.0   1.01
Ia_begin  1.725  1.672   0.015    4.384  ...    72.0      78.0      59.0   1.00
E_begin   0.954  1.329   0.000    3.526  ...    76.0      81.0      51.0   0.99

[8 rows x 11 columns]