0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -500.00    22.34
p_loo       19.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.049   0.157    0.331  ...    81.0      90.0      60.0   0.98
pu        0.879  0.016   0.845    0.900  ...    70.0      66.0      91.0   1.05
mu        0.130  0.025   0.098    0.177  ...    42.0      30.0      52.0   1.08
mus       0.160  0.030   0.110    0.213  ...   139.0     126.0      55.0   1.04
gamma     0.183  0.029   0.140    0.244  ...   128.0     110.0      86.0   1.01
Is_begin  1.014  0.882   0.009    2.606  ...   131.0     117.0      39.0   1.00
Ia_begin  2.385  2.073   0.168    6.081  ...    99.0      58.0      73.0   1.00
E_begin   1.182  1.171   0.027    3.102  ...    81.0      64.0      60.0   1.01

[8 rows x 11 columns]