0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -858.53    24.34
p_loo       21.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.061   0.156    0.342  ...   105.0      95.0      59.0   1.07
pu        0.743  0.035   0.700    0.808  ...    13.0       8.0      46.0   1.21
mu        0.131  0.027   0.080    0.172  ...    26.0      24.0      32.0   1.08
mus       0.169  0.035   0.110    0.241  ...    90.0      72.0      69.0   1.00
gamma     0.199  0.038   0.132    0.260  ...   148.0     152.0      75.0   0.99
Is_begin  1.069  1.066   0.021    3.036  ...   147.0     152.0      93.0   1.00
Ia_begin  2.220  2.013   0.031    6.093  ...    36.0      72.0      20.0   1.05
E_begin   1.211  1.381   0.015    4.256  ...    82.0      44.0     100.0   1.08

[8 rows x 11 columns]