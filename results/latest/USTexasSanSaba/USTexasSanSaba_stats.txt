0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -714.10    58.80
p_loo       44.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.227  0.046   0.158    0.305  ...    66.0      64.0      38.0   1.01
pu        0.732  0.021   0.700    0.769  ...    91.0      71.0      36.0   1.00
mu        0.127  0.028   0.075    0.181  ...    37.0      28.0      56.0   1.07
mus       0.229  0.034   0.177    0.304  ...    45.0      48.0      74.0   1.11
gamma     0.289  0.060   0.169    0.378  ...    36.0      32.0      60.0   1.07
Is_begin  0.531  0.554   0.005    1.400  ...    73.0      54.0      60.0   1.00
Ia_begin  0.662  0.898   0.005    2.467  ...   105.0      73.0      57.0   0.99
E_begin   0.333  0.396   0.005    1.114  ...    79.0      54.0      59.0   1.02

[8 rows x 11 columns]