14 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1356.66    36.55
p_loo       25.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.047   0.181    0.327  ...    92.0      98.0      38.0   1.15
pu        0.793  0.045   0.712    0.857  ...    26.0      43.0      53.0   1.06
mu        0.105  0.024   0.071    0.158  ...    38.0      36.0      56.0   1.04
mus       0.172  0.027   0.122    0.230  ...   102.0     102.0      60.0   1.06
gamma     0.194  0.035   0.136    0.253  ...    64.0      71.0      83.0   1.03
Is_begin  2.007  1.315   0.075    4.201  ...    74.0      82.0      40.0   1.07
Ia_begin  3.333  2.974   0.080    9.336  ...    52.0      66.0      60.0   1.09
E_begin   2.097  2.755   0.006    8.800  ...    41.0      35.0      45.0   1.08

[8 rows x 11 columns]