0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -537.93    32.00
p_loo       23.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.047   0.172    0.331  ...    72.0      59.0      24.0   1.03
pu        0.883  0.018   0.852    0.900  ...     6.0       5.0      18.0   1.41
mu        0.119  0.019   0.094    0.155  ...    48.0      53.0      69.0   1.02
mus       0.206  0.040   0.128    0.268  ...   152.0     138.0      19.0   1.07
gamma     0.297  0.044   0.211    0.366  ...   111.0     113.0      62.0   1.03
Is_begin  0.583  0.654   0.020    1.791  ...    53.0      52.0      60.0   1.01
Ia_begin  0.902  1.070   0.002    3.082  ...    53.0      60.0      34.0   1.06
E_begin   0.495  0.575   0.005    1.552  ...    46.0      75.0      40.0   1.01

[8 rows x 11 columns]