7 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1405.73    25.39
p_loo       23.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.062   0.151    0.340  ...   152.0     152.0      81.0   1.04
pu        0.784  0.050   0.712    0.869  ...    39.0      45.0      59.0   1.01
mu        0.124  0.021   0.092    0.166  ...    17.0      24.0      30.0   1.14
mus       0.169  0.024   0.127    0.214  ...   152.0     152.0      54.0   1.05
gamma     0.241  0.042   0.170    0.315  ...    80.0      84.0     100.0   1.00
Is_begin  1.687  1.458   0.070    4.532  ...    94.0      44.0      24.0   1.05
Ia_begin  3.900  3.100   0.129   10.052  ...    82.0      55.0      60.0   1.04
E_begin   2.931  3.015   0.075    8.855  ...    44.0      29.0      53.0   1.10

[8 rows x 11 columns]