0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -421.41    30.24
p_loo       31.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.052   0.178    0.346  ...    58.0      51.0      34.0   1.03
pu        0.874  0.026   0.831    0.899  ...    34.0      35.0      46.0   1.04
mu        0.170  0.034   0.095    0.219  ...    27.0      27.0      24.0   1.06
mus       0.207  0.035   0.140    0.269  ...   102.0     108.0      38.0   1.06
gamma     0.282  0.050   0.203    0.367  ...   152.0     152.0      65.0   1.00
Is_begin  2.030  1.174   0.110    4.326  ...    76.0      81.0      53.0   1.12
Ia_begin  0.782  0.550   0.054    1.863  ...    89.0      70.0      43.0   1.01
E_begin   1.424  1.144   0.074    3.600  ...   118.0      56.0      35.0   1.01

[8 rows x 11 columns]