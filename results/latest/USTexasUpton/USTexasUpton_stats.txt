0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -684.52    56.14
p_loo       47.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.049   0.154    0.320  ...    23.0      18.0      21.0   1.08
pu        0.754  0.028   0.702    0.796  ...    18.0      20.0      73.0   1.07
mu        0.117  0.021   0.076    0.152  ...    32.0      30.0      88.0   1.04
mus       0.165  0.032   0.106    0.214  ...    97.0     118.0      76.0   1.02
gamma     0.201  0.038   0.151    0.277  ...    68.0      77.0      60.0   1.02
Is_begin  0.261  0.295   0.004    0.775  ...    27.0      14.0      61.0   1.12
Ia_begin  0.458  0.633   0.001    1.474  ...    85.0      41.0      58.0   1.03
E_begin   0.216  0.259   0.003    0.682  ...    82.0      35.0      60.0   1.03

[8 rows x 11 columns]