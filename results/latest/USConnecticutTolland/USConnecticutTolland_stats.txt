0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1216.03    31.04
p_loo       32.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.052   0.171    0.348  ...    68.0      77.0      59.0   0.98
pu        0.785  0.053   0.700    0.866  ...    49.0      45.0      46.0   1.05
mu        0.130  0.021   0.087    0.164  ...    12.0      12.0      20.0   1.14
mus       0.175  0.032   0.130    0.247  ...    47.0      42.0      67.0   1.03
gamma     0.232  0.041   0.167    0.305  ...   152.0     152.0      72.0   1.01
Is_begin  0.682  0.669   0.012    1.845  ...    36.0      25.0      60.0   1.09
Ia_begin  1.747  1.026   0.126    3.548  ...   143.0     121.0      59.0   1.00
E_begin   1.462  1.138   0.086    3.790  ...    94.0      64.0      38.0   1.01

[8 rows x 11 columns]