0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -860.07    29.50
p_loo       24.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.208  0.038   0.152    0.273  ...    30.0      28.0      56.0   1.04
pu        0.716  0.014   0.700    0.740  ...    30.0      17.0      34.0   1.09
mu        0.131  0.027   0.098    0.192  ...     6.0       6.0      15.0   1.32
mus       0.176  0.041   0.112    0.252  ...    23.0      22.0      15.0   1.01
gamma     0.192  0.039   0.130    0.269  ...    25.0      25.0      29.0   1.01
Is_begin  0.872  0.685   0.054    2.357  ...    45.0      49.0      54.0   1.00
Ia_begin  1.473  1.555   0.108    4.569  ...    16.0       8.0      43.0   1.22
E_begin   0.770  0.747   0.040    2.471  ...    27.0      11.0      15.0   1.14

[8 rows x 11 columns]