0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1048.96    64.37
p_loo       22.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      289   98.0%
 (0.5, 0.7]   (ok)          4    1.4%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.016   0.150    0.212  ...   152.0     107.0      55.0   1.03
pu        0.706  0.005   0.700    0.714  ...   121.0      75.0      40.0   1.03
mu        0.062  0.010   0.048    0.080  ...   112.0     105.0      37.0   1.03
mus       0.240  0.035   0.186    0.306  ...   114.0     143.0      39.0   1.00
gamma     0.461  0.076   0.323    0.596  ...    56.0      57.0      86.0   0.99
Is_begin  0.336  0.402   0.003    0.909  ...    54.0      36.0      37.0   1.05
Ia_begin  0.288  0.329   0.009    0.986  ...    95.0      41.0      31.0   1.04
E_begin   0.190  0.271   0.005    0.574  ...    64.0      12.0      59.0   1.15

[8 rows x 11 columns]