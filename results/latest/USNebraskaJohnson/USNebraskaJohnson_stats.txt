0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -714.23    62.58
p_loo       46.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   96.9%
 (0.5, 0.7]   (ok)          5    1.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.049   0.161    0.329  ...    76.0      81.0      86.0   1.02
pu        0.750  0.026   0.705    0.794  ...    90.0      77.0      40.0   0.99
mu        0.125  0.024   0.087    0.167  ...    31.0      27.0      17.0   1.03
mus       0.231  0.040   0.159    0.308  ...   129.0     127.0     100.0   1.02
gamma     0.279  0.050   0.202    0.374  ...    92.0      96.0      60.0   1.01
Is_begin  0.664  0.731   0.012    2.006  ...    83.0      78.0      54.0   1.02
Ia_begin  1.064  1.396   0.004    3.944  ...   113.0      81.0      38.0   1.01
E_begin   0.540  0.702   0.011    1.482  ...    99.0     142.0      88.0   0.99

[8 rows x 11 columns]