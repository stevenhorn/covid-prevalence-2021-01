0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1148.48    55.19
p_loo       56.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.047   0.182    0.343  ...    81.0      83.0      41.0   1.01
pu        0.831  0.040   0.762    0.888  ...    11.0      13.0      21.0   1.16
mu        0.154  0.019   0.114    0.187  ...    21.0      22.0      57.0   1.08
mus       0.234  0.031   0.180    0.295  ...    39.0      40.0      59.0   1.08
gamma     0.410  0.069   0.293    0.563  ...    71.0      72.0      91.0   1.01
Is_begin  0.876  0.879   0.030    2.484  ...    51.0      98.0      33.0   0.98
Ia_begin  1.505  1.787   0.001    5.774  ...    31.0      14.0      24.0   1.12
E_begin   0.628  0.650   0.004    1.727  ...    65.0      47.0      68.0   1.02

[8 rows x 11 columns]