0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -837.72    28.03
p_loo       34.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.058   0.151    0.327  ...    76.0      67.0      40.0   1.04
pu        0.753  0.026   0.716    0.803  ...    64.0      72.0      54.0   0.98
mu        0.193  0.028   0.131    0.232  ...    54.0      56.0      38.0   1.00
mus       0.258  0.035   0.197    0.321  ...    68.0      76.0      62.0   1.04
gamma     0.394  0.069   0.258    0.508  ...    47.0      57.0      87.0   1.01
Is_begin  0.846  0.726   0.010    2.242  ...    70.0      62.0      37.0   1.00
Ia_begin  1.333  1.505   0.025    4.631  ...   125.0      78.0      59.0   1.01
E_begin   0.653  0.672   0.072    1.649  ...    69.0      59.0      80.0   1.01

[8 rows x 11 columns]