0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1040.27    30.84
p_loo       24.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.045   0.155    0.320  ...    87.0      88.0      56.0   1.02
pu        0.863  0.026   0.820    0.898  ...    61.0      82.0      60.0   1.01
mu        0.149  0.029   0.092    0.194  ...    10.0      10.0      19.0   1.20
mus       0.169  0.034   0.118    0.237  ...    94.0      89.0      40.0   1.01
gamma     0.216  0.048   0.123    0.287  ...    90.0      78.0      30.0   1.00
Is_begin  0.736  0.617   0.057    1.906  ...   102.0      71.0      48.0   1.01
Ia_begin  1.513  1.087   0.082    3.817  ...   145.0     126.0      60.0   1.04
E_begin   1.133  1.218   0.011    3.713  ...   118.0     124.0      93.0   1.01

[8 rows x 11 columns]