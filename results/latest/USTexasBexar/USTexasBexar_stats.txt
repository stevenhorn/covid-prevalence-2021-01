5 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -2085.20    37.15
p_loo       33.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.057   0.174    0.348  ...    86.0     104.0      57.0   1.02
pu        0.820  0.042   0.732    0.883  ...    15.0      18.0      42.0   1.08
mu        0.160  0.026   0.100    0.195  ...    16.0      15.0      33.0   1.12
mus       0.203  0.033   0.156    0.267  ...    85.0      73.0      60.0   1.04
gamma     0.298  0.050   0.221    0.375  ...    57.0      81.0      56.0   1.03
Is_begin  2.557  2.561   0.029    7.041  ...    12.0      14.0      25.0   1.13
Ia_begin  6.588  6.129   0.148   18.262  ...    46.0      32.0      37.0   1.05
E_begin   3.424  3.701   0.116   11.968  ...    56.0      19.0      87.0   1.07

[8 rows x 11 columns]