0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1036.61    25.85
p_loo       25.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      259   87.8%
 (0.5, 0.7]   (ok)         30   10.2%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.042   0.171    0.304  ...   152.0     136.0      96.0   1.00
pu        0.788  0.042   0.715    0.859  ...    16.0      17.0      59.0   1.10
mu        0.131  0.023   0.092    0.171  ...     9.0      10.0      33.0   1.19
mus       0.184  0.033   0.127    0.248  ...   152.0     152.0      91.0   1.01
gamma     0.282  0.052   0.198    0.369  ...   152.0     152.0      93.0   1.01
Is_begin  1.552  1.039   0.036    3.560  ...    50.0      42.0      39.0   1.04
Ia_begin  5.880  3.062   1.094   11.886  ...   152.0     152.0      74.0   1.03
E_begin   5.083  4.473   0.236   14.535  ...    69.0      25.0      40.0   1.07

[8 rows x 11 columns]