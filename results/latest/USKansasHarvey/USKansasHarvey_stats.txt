0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1031.24    31.12
p_loo        9.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.6%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.025   0.167    0.254  ...     3.0       3.0      24.0   2.17
pu        0.801  0.010   0.786    0.817  ...     3.0       3.0      25.0   1.99
mu        0.192  0.015   0.173    0.216  ...     3.0       3.0      36.0   2.22
mus       0.147  0.020   0.111    0.179  ...     4.0       4.0      22.0   1.76
gamma     0.079  0.004   0.073    0.087  ...     8.0       8.0      38.0   1.21
Is_begin  0.984  1.173   0.190    3.944  ...     4.0       6.0      24.0   1.49
Ia_begin  1.303  0.973   0.321    2.487  ...     4.0       3.0      14.0   2.17
E_begin   0.825  0.328   0.120    1.340  ...    13.0      14.0      17.0   1.10

[8 rows x 11 columns]