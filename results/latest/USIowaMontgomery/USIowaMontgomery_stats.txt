0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -609.84    27.09
p_loo       25.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      258   87.5%
 (0.5, 0.7]   (ok)         31   10.5%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.062   0.157    0.342  ...    91.0      80.0      60.0   1.08
pu        0.829  0.029   0.762    0.866  ...    29.0      29.0      38.0   1.03
mu        0.128  0.025   0.093    0.178  ...    67.0      68.0      59.0   1.00
mus       0.166  0.043   0.107    0.247  ...   152.0     152.0      76.0   1.13
gamma     0.180  0.038   0.126    0.269  ...   128.0     138.0      60.0   1.00
Is_begin  0.489  0.561   0.005    1.546  ...    96.0      76.0      60.0   1.00
Ia_begin  1.061  1.250   0.020    3.644  ...   109.0     101.0      91.0   1.03
E_begin   0.454  0.564   0.005    1.299  ...    98.0      79.0      57.0   1.00

[8 rows x 11 columns]