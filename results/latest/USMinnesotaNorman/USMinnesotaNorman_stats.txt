0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -417.03    21.92
p_loo       18.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.045   0.197    0.345  ...   152.0     152.0      51.0   1.02
pu        0.885  0.009   0.870    0.899  ...    46.0      51.0      35.0   1.04
mu        0.127  0.023   0.082    0.164  ...    20.0      22.0      53.0   1.10
mus       0.163  0.031   0.107    0.222  ...   108.0     124.0      91.0   1.03
gamma     0.191  0.036   0.128    0.247  ...    81.0      86.0      59.0   1.01
Is_begin  0.713  0.580   0.043    1.976  ...   114.0      90.0      60.0   0.99
Ia_begin  1.663  1.880   0.029    5.640  ...    70.0     132.0      60.0   0.99
E_begin   1.035  1.452   0.000    3.217  ...    52.0     109.0      57.0   1.04

[8 rows x 11 columns]