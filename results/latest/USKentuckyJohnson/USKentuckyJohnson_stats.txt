0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -710.60    30.52
p_loo       25.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.046   0.186    0.343  ...    59.0      85.0      60.0   1.02
pu        0.868  0.022   0.819    0.898  ...    66.0      57.0      24.0   1.05
mu        0.146  0.029   0.088    0.193  ...    11.0      11.0      59.0   1.17
mus       0.170  0.030   0.128    0.228  ...   147.0     152.0      60.0   1.00
gamma     0.206  0.033   0.156    0.262  ...    69.0      75.0     100.0   1.00
Is_begin  0.794  0.816   0.015    2.542  ...   126.0     110.0      47.0   1.00
Ia_begin  1.606  1.454   0.018    4.781  ...    68.0      97.0      57.0   1.11
E_begin   0.666  0.747   0.013    2.279  ...    38.0      73.0      60.0   1.07

[8 rows x 11 columns]