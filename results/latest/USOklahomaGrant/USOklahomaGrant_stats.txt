0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -490.79    26.67
p_loo       25.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    5    1.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.054   0.153    0.322  ...    62.0      62.0      21.0   1.03
pu        0.786  0.028   0.736    0.834  ...    26.0      27.0      57.0   1.03
mu        0.128  0.025   0.091    0.180  ...    16.0      17.0      74.0   1.08
mus       0.154  0.026   0.105    0.205  ...    52.0      54.0      59.0   1.03
gamma     0.168  0.036   0.114    0.234  ...   109.0      93.0      53.0   1.01
Is_begin  0.543  0.613   0.003    1.950  ...    91.0      79.0      57.0   1.00
Ia_begin  1.018  0.882   0.045    2.897  ...    73.0      66.0      60.0   1.02
E_begin   0.470  0.518   0.002    1.306  ...    90.0      67.0      37.0   1.00

[8 rows x 11 columns]