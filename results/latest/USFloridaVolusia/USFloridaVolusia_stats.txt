0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1389.21    37.39
p_loo       26.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.057   0.167    0.349  ...    85.0      98.0      53.0   1.00
pu        0.782  0.044   0.701    0.846  ...    43.0      42.0      57.0   1.05
mu        0.124  0.019   0.094    0.164  ...    46.0      41.0      59.0   1.02
mus       0.177  0.033   0.100    0.230  ...   152.0     137.0      40.0   1.00
gamma     0.255  0.044   0.175    0.334  ...   105.0     140.0      83.0   0.99
Is_begin  1.813  1.324   0.091    4.393  ...   131.0      83.0      88.0   1.01
Ia_begin  3.824  2.460   0.221    7.974  ...    79.0      71.0      59.0   1.02
E_begin   3.991  3.957   0.082   12.522  ...    70.0      54.0      73.0   1.00

[8 rows x 11 columns]