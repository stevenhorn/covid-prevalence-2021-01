0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -875.35    22.14
p_loo       22.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.052   0.150    0.333  ...    71.0      69.0      60.0   1.03
pu        0.841  0.029   0.786    0.880  ...    44.0      41.0      60.0   1.02
mu        0.105  0.021   0.072    0.141  ...    56.0      54.0     100.0   1.02
mus       0.146  0.033   0.083    0.196  ...    73.0     119.0      38.0   0.98
gamma     0.159  0.033   0.108    0.227  ...   152.0     148.0      69.0   1.00
Is_begin  0.646  0.769   0.026    1.874  ...    95.0     130.0      80.0   1.01
Ia_begin  0.972  1.005   0.007    3.415  ...   121.0     121.0      32.0   1.00
E_begin   0.588  0.931   0.002    1.846  ...    92.0      60.0      46.0   1.00

[8 rows x 11 columns]