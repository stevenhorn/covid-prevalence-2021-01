0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -947.16    20.40
p_loo       20.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         30   10.2%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.052   0.159    0.336  ...   103.0      93.0      60.0   1.03
pu        0.822  0.033   0.762    0.881  ...    13.0      13.0     100.0   1.13
mu        0.142  0.020   0.111    0.180  ...    20.0      24.0      59.0   1.07
mus       0.181  0.030   0.130    0.234  ...   120.0      97.0      38.0   1.01
gamma     0.259  0.041   0.194    0.340  ...    92.0     104.0      87.0   1.06
Is_begin  1.756  1.110   0.286    3.876  ...    74.0      68.0      54.0   1.02
Ia_begin  4.527  3.094   0.149    9.392  ...    90.0      77.0      57.0   1.01
E_begin   4.104  2.900   0.107   10.010  ...    77.0      65.0      31.0   1.01

[8 rows x 11 columns]