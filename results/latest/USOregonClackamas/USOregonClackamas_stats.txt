1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1148.70    25.77
p_loo       21.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.059   0.163    0.349  ...    74.0      64.0      22.0   1.10
pu        0.805  0.056   0.717    0.892  ...    70.0      71.0      38.0   1.02
mu        0.110  0.017   0.083    0.141  ...    12.0      12.0      24.0   1.15
mus       0.161  0.029   0.118    0.217  ...    95.0      76.0      62.0   1.04
gamma     0.201  0.039   0.145    0.295  ...    46.0      45.0      59.0   1.06
Is_begin  1.656  1.328   0.017    4.162  ...   129.0      68.0      40.0   0.98
Ia_begin  5.206  2.810   0.427   10.227  ...    82.0      78.0      73.0   0.99
E_begin   3.154  2.526   0.036    7.705  ...    22.0      59.0      37.0   1.02

[8 rows x 11 columns]