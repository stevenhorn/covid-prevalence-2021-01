0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -777.38    27.14
p_loo       20.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.152    0.337  ...   152.0     152.0      53.0   1.03
pu        0.856  0.033   0.800    0.899  ...    85.0      83.0      42.0   1.00
mu        0.124  0.023   0.085    0.169  ...    66.0      67.0      60.0   1.01
mus       0.155  0.028   0.107    0.216  ...    77.0      81.0      58.0   1.03
gamma     0.175  0.037   0.105    0.239  ...    77.0      87.0      69.0   1.00
Is_begin  0.944  0.697   0.015    2.216  ...   152.0     150.0      60.0   1.00
Ia_begin  1.791  1.651   0.147    5.695  ...    97.0      59.0      83.0   1.05
E_begin   0.864  0.930   0.006    2.850  ...   112.0      90.0      60.0   0.98

[8 rows x 11 columns]