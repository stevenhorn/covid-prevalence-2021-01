0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1197.90    29.08
p_loo       21.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.063   0.160    0.346  ...    31.0      34.0      38.0   1.09
pu        0.803  0.028   0.760    0.850  ...    27.0      30.0      14.0   1.11
mu        0.149  0.027   0.112    0.201  ...     6.0       8.0      18.0   1.21
mus       0.173  0.026   0.130    0.230  ...    35.0      34.0      57.0   1.01
gamma     0.206  0.039   0.140    0.282  ...    82.0      84.0      88.0   1.03
Is_begin  1.076  1.120   0.002    3.701  ...    68.0      56.0      56.0   1.00
Ia_begin  0.678  0.507   0.006    1.618  ...    42.0      29.0      40.0   1.05
E_begin   0.550  0.634   0.005    1.965  ...    61.0      37.0      56.0   1.01

[8 rows x 11 columns]