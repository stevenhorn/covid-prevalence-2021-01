0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -541.01    29.14
p_loo       28.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.051   0.165    0.341  ...    86.0      95.0      88.0   1.00
pu        0.848  0.015   0.821    0.874  ...    58.0      56.0      96.0   0.99
mu        0.135  0.022   0.094    0.176  ...    54.0      58.0     100.0   1.02
mus       0.166  0.027   0.116    0.210  ...   125.0     134.0      79.0   1.01
gamma     0.201  0.046   0.125    0.286  ...    96.0      95.0      97.0   1.01
Is_begin  0.693  0.729   0.003    2.333  ...   112.0      61.0      59.0   1.03
Ia_begin  1.132  1.259   0.008    3.173  ...   108.0      96.0      57.0   0.99
E_begin   0.526  0.698   0.001    2.055  ...    73.0      41.0      33.0   1.05

[8 rows x 11 columns]