0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -940.31    44.77
p_loo       29.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.285  0.051   0.196    0.349  ...    43.0      35.0      54.0   1.06
pu        0.883  0.018   0.842    0.900  ...    34.0      42.0      43.0   1.02
mu        0.123  0.028   0.088    0.190  ...    11.0      11.0      48.0   1.15
mus       0.179  0.033   0.136    0.250  ...    80.0      88.0      88.0   0.99
gamma     0.190  0.039   0.125    0.258  ...    82.0      85.0      88.0   0.99
Is_begin  0.825  0.735   0.028    2.415  ...    81.0      28.0      31.0   1.08
Ia_begin  1.588  1.380   0.023    4.064  ...    16.0      13.0      59.0   1.13
E_begin   0.945  1.125   0.013    2.935  ...    51.0      12.0      74.0   1.16

[8 rows x 11 columns]