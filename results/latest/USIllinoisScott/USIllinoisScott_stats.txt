0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -505.44    33.99
p_loo       28.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.053   0.162    0.345  ...    10.0      10.0      36.0   1.16
pu        0.810  0.036   0.730    0.858  ...    24.0      22.0      74.0   1.08
mu        0.124  0.022   0.087    0.166  ...    11.0       9.0      35.0   1.19
mus       0.165  0.033   0.103    0.222  ...    47.0      56.0      60.0   1.02
gamma     0.194  0.038   0.134    0.257  ...    88.0     100.0      58.0   1.04
Is_begin  0.178  0.270   0.006    0.703  ...    99.0      64.0      65.0   1.02
Ia_begin  0.603  0.864   0.001    2.356  ...    21.0      30.0      59.0   1.06
E_begin   0.135  0.143   0.001    0.434  ...    30.0      27.0      40.0   1.04

[8 rows x 11 columns]