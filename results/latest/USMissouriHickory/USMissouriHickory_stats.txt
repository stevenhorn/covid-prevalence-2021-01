0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -575.51    41.24
p_loo       33.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.047   0.178    0.340  ...   152.0     152.0      63.0   1.02
pu        0.835  0.044   0.737    0.890  ...    27.0      19.0      37.0   1.09
mu        0.130  0.029   0.083    0.176  ...   100.0      60.0      97.0   1.03
mus       0.168  0.032   0.106    0.216  ...   152.0     152.0      63.0   1.11
gamma     0.192  0.035   0.134    0.270  ...   152.0     150.0      56.0   0.99
Is_begin  0.340  0.536   0.000    0.970  ...    74.0      69.0      69.0   1.03
Ia_begin  0.588  0.791   0.002    2.451  ...    57.0      38.0      25.0   1.05
E_begin   0.205  0.260   0.001    0.657  ...    63.0      63.0      93.0   1.00

[8 rows x 11 columns]