0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -521.04    33.44
p_loo       27.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.052   0.182    0.347  ...   114.0      95.0      53.0   1.09
pu        0.866  0.025   0.811    0.899  ...    13.0      13.0      51.0   1.12
mu        0.124  0.023   0.092    0.175  ...    80.0      73.0      20.0   1.01
mus       0.174  0.042   0.098    0.251  ...   152.0     152.0      81.0   1.04
gamma     0.199  0.041   0.137    0.280  ...   151.0     132.0      83.0   1.00
Is_begin  0.343  0.486   0.004    1.441  ...    92.0      80.0      60.0   1.01
Ia_begin  0.509  0.678   0.000    1.837  ...   102.0      81.0      59.0   1.03
E_begin   0.300  0.570   0.001    1.074  ...    97.0      91.0      71.0   0.99

[8 rows x 11 columns]