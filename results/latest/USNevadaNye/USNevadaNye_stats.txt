0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -924.17    28.38
p_loo       24.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.043   0.196    0.350  ...    89.0      84.0      60.0   0.99
pu        0.885  0.013   0.857    0.900  ...    18.0      10.0      34.0   1.19
mu        0.157  0.025   0.114    0.204  ...    20.0      19.0      20.0   1.08
mus       0.168  0.028   0.126    0.226  ...    77.0      81.0      95.0   1.02
gamma     0.226  0.040   0.171    0.296  ...   152.0     152.0      77.0   0.99
Is_begin  0.661  0.748   0.000    2.092  ...    58.0      20.0      17.0   1.08
Ia_begin  2.281  2.026   0.030    6.044  ...   102.0      94.0      60.0   1.01
E_begin   1.091  1.235   0.001    3.918  ...    99.0      55.0      38.0   1.04

[8 rows x 11 columns]