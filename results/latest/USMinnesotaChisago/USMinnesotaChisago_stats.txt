0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.00    31.13
p_loo       22.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.054   0.154    0.336  ...    76.0      79.0      30.0   1.00
pu        0.849  0.019   0.820    0.879  ...    59.0      58.0      99.0   1.03
mu        0.114  0.023   0.080    0.150  ...    12.0      10.0      60.0   1.19
mus       0.157  0.033   0.119    0.223  ...    98.0     108.0      83.0   1.00
gamma     0.167  0.033   0.110    0.219  ...   152.0     152.0      86.0   1.00
Is_begin  0.774  0.720   0.001    2.045  ...    96.0      39.0      29.0   1.03
Ia_begin  1.529  1.514   0.017    4.585  ...   105.0      88.0      40.0   1.01
E_begin   0.809  0.804   0.007    2.790  ...    83.0      84.0      93.0   1.06

[8 rows x 11 columns]