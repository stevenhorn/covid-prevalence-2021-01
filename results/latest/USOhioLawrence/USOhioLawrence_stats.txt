0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -843.95    31.22
p_loo       23.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.049   0.151    0.319  ...    84.0      82.0      58.0   1.01
pu        0.844  0.018   0.817    0.874  ...    59.0      56.0      58.0   1.01
mu        0.118  0.023   0.076    0.158  ...    35.0      35.0      57.0   1.01
mus       0.160  0.032   0.091    0.206  ...   116.0     152.0      47.0   1.03
gamma     0.188  0.045   0.131    0.288  ...   152.0     152.0      96.0   1.03
Is_begin  0.918  0.830   0.043    2.754  ...    88.0     135.0      48.0   1.01
Ia_begin  1.695  1.927   0.004    5.716  ...    62.0      58.0      44.0   1.03
E_begin   0.806  0.928   0.009    2.841  ...    86.0      90.0      88.0   0.98

[8 rows x 11 columns]