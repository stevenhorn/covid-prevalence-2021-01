0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -853.08    21.86
p_loo       19.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.284  0.046   0.202    0.347  ...    61.0      53.0      39.0   0.99
pu        0.870  0.027   0.827    0.899  ...    27.0      28.0      59.0   1.07
mu        0.101  0.023   0.064    0.137  ...     6.0       5.0      17.0   1.39
mus       0.150  0.028   0.100    0.204  ...   111.0     150.0      59.0   1.03
gamma     0.172  0.040   0.090    0.242  ...   152.0     132.0      46.0   1.08
Is_begin  0.843  0.583   0.038    1.922  ...    67.0      71.0      43.0   1.00
Ia_begin  2.125  1.944   0.006    5.872  ...    65.0      59.0      59.0   1.06
E_begin   1.168  1.391   0.026    3.258  ...    91.0      87.0      59.0   1.02

[8 rows x 11 columns]