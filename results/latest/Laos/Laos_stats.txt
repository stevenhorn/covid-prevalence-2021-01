0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -241.56    69.84
p_loo       53.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.3%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.061   0.151    0.340  ...    18.0      25.0      26.0   1.13
pu        0.771  0.053   0.700    0.867  ...    50.0      48.0      60.0   1.02
mu        0.199  0.030   0.142    0.263  ...    12.0      14.0      47.0   1.13
mus       0.316  0.044   0.241    0.395  ...   152.0     124.0      87.0   1.01
gamma     0.417  0.070   0.294    0.547  ...    79.0      72.0      59.0   1.05
Is_begin  1.474  1.110   0.014    3.176  ...   105.0     112.0      93.0   1.01
Ia_begin  2.523  2.079   0.091    5.985  ...    49.0      34.0      60.0   1.05
E_begin   1.617  1.528   0.014    4.712  ...    70.0      52.0      79.0   1.00

[8 rows x 11 columns]