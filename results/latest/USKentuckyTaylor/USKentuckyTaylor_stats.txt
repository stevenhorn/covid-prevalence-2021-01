0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -739.86    28.71
p_loo       19.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.054   0.155    0.332  ...   121.0     102.0      55.0   1.02
pu        0.861  0.014   0.828    0.882  ...    82.0      80.0      95.0   1.00
mu        0.114  0.018   0.084    0.150  ...    71.0      71.0      60.0   1.02
mus       0.154  0.029   0.112    0.213  ...    94.0     100.0      48.0   1.00
gamma     0.167  0.035   0.104    0.226  ...    78.0      83.0      32.0   1.09
Is_begin  0.696  0.635   0.027    1.808  ...    95.0      94.0      59.0   0.99
Ia_begin  1.379  1.534   0.014    3.925  ...    58.0      99.0      74.0   1.04
E_begin   0.681  0.712   0.003    1.901  ...   107.0      78.0      40.0   1.01

[8 rows x 11 columns]