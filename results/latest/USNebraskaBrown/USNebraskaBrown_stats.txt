0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -372.95    31.61
p_loo       32.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.050   0.173    0.338  ...    23.0      26.0      59.0   1.05
pu        0.824  0.019   0.786    0.854  ...    19.0      19.0      29.0   1.09
mu        0.138  0.027   0.085    0.185  ...    36.0      37.0      54.0   1.01
mus       0.175  0.031   0.135    0.253  ...    54.0      47.0      22.0   1.06
gamma     0.207  0.038   0.139    0.283  ...    86.0      81.0      60.0   1.03
Is_begin  0.377  0.519   0.003    1.305  ...    62.0      24.0      56.0   1.05
Ia_begin  0.723  1.107   0.000    3.379  ...    34.0      11.0      24.0   1.16
E_begin   0.255  0.357   0.001    1.003  ...    23.0      13.0      53.0   1.14

[8 rows x 11 columns]