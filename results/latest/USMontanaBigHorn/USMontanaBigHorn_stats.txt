0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -955.19    35.85
p_loo       26.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.217  0.044   0.153    0.294  ...    61.0      61.0      38.0   1.01
pu        0.728  0.018   0.701    0.762  ...    54.0      54.0      58.0   1.00
mu        0.121  0.021   0.086    0.162  ...    12.0      11.0      25.0   1.16
mus       0.169  0.030   0.128    0.248  ...    23.0      25.0      36.0   1.07
gamma     0.203  0.038   0.148    0.293  ...    96.0      98.0      79.0   0.99
Is_begin  0.409  0.521   0.004    0.972  ...    92.0      77.0      43.0   1.00
Ia_begin  0.698  0.872   0.024    2.556  ...    90.0      73.0      93.0   1.04
E_begin   0.337  0.501   0.016    1.086  ...    90.0      75.0      60.0   0.98

[8 rows x 11 columns]