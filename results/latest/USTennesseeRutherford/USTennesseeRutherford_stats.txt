0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1486.00    25.92
p_loo       20.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      264   89.5%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.054   0.153    0.337  ...   151.0     152.0      72.0   1.02
pu        0.796  0.045   0.718    0.857  ...    52.0      54.0     100.0   1.03
mu        0.115  0.027   0.065    0.158  ...    55.0      53.0      40.0   1.01
mus       0.164  0.031   0.106    0.217  ...   104.0     115.0      60.0   1.02
gamma     0.205  0.032   0.143    0.259  ...    71.0      71.0      91.0   1.00
Is_begin  0.817  0.672   0.025    2.144  ...    87.0      68.0      88.0   1.01
Ia_begin  1.463  1.245   0.044    3.597  ...    80.0      52.0      60.0   1.00
E_begin   1.481  1.563   0.018    4.802  ...    80.0     102.0      91.0   1.03

[8 rows x 11 columns]