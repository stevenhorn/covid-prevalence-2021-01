0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -716.02    25.03
p_loo       18.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.052   0.181    0.347  ...   152.0     152.0     100.0   1.01
pu        0.850  0.017   0.819    0.883  ...    13.0      17.0      35.0   1.09
mu        0.122  0.022   0.082    0.153  ...    35.0      30.0      51.0   1.07
mus       0.155  0.025   0.109    0.198  ...    96.0      92.0      86.0   1.02
gamma     0.176  0.037   0.122    0.245  ...    79.0      81.0      60.0   1.01
Is_begin  0.696  0.796   0.014    2.179  ...   115.0     108.0      88.0   1.00
Ia_begin  1.351  1.432   0.007    3.985  ...   106.0     124.0      60.0   1.01
E_begin   0.552  0.656   0.000    1.764  ...    64.0      27.0      59.0   1.05

[8 rows x 11 columns]