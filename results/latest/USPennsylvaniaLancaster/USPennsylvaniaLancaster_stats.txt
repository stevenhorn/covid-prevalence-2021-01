2 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1338.05    22.19
p_loo       24.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      256   86.8%
 (0.5, 0.7]   (ok)         30   10.2%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.232  0.052   0.158    0.339  ...    51.0     152.0      37.0   1.07
pu        0.732  0.024   0.701    0.781  ...   135.0     100.0      51.0   1.07
mu        0.099  0.022   0.063    0.144  ...    45.0      45.0      54.0   1.04
mus       0.166  0.034   0.101    0.220  ...   120.0     152.0      15.0   1.03
gamma     0.300  0.049   0.203    0.378  ...   129.0     122.0      86.0   0.99
Is_begin  0.895  0.656   0.018    2.138  ...    94.0      52.0      40.0   1.04
Ia_begin  2.328  1.909   0.031    5.806  ...    67.0      35.0      43.0   1.06
E_begin   3.195  3.877   0.001   12.058  ...    45.0      13.0      35.0   1.16

[8 rows x 11 columns]