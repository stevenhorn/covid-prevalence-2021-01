0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -832.71    27.88
p_loo       16.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      288   97.6%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.056   0.159    0.331  ...    41.0      30.0      18.0   1.08
pu        0.801  0.022   0.760    0.837  ...    30.0      31.0      92.0   1.07
mu        0.121  0.024   0.090    0.166  ...    14.0      12.0      42.0   1.14
mus       0.154  0.033   0.095    0.198  ...    64.0      63.0      88.0   1.02
gamma     0.164  0.030   0.117    0.226  ...   106.0     100.0      76.0   1.00
Is_begin  0.661  0.670   0.002    1.613  ...   109.0      36.0      40.0   1.05
Ia_begin  1.331  1.473   0.023    4.421  ...    82.0      46.0      59.0   1.06
E_begin   0.619  0.711   0.000    1.909  ...   110.0      75.0      30.0   1.00

[8 rows x 11 columns]