0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -451.40    30.00
p_loo       30.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      250   84.5%
 (0.5, 0.7]   (ok)         32   10.8%
   (0.7, 1]   (bad)        12    4.1%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.056   0.163    0.349  ...   152.0     152.0      59.0   0.99
pu        0.812  0.044   0.734    0.882  ...    74.0      74.0      95.0   1.00
mu        0.152  0.026   0.100    0.194  ...    80.0      77.0      55.0   1.01
mus       0.235  0.034   0.164    0.282  ...    49.0      57.0      43.0   1.01
gamma     0.357  0.046   0.282    0.436  ...   116.0     118.0      79.0   1.05
Is_begin  1.974  1.483   0.080    5.020  ...   152.0     147.0      56.0   1.06
Ia_begin  5.642  3.575   0.470   11.868  ...    16.0      14.0      59.0   1.16
E_begin   6.427  5.492   0.020   17.183  ...    58.0      32.0      34.0   1.05

[8 rows x 11 columns]