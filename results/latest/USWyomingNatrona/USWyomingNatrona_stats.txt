0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1003.67    33.17
p_loo       22.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.057   0.160    0.342  ...    87.0      81.0      57.0   1.05
pu        0.831  0.023   0.790    0.868  ...    23.0      28.0      19.0   1.03
mu        0.117  0.023   0.087    0.153  ...    15.0      17.0      59.0   1.14
mus       0.161  0.029   0.109    0.217  ...    93.0     115.0      31.0   1.11
gamma     0.182  0.031   0.132    0.232  ...    11.0      11.0      24.0   1.15
Is_begin  0.894  1.080   0.006    3.267  ...    64.0      64.0      60.0   1.05
Ia_begin  1.984  1.931   0.025    5.912  ...    74.0      44.0      60.0   1.04
E_begin   1.020  1.224   0.002    4.204  ...    67.0      58.0      59.0   0.99

[8 rows x 11 columns]