0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -918.02    28.95
p_loo       24.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.050   0.154    0.322  ...    78.0      65.0      59.0   1.02
pu        0.803  0.026   0.766    0.857  ...    63.0      62.0      35.0   1.04
mu        0.125  0.030   0.070    0.188  ...    11.0       9.0      42.0   1.17
mus       0.161  0.031   0.118    0.229  ...   118.0     106.0      60.0   1.00
gamma     0.187  0.044   0.099    0.249  ...    74.0      70.0      57.0   1.01
Is_begin  1.100  0.921   0.011    2.750  ...    94.0      68.0      42.0   1.02
Ia_begin  0.760  0.546   0.055    2.014  ...   105.0      38.0      40.0   1.04
E_begin   0.680  0.631   0.017    1.578  ...    60.0      70.0      75.0   1.00

[8 rows x 11 columns]