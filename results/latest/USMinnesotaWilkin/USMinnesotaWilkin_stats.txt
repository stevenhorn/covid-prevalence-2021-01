0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -493.00    27.49
p_loo       24.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.052   0.157    0.329  ...    96.0      99.0      58.0   1.03
pu        0.837  0.018   0.803    0.864  ...    94.0      93.0      60.0   0.99
mu        0.126  0.023   0.085    0.167  ...    58.0      61.0      70.0   0.99
mus       0.177  0.031   0.100    0.228  ...   152.0     152.0      67.0   1.02
gamma     0.194  0.037   0.137    0.261  ...   110.0      94.0      53.0   1.01
Is_begin  0.958  0.849   0.014    2.580  ...   105.0     152.0      59.0   1.01
Ia_begin  1.572  1.696   0.014    4.848  ...    95.0      76.0      56.0   1.04
E_begin   0.909  1.024   0.001    2.975  ...    98.0     105.0      59.0   1.00

[8 rows x 11 columns]