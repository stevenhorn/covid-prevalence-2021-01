0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -259.53    38.74
p_loo       40.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.054   0.150    0.316  ...     8.0       7.0      40.0   1.26
pu        0.749  0.042   0.701    0.834  ...     9.0      10.0      49.0   1.20
mu        0.138  0.029   0.090    0.199  ...    16.0      13.0      74.0   1.13
mus       0.261  0.044   0.173    0.329  ...    72.0      84.0      60.0   1.02
gamma     0.288  0.046   0.205    0.361  ...    86.0      84.0      81.0   0.99
Is_begin  0.308  0.437   0.001    1.331  ...    46.0      16.0      44.0   1.11
Ia_begin  0.355  0.446   0.001    1.239  ...    21.0      10.0      40.0   1.17
E_begin   0.173  0.212   0.006    0.636  ...    16.0      12.0      67.0   1.15

[8 rows x 11 columns]