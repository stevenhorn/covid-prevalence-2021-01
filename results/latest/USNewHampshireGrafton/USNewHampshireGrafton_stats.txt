0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -644.78    28.40
p_loo       24.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.043   0.199    0.344  ...    74.0      82.0      59.0   1.13
pu        0.874  0.022   0.831    0.899  ...    38.0      34.0      46.0   1.07
mu        0.128  0.027   0.082    0.177  ...     7.0       7.0      37.0   1.24
mus       0.182  0.038   0.117    0.253  ...    29.0      38.0      38.0   1.01
gamma     0.227  0.045   0.162    0.332  ...    38.0      40.0      72.0   1.04
Is_begin  2.189  1.221   0.360    4.348  ...   105.0      95.0      72.0   1.00
Ia_begin  5.599  2.902   1.069   11.459  ...    83.0      99.0      88.0   1.01
E_begin   3.759  3.259   0.189   10.056  ...    55.0      53.0      60.0   1.02

[8 rows x 11 columns]