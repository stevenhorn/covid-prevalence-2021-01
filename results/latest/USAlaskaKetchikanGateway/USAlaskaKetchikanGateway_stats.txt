0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -456.28    26.46
p_loo       32.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.288  0.048   0.208    0.349  ...    10.0       9.0      24.0   1.17
pu        0.872  0.028   0.822    0.898  ...    48.0      59.0      81.0   1.01
mu        0.174  0.030   0.123    0.226  ...    21.0      25.0      59.0   1.06
mus       0.211  0.046   0.147    0.321  ...   149.0     123.0      80.0   1.00
gamma     0.292  0.051   0.194    0.385  ...    51.0      49.0      39.0   1.01
Is_begin  1.670  1.288   0.137    4.423  ...    19.0      12.0      23.0   1.13
Ia_begin  0.737  0.626   0.009    2.097  ...   124.0      74.0      17.0   1.04
E_begin   0.878  0.930   0.009    2.478  ...    90.0      56.0      15.0   1.07

[8 rows x 11 columns]