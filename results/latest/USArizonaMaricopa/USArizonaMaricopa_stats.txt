0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -2141.86    33.24
p_loo       25.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.225   0.052   0.152    0.308  ...    49.0      22.0      17.0   1.06
pu         0.744   0.029   0.700    0.791  ...    55.0      45.0      14.0   1.05
mu         0.120   0.022   0.070    0.151  ...    10.0      11.0      14.0   1.17
mus        0.171   0.025   0.134    0.222  ...    65.0      69.0      54.0   1.01
gamma      0.251   0.045   0.180    0.338  ...    62.0      63.0      43.0   1.01
Is_begin   8.402   7.622   0.236   22.061  ...     7.0       5.0      19.0   1.45
Ia_begin  26.249  13.777   0.402   51.683  ...    29.0      27.0      22.0   1.09
E_begin   25.876  16.450   1.087   58.635  ...    79.0      64.0      31.0   1.00

[8 rows x 11 columns]