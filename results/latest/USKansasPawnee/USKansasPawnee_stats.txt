0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -931.44    42.28
p_loo       31.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.012   0.153    0.196  ...     8.0       6.0      14.0   1.33
pu        0.703  0.003   0.700    0.709  ...     3.0       3.0      20.0   2.27
mu        0.169  0.024   0.130    0.215  ...     4.0       5.0      14.0   1.49
mus       0.190  0.030   0.140    0.249  ...     4.0       4.0      15.0   1.76
gamma     0.237  0.031   0.184    0.289  ...     5.0       6.0      42.0   1.31
Is_begin  0.503  0.402   0.022    1.176  ...     4.0       4.0      16.0   1.54
Ia_begin  0.393  0.313   0.006    0.944  ...     6.0       5.0      14.0   1.47
E_begin   0.126  0.101   0.007    0.278  ...     4.0       4.0      32.0   1.67

[8 rows x 11 columns]