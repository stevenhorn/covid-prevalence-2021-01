0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -538.16    26.93
p_loo       26.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.057   0.167    0.346  ...    89.0      67.0      58.0   1.02
pu        0.818  0.027   0.767    0.855  ...    39.0      44.0      40.0   1.02
mu        0.134  0.025   0.088    0.183  ...    62.0      73.0      93.0   1.00
mus       0.170  0.037   0.114    0.253  ...   152.0     148.0      40.0   1.04
gamma     0.198  0.044   0.122    0.263  ...   152.0     152.0      53.0   0.99
Is_begin  0.405  0.472   0.006    1.109  ...   101.0      70.0      59.0   1.02
Ia_begin  0.634  0.685   0.001    2.005  ...    82.0      54.0      40.0   1.00
E_begin   0.314  0.483   0.003    1.081  ...    78.0      50.0      96.0   1.02

[8 rows x 11 columns]