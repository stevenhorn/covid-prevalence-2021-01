0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -2432.09    38.94
p_loo       32.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      254   85.8%
 (0.5, 0.7]   (ok)         33   11.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    1    0.3%

              mean        sd    hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.244     0.056     0.156  ...     152.0      93.0   1.04
pu           0.753     0.039     0.701  ...      30.0      42.0   1.03
mu           0.120     0.021     0.083  ...      12.0      27.0   1.13
mus          0.214     0.035     0.160  ...      94.0      60.0   1.00
gamma        0.285     0.047     0.202  ...     134.0      72.0   1.01
Is_begin  1499.204   942.154   122.191  ...      36.0      59.0   1.07
Ia_begin  4827.062  2034.220  1676.554  ...     152.0      93.0   1.01
E_begin   5217.291  3229.071   627.823  ...      43.0      60.0   1.04

[8 rows x 11 columns]