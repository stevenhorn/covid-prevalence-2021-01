0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.83    61.04
p_loo       37.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.053   0.181    0.342  ...   152.0     152.0      53.0   0.99
pu        0.800  0.038   0.723    0.851  ...    37.0      43.0      39.0   1.03
mu        0.151  0.028   0.111    0.203  ...    24.0      24.0      69.0   1.02
mus       0.248  0.047   0.159    0.334  ...    98.0      98.0      76.0   1.01
gamma     0.358  0.068   0.241    0.443  ...    75.0     122.0      59.0   1.00
Is_begin  0.946  0.959   0.002    2.935  ...    78.0      33.0      14.0   1.07
Ia_begin  1.582  1.560   0.055    4.245  ...   113.0      60.0      80.0   1.01
E_begin   1.035  1.351   0.004    3.420  ...    90.0      80.0      59.0   0.99

[8 rows x 11 columns]