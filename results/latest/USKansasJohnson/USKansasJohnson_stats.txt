0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1715.35    25.65
p_loo       11.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.051   0.179    0.345  ...    18.0      33.0      39.0   1.06
pu        0.873  0.017   0.841    0.900  ...     7.0       7.0      46.0   1.27
mu        0.159  0.039   0.095    0.225  ...     3.0       3.0      26.0   2.04
mus       0.151  0.030   0.099    0.195  ...    33.0      34.0      38.0   1.05
gamma     0.135  0.039   0.085    0.197  ...     5.0       5.0      93.0   1.42
Is_begin  1.841  1.695   0.077    5.246  ...     5.0       6.0      33.0   1.32
Ia_begin  3.513  3.366   0.009   10.624  ...    36.0      26.0      54.0   1.06
E_begin   1.527  1.247   0.019    3.544  ...    45.0      19.0      33.0   1.10

[8 rows x 11 columns]