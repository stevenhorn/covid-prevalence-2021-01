0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -744.88    33.81
p_loo       23.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.052   0.170    0.346  ...   133.0     107.0      59.0   0.99
pu        0.882  0.012   0.861    0.899  ...    51.0      49.0      58.0   1.04
mu        0.152  0.029   0.111    0.204  ...    11.0      11.0      84.0   1.16
mus       0.169  0.029   0.110    0.227  ...    82.0     100.0      46.0   1.04
gamma     0.223  0.045   0.143    0.289  ...   105.0     105.0      86.0   1.00
Is_begin  0.903  0.835   0.045    2.671  ...   150.0     134.0      56.0   0.99
Ia_begin  1.980  1.739   0.037    5.579  ...    90.0      94.0      96.0   0.98
E_begin   1.001  1.190   0.011    3.350  ...   130.0     105.0      59.0   0.99

[8 rows x 11 columns]