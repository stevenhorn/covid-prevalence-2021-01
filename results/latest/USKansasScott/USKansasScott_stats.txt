0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -734.26    54.53
p_loo       44.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      287   97.3%
 (0.5, 0.7]   (ok)          5    1.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.031   0.152    0.249  ...    69.0      58.0      58.0   1.04
pu        0.713  0.011   0.700    0.733  ...    39.0      34.0      17.0   1.03
mu        0.127  0.019   0.091    0.156  ...    32.0      34.0      55.0   1.02
mus       0.205  0.036   0.140    0.268  ...    39.0      38.0      57.0   1.05
gamma     0.275  0.041   0.201    0.335  ...    67.0      53.0      16.0   1.03
Is_begin  0.510  0.497   0.001    1.661  ...    13.0      21.0      40.0   1.08
Ia_begin  0.794  0.922   0.009    2.169  ...    89.0      23.0      60.0   1.08
E_begin   0.340  0.357   0.006    0.957  ...    74.0      55.0      58.0   1.03

[8 rows x 11 columns]