0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1464.19    23.01
p_loo       24.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   89.6%
 (0.5, 0.7]   (ok)         27    8.7%
   (0.7, 1]   (bad)         5    1.6%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.256   0.050   0.172    0.339  ...    62.0      73.0      59.0   1.08
pu         0.794   0.053   0.718    0.886  ...    64.0      68.0      66.0   1.00
mu         0.108   0.018   0.075    0.137  ...    18.0      22.0      48.0   1.12
mus        0.180   0.031   0.133    0.232  ...    63.0      65.0      63.0   1.00
gamma      0.244   0.043   0.175    0.315  ...    90.0     117.0      76.0   1.02
Is_begin   7.260   6.453   0.637   19.557  ...    93.0      43.0      53.0   1.03
Ia_begin  54.332  29.929   6.028  108.955  ...   152.0     152.0      59.0   1.00
E_begin   40.472  35.746   0.483  120.658  ...   105.0      89.0      77.0   1.01

[8 rows x 11 columns]