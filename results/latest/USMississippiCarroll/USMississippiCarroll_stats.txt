0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -839.44    50.62
p_loo       37.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.056   0.165    0.343  ...   102.0     105.0      80.0   1.00
pu        0.753  0.028   0.700    0.797  ...    66.0      61.0      30.0   1.02
mu        0.133  0.022   0.083    0.167  ...    45.0      41.0      56.0   1.02
mus       0.213  0.030   0.153    0.267  ...    89.0      97.0      88.0   1.00
gamma     0.288  0.050   0.213    0.374  ...    96.0     103.0      59.0   1.01
Is_begin  0.569  0.553   0.001    1.634  ...    76.0      57.0      60.0   1.03
Ia_begin  1.178  1.293   0.026    3.642  ...    60.0      81.0      60.0   1.00
E_begin   0.573  0.570   0.011    1.721  ...   102.0     124.0      80.0   1.03

[8 rows x 11 columns]