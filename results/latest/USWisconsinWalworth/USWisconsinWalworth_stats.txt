0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1260.97    35.87
p_loo       25.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.054   0.150    0.336  ...    66.0      54.0      14.0   1.06
pu        0.816  0.034   0.754    0.873  ...    52.0      59.0      43.0   1.03
mu        0.107  0.022   0.075    0.146  ...    20.0      20.0      40.0   1.05
mus       0.151  0.029   0.103    0.202  ...   147.0     135.0      75.0   1.00
gamma     0.167  0.026   0.121    0.211  ...    94.0     101.0      97.0   1.01
Is_begin  1.005  0.862   0.022    2.972  ...   152.0     117.0      59.0   1.00
Ia_begin  0.674  0.584   0.011    1.936  ...   108.0      27.0      21.0   1.08
E_begin   0.789  0.803   0.068    2.477  ...   129.0      82.0      70.0   1.02

[8 rows x 11 columns]