0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -921.08    31.51
p_loo       26.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.060   0.156    0.338  ...    76.0      79.0      72.0   0.99
pu        0.839  0.026   0.799    0.886  ...    48.0      51.0      59.0   1.03
mu        0.140  0.024   0.098    0.177  ...    20.0      20.0      40.0   1.05
mus       0.164  0.029   0.120    0.223  ...    57.0      59.0      59.0   1.02
gamma     0.223  0.037   0.158    0.289  ...    63.0      67.0      92.0   1.04
Is_begin  0.505  0.617   0.005    1.816  ...    56.0      64.0      58.0   0.99
Ia_begin  1.682  2.014   0.023    6.420  ...    77.0     113.0      58.0   1.00
E_begin   0.607  0.768   0.005    2.196  ...    58.0      63.0      56.0   1.00

[8 rows x 11 columns]