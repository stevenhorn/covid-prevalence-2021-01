0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -780.49    30.08
p_loo       28.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.056   0.170    0.349  ...   114.0      99.0      19.0   1.03
pu        0.806  0.029   0.752    0.850  ...    35.0      38.0      40.0   1.06
mu        0.126  0.024   0.081    0.163  ...    17.0      16.0      43.0   1.11
mus       0.164  0.036   0.104    0.246  ...   104.0      97.0      59.0   1.00
gamma     0.184  0.038   0.105    0.249  ...    84.0      78.0      44.0   0.99
Is_begin  0.773  0.698   0.019    1.809  ...   104.0     114.0      48.0   1.02
Ia_begin  1.546  1.158   0.119    4.063  ...    82.0      80.0      93.0   1.01
E_begin   0.850  0.825   0.012    2.599  ...    84.0      60.0      60.0   0.99

[8 rows x 11 columns]