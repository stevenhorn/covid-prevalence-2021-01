2 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1379.17    25.81
p_loo       27.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.232   0.057   0.156    0.332  ...   152.0     152.0      53.0   1.10
pu         0.740   0.033   0.703    0.803  ...   152.0     152.0      95.0   0.99
mu         0.116   0.017   0.087    0.148  ...   106.0     117.0      61.0   1.08
mus        0.176   0.029   0.127    0.226  ...   133.0     152.0      80.0   1.03
gamma      0.294   0.044   0.226    0.371  ...   152.0     152.0      74.0   1.01
Is_begin   7.861   4.814   0.241   16.483  ...    92.0      74.0      54.0   1.02
Ia_begin  25.350   9.088  10.359   42.976  ...    73.0      89.0      40.0   1.01
E_begin   45.839  21.860   8.266   78.120  ...    91.0      72.0      54.0   1.03

[8 rows x 11 columns]