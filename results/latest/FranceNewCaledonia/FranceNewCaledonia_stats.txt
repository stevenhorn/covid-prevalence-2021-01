0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -167.01    39.34
p_loo       44.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      257   86.8%
 (0.5, 0.7]   (ok)         27    9.1%
   (0.7, 1]   (bad)         9    3.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.056   0.164    0.347  ...   152.0     152.0      49.0   0.99
pu        0.860  0.028   0.806    0.898  ...    69.0      50.0      38.0   1.00
mu        0.168  0.033   0.115    0.232  ...    72.0      73.0      18.0   1.03
mus       0.232  0.034   0.177    0.295  ...    80.0      80.0      20.0   1.05
gamma     0.293  0.047   0.208    0.381  ...    18.0      21.0      60.0   1.12
Is_begin  1.535  1.138   0.069    3.803  ...    70.0      68.0      59.0   1.00
Ia_begin  0.783  0.588   0.008    1.934  ...    83.0      51.0      18.0   1.02
E_begin   1.054  1.215   0.000    3.347  ...    66.0      61.0      30.0   1.00

[8 rows x 11 columns]