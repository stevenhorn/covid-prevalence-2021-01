0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo   330.25    97.31
p_loo       76.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.047   0.194    0.345  ...   152.0     132.0      57.0   1.03
pu        0.875  0.024   0.823    0.900  ...    97.0      63.0      33.0   1.03
mu        0.132  0.029   0.089    0.178  ...    25.0      17.0      58.0   1.08
mus       0.261  0.049   0.164    0.331  ...    81.0      81.0      96.0   1.00
gamma     0.291  0.054   0.189    0.382  ...   118.0     129.0      96.0   1.08
Is_begin  0.192  0.177   0.003    0.570  ...    48.0      45.0      97.0   1.02
Ia_begin  0.301  0.459   0.004    0.898  ...    72.0      35.0      59.0   1.05
E_begin   0.156  0.244   0.003    0.436  ...    83.0      64.0      39.0   0.99

[8 rows x 11 columns]