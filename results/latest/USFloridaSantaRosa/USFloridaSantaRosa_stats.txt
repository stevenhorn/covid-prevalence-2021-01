0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1229.39    35.58
p_loo       31.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.059   0.164    0.350  ...    87.0      88.0      51.0   1.02
pu        0.788  0.038   0.713    0.846  ...    59.0      58.0      60.0   1.00
mu        0.133  0.024   0.088    0.176  ...    21.0      22.0      47.0   1.06
mus       0.190  0.031   0.144    0.259  ...    64.0      64.0      50.0   1.02
gamma     0.248  0.051   0.181    0.360  ...    86.0     104.0      91.0   1.04
Is_begin  1.333  1.287   0.006    3.936  ...    66.0      41.0      22.0   1.02
Ia_begin  0.830  0.619   0.025    1.870  ...   152.0     104.0      38.0   1.02
E_begin   0.909  0.909   0.050    2.731  ...    91.0      84.0      61.0   1.02

[8 rows x 11 columns]