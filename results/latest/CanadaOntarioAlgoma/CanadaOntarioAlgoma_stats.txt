0 Divergences 
Passed validation 
Computed from 80 by 303 log-likelihood matrix

         Estimate       SE
elpd_loo  -387.17    44.64
p_loo       32.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      288   95.0%
 (0.5, 0.7]   (ok)         11    3.6%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.271   0.054   0.173    0.345  ...    53.0      76.0      50.0   1.01
pu         0.883   0.014   0.856    0.899  ...    38.0      58.0      33.0   1.03
mu         0.152   0.030   0.104    0.198  ...    12.0      13.0      59.0   1.13
mus        0.212   0.044   0.139    0.292  ...    37.0      32.0      79.0   1.06
gamma      0.277   0.065   0.181    0.398  ...   152.0     152.0      93.0   1.11
Is_begin   3.415   3.033   0.032    9.402  ...   122.0     133.0      93.0   0.99
Ia_begin  10.949  11.862   0.020   38.624  ...    61.0      50.0      59.0   1.01
E_begin    2.777   2.933   0.007   10.066  ...   124.0      70.0      56.0   1.04

[8 rows x 11 columns]