0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -912.47    22.58
p_loo       24.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.058   0.150    0.333  ...    56.0      49.0      39.0   1.10
pu        0.827  0.033   0.733    0.861  ...    35.0      41.0      15.0   1.02
mu        0.149  0.026   0.094    0.191  ...    20.0      19.0      16.0   1.07
mus       0.193  0.030   0.144    0.254  ...    74.0      58.0      87.0   1.11
gamma     0.271  0.040   0.209    0.336  ...   152.0     152.0      99.0   0.99
Is_begin  0.757  0.690   0.035    1.821  ...    76.0      78.0      52.0   0.99
Ia_begin  1.578  1.464   0.039    4.258  ...    80.0      67.0      60.0   1.03
E_begin   1.054  1.237   0.012    3.224  ...    66.0      39.0      40.0   1.05

[8 rows x 11 columns]