0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1179.14    23.92
p_loo       24.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.061   0.165    0.348  ...    78.0      71.0      55.0   1.00
pu        0.781  0.037   0.712    0.834  ...    23.0      26.0      88.0   1.06
mu        0.136  0.021   0.093    0.169  ...    19.0      20.0      14.0   1.07
mus       0.201  0.032   0.143    0.257  ...   152.0     152.0      99.0   1.00
gamma     0.266  0.042   0.199    0.345  ...   152.0     152.0      96.0   0.99
Is_begin  1.517  0.930   0.095    2.912  ...    86.0      65.0      54.0   1.02
Ia_begin  4.028  2.968   0.186    9.703  ...    64.0      73.0      51.0   1.00
E_begin   2.807  2.453   0.046    8.004  ...    59.0     131.0      50.0   1.01

[8 rows x 11 columns]