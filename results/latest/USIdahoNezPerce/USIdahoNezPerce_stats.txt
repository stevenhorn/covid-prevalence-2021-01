0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -921.88    25.95
p_loo        7.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.024   0.221    0.307  ...     3.0       3.0      15.0   2.30
pu        0.831  0.010   0.813    0.845  ...     3.0       3.0      14.0   2.58
mu        0.207  0.006   0.199    0.217  ...     3.0       3.0      24.0   2.22
mus       0.150  0.010   0.138    0.172  ...     3.0       3.0      22.0   1.91
gamma     0.122  0.004   0.116    0.128  ...     4.0       4.0      15.0   1.65
Is_begin  1.934  1.985   0.154    5.463  ...     3.0       3.0      24.0   2.95
Ia_begin  5.295  3.898   1.416   11.552  ...     3.0       3.0      15.0   2.13
E_begin   2.707  2.558   0.374    8.396  ...     3.0       3.0      20.0   2.26

[8 rows x 11 columns]