0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -769.82    26.73
p_loo       24.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.051   0.158    0.331  ...   152.0     152.0      59.0   0.99
pu        0.807  0.047   0.734    0.880  ...    23.0      21.0      59.0   1.07
mu        0.138  0.023   0.106    0.184  ...    13.0      11.0      66.0   1.15
mus       0.178  0.037   0.120    0.239  ...   152.0     152.0      40.0   1.00
gamma     0.233  0.036   0.176    0.288  ...   116.0     152.0      95.0   1.05
Is_begin  1.376  1.144   0.025    3.726  ...    92.0      58.0      60.0   1.03
Ia_begin  3.902  2.511   0.357    8.803  ...    63.0      39.0      28.0   1.05
E_begin   2.647  2.478   0.097    7.259  ...   109.0     102.0      95.0   0.99

[8 rows x 11 columns]