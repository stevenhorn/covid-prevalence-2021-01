0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -860.04    26.31
p_loo       23.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.049   0.165    0.336  ...    42.0      40.0      22.0   1.08
pu        0.851  0.024   0.811    0.880  ...    75.0      73.0      58.0   1.02
mu        0.133  0.034   0.084    0.207  ...     4.0       5.0      21.0   1.48
mus       0.154  0.030   0.105    0.210  ...   107.0     102.0      93.0   1.03
gamma     0.173  0.033   0.108    0.229  ...    58.0      60.0     100.0   1.04
Is_begin  1.010  0.940   0.003    2.535  ...    93.0      50.0      48.0   1.02
Ia_begin  2.651  2.185   0.026    6.890  ...    70.0      69.0      42.0   1.02
E_begin   1.316  1.387   0.000    4.333  ...    66.0      67.0      77.0   1.00

[8 rows x 11 columns]