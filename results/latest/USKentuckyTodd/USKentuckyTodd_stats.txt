0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -717.42    39.45
p_loo       38.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.056   0.170    0.348  ...    88.0      87.0      40.0   1.00
pu        0.813  0.027   0.749    0.865  ...    90.0      93.0      56.0   0.99
mu        0.147  0.028   0.104    0.200  ...    32.0      34.0      74.0   1.04
mus       0.220  0.039   0.161    0.308  ...    97.0     107.0      59.0   0.99
gamma     0.279  0.071   0.152    0.422  ...   152.0     152.0      60.0   1.00
Is_begin  0.992  0.899   0.013    2.355  ...    83.0     147.0      59.0   0.99
Ia_begin  1.831  1.381   0.023    4.152  ...    81.0      88.0     100.0   1.00
E_begin   0.844  0.992   0.003    2.778  ...    67.0      98.0      59.0   1.00

[8 rows x 11 columns]