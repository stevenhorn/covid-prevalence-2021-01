0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1389.20    36.69
p_loo       33.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.179   0.018   0.150    0.211  ...    95.0      70.0      52.0   1.04
pu         0.709   0.009   0.700    0.725  ...   152.0     113.0      51.0   0.99
mu         0.094   0.016   0.068    0.121  ...    16.0      16.0      33.0   1.06
mus        0.242   0.041   0.171    0.308  ...    80.0     148.0      48.0   0.98
gamma      0.685   0.092   0.536    0.900  ...   131.0     152.0      54.0   0.99
Is_begin   3.573   1.707   1.178    6.754  ...   147.0     152.0     100.0   1.03
Ia_begin  20.037   3.921  13.605   27.440  ...   152.0     152.0      93.0   1.00
E_begin   99.860  22.246  65.973  138.070  ...    72.0      66.0      93.0   1.11

[8 rows x 11 columns]