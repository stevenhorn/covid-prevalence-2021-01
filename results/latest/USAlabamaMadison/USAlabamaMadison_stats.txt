0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1267.39    25.45
p_loo       26.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.048   0.169    0.328  ...   152.0     152.0      56.0   1.07
pu        0.754  0.031   0.701    0.804  ...    65.0      67.0      91.0   1.02
mu        0.142  0.026   0.088    0.183  ...    26.0      24.0      48.0   1.05
mus       0.215  0.036   0.162    0.294  ...   126.0     119.0      74.0   1.02
gamma     0.292  0.048   0.199    0.368  ...    93.0     110.0      96.0   0.99
Is_begin  1.881  1.438   0.029    4.521  ...   139.0      87.0      54.0   1.02
Ia_begin  4.653  3.014   0.101    9.836  ...   110.0     132.0      60.0   1.11
E_begin   3.674  3.374   0.082   11.398  ...    59.0      63.0      56.0   1.04

[8 rows x 11 columns]