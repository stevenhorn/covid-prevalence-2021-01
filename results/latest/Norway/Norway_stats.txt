0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1510.12    46.53
p_loo       31.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   90.9%
 (0.5, 0.7]   (ok)         17    5.7%
   (0.7, 1]   (bad)         9    3.0%
   (1, Inf)   (very bad)    1    0.3%

              mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.229    0.047    0.157  ...      69.0      60.0   1.02
pu           0.745    0.032    0.704  ...     152.0      88.0   0.99
mu           0.151    0.020    0.115  ...       9.0      59.0   1.19
mus          0.234    0.036    0.169  ...      75.0      59.0   1.03
gamma        0.300    0.041    0.237  ...     119.0      91.0   0.98
Is_begin   374.628  223.442   16.421  ...      18.0      14.0   1.09
Ia_begin   988.719  337.909  265.076  ...      30.0      19.0   1.07
E_begin   1168.594  591.107   75.033  ...      51.0      24.0   1.03

[8 rows x 11 columns]