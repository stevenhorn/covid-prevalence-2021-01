0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -653.16    37.10
p_loo       26.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.217  0.035   0.163    0.279  ...    20.0      21.0      38.0   1.22
pu        0.760  0.017   0.720    0.788  ...    17.0      23.0      41.0   1.08
mu        0.168  0.032   0.125    0.227  ...     3.0       3.0      38.0   2.02
mus       0.201  0.033   0.145    0.257  ...    11.0      12.0      39.0   1.14
gamma     0.149  0.026   0.111    0.196  ...     6.0       6.0      43.0   1.37
Is_begin  0.794  0.532   0.081    1.684  ...     8.0       8.0      36.0   1.21
Ia_begin  0.300  0.262   0.019    0.767  ...     7.0       4.0      16.0   1.53
E_begin   0.389  0.296   0.012    0.856  ...    11.0       9.0      29.0   1.22

[8 rows x 11 columns]