0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -632.58    35.90
p_loo       29.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.054   0.156    0.333  ...    29.0      26.0      40.0   1.05
pu        0.752  0.030   0.700    0.804  ...     9.0       9.0      14.0   1.20
mu        0.143  0.025   0.098    0.188  ...    14.0      22.0      57.0   1.12
mus       0.185  0.033   0.135    0.250  ...    68.0      70.0     100.0   1.00
gamma     0.219  0.044   0.149    0.291  ...    68.0      89.0      40.0   1.04
Is_begin  0.706  0.709   0.018    2.193  ...   144.0     122.0      93.0   1.00
Ia_begin  1.376  1.402   0.012    4.263  ...    81.0     152.0      73.0   0.99
E_begin   0.629  0.692   0.004    1.924  ...    74.0      98.0      59.0   1.02

[8 rows x 11 columns]