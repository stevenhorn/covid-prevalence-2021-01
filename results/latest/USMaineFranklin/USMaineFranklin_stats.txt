0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -555.67    42.82
p_loo       33.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.298  0.037   0.226    0.347  ...    68.0      56.0      48.0   1.01
pu        0.891  0.010   0.876    0.900  ...    58.0      57.0      40.0   1.01
mu        0.153  0.032   0.109    0.215  ...     6.0       8.0      14.0   1.23
mus       0.179  0.029   0.131    0.231  ...    62.0      70.0      22.0   1.02
gamma     0.235  0.038   0.171    0.304  ...    52.0      56.0      88.0   1.05
Is_begin  1.054  1.002   0.028    3.244  ...   105.0      70.0      54.0   0.98
Ia_begin  2.669  2.227   0.047    6.696  ...   108.0      71.0      24.0   1.00
E_begin   1.622  1.425   0.040    4.396  ...   103.0      73.0      36.0   1.01

[8 rows x 11 columns]