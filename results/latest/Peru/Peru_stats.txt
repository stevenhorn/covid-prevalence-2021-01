1 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -2609.90    26.36
p_loo       19.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   90.9%
 (0.5, 0.7]   (ok)         25    8.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.246   0.043   0.175    0.334  ...   152.0     152.0      23.0   1.06
pu         0.790   0.057   0.701    0.882  ...    67.0      90.0      91.0   1.01
mu         0.098   0.016   0.069    0.130  ...   112.0     109.0      91.0   0.99
mus        0.143   0.034   0.092    0.219  ...    80.0      80.0      59.0   1.00
gamma      0.233   0.035   0.169    0.291  ...    40.0      42.0      53.0   1.01
Is_begin  27.829  27.105   0.333   79.694  ...    93.0      58.0      57.0   1.00
Ia_begin  77.273  57.186   0.338  172.713  ...    81.0      51.0      21.0   1.03
E_begin   60.754  57.500   0.704  153.338  ...    54.0      33.0      46.0   1.03

[8 rows x 11 columns]