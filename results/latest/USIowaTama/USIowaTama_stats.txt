0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -839.36    34.54
p_loo       28.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.059   0.162    0.348  ...   123.0     115.0      60.0   1.02
pu        0.785  0.037   0.710    0.842  ...     8.0       9.0      33.0   1.20
mu        0.134  0.023   0.102    0.176  ...    54.0      50.0      39.0   1.01
mus       0.213  0.034   0.164    0.281  ...    88.0     107.0      17.0   1.18
gamma     0.288  0.047   0.216    0.364  ...   152.0     152.0      93.0   0.99
Is_begin  1.115  0.924   0.022    2.842  ...   114.0      73.0      58.0   1.02
Ia_begin  2.367  1.606   0.023    5.155  ...    31.0      25.0      43.0   1.07
E_begin   1.642  1.659   0.084    3.841  ...   106.0     116.0      59.0   1.01

[8 rows x 11 columns]