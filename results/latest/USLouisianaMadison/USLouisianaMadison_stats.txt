0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -951.37    51.63
p_loo       41.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.042   0.158    0.297  ...    73.0      49.0      56.0   1.04
pu        0.721  0.018   0.700    0.752  ...    61.0      37.0      22.0   1.02
mu        0.172  0.024   0.133    0.216  ...    38.0      43.0      43.0   1.04
mus       0.242  0.031   0.186    0.294  ...   152.0     152.0      74.0   0.99
gamma     0.342  0.058   0.244    0.448  ...   118.0     130.0      58.0   0.98
Is_begin  0.580  0.622   0.000    1.753  ...   113.0      64.0      40.0   1.00
Ia_begin  1.204  1.289   0.027    3.679  ...   105.0      82.0      60.0   1.03
E_begin   0.448  0.449   0.001    1.507  ...    87.0      81.0      54.0   1.02

[8 rows x 11 columns]