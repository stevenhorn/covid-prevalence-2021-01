0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -633.40    32.60
p_loo       24.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.055   0.171    0.347  ...    93.0      84.0      26.0   1.13
pu        0.880  0.019   0.847    0.900  ...   143.0      93.0      51.0   1.01
mu        0.134  0.028   0.094    0.191  ...    41.0      40.0      40.0   1.07
mus       0.179  0.034   0.119    0.233  ...    97.0      80.0      88.0   1.03
gamma     0.203  0.043   0.135    0.299  ...   152.0     152.0      93.0   1.01
Is_begin  0.675  0.533   0.068    1.913  ...   141.0     112.0      91.0   1.04
Ia_begin  1.325  1.523   0.006    4.462  ...   136.0      65.0      40.0   1.03
E_begin   0.655  0.931   0.001    2.410  ...   108.0      80.0      93.0   1.11

[8 rows x 11 columns]