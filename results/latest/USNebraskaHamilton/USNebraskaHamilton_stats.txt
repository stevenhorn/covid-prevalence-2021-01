0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -607.32    32.60
p_loo       27.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.055   0.161    0.333  ...    51.0      57.0      40.0   1.03
pu        0.858  0.025   0.809    0.899  ...    12.0      11.0      15.0   1.15
mu        0.140  0.023   0.098    0.183  ...    77.0      80.0      59.0   1.01
mus       0.179  0.030   0.126    0.232  ...    89.0      87.0      60.0   1.00
gamma     0.235  0.041   0.159    0.297  ...    26.0      28.0      58.0   1.09
Is_begin  1.077  0.894   0.021    2.620  ...   118.0      87.0      60.0   1.00
Ia_begin  2.611  2.443   0.075    7.357  ...    95.0      65.0      73.0   1.01
E_begin   1.365  1.778   0.008    5.463  ...    73.0      57.0      60.0   1.02

[8 rows x 11 columns]