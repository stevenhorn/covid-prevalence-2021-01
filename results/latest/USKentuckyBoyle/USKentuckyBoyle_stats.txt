0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -870.36    40.61
p_loo       30.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.056   0.150    0.321  ...    94.0      73.0      59.0   1.00
pu        0.776  0.031   0.714    0.826  ...    98.0     102.0      96.0   0.99
mu        0.121  0.023   0.079    0.164  ...     8.0       8.0      15.0   1.23
mus       0.169  0.027   0.123    0.219  ...    34.0      46.0      59.0   1.04
gamma     0.187  0.035   0.140    0.263  ...   152.0     152.0      96.0   1.01
Is_begin  0.824  0.690   0.006    2.123  ...   137.0      84.0      51.0   1.02
Ia_begin  1.415  1.160   0.006    3.921  ...    63.0      42.0      37.0   0.99
E_begin   0.825  0.717   0.005    2.067  ...    75.0      67.0      59.0   1.01

[8 rows x 11 columns]