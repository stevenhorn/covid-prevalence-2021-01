0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1166.95    27.17
p_loo       31.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.050   0.157    0.331  ...    86.0     120.0      44.0   1.00
pu        0.744  0.032   0.700    0.797  ...   128.0      98.0      34.0   1.01
mu        0.179  0.019   0.139    0.214  ...    32.0      33.0      19.0   1.02
mus       0.298  0.035   0.245    0.354  ...   104.0     139.0      60.0   1.04
gamma     0.521  0.069   0.400    0.648  ...    85.0      85.0      79.0   0.99
Is_begin  1.926  1.799   0.087    5.548  ...   103.0      96.0      59.0   1.01
Ia_begin  3.927  3.258   0.150   10.437  ...    70.0      54.0      60.0   1.03
E_begin   2.300  2.017   0.006    5.442  ...    44.0      38.0      88.0   1.03

[8 rows x 11 columns]