0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -569.22    21.10
p_loo       18.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.182    0.336  ...    60.0      54.0      43.0   1.01
pu        0.846  0.027   0.776    0.883  ...    10.0      10.0      22.0   1.16
mu        0.136  0.025   0.097    0.177  ...    23.0      22.0      49.0   1.11
mus       0.172  0.033   0.106    0.224  ...    67.0      68.0      36.0   1.04
gamma     0.201  0.043   0.128    0.263  ...    76.0      83.0      57.0   1.01
Is_begin  0.866  0.735   0.018    2.212  ...   116.0      62.0      24.0   1.02
Ia_begin  0.602  0.602   0.028    2.068  ...    86.0      38.0      59.0   1.07
E_begin   0.437  0.575   0.007    1.435  ...    95.0      23.0      59.0   1.08

[8 rows x 11 columns]